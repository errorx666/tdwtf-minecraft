globalThis.initializeCoreMod = () => {
	const className = 'net.minecraft.block.SpawnerBlock';
	const classNameInternal = className.replace( /\./g, '/' );
	try {
		log( 'DEBUG', '{}::initializeCoreMod', [ __filename ] );
		return {
			SpawnerBlock: {
				target: {
					type: 'CLASS',
					name: className
				},
				transformer( classNode ) {
					try {
						log( 'DEBUG', 'SpawnerBlock::transformer' );

						const LOOT_TABLE = new FieldNode(
							Opcodes.ACC_PUBLIC | Opcodes.ACC_STATIC | Opcodes.ACC_FINAL, // access
							'LOOT_TABLE', // name
							'Lnet/minecraft/util/ResourceLocation;', // descriptor
							null, // signature
							null // value
						);
						classNode.fields.add( LOOT_TABLE );

						const clinit =
							new MethodNode(
								Opcodes.ACC_STATIC, // access
								'<clinit>', // name
								'()V', // description
								null, // signature
								[] // exceptions
							);
						const clinitInstructions = clinit.instructions = new InsnList;
						clinitInstructions.add( new TypeInsnNode( Opcodes.NEW, 'net/minecraft/util/ResourceLocation' ) );
						clinitInstructions.add( new InsnNode( Opcodes.DUP ) );
						clinitInstructions.add( new LdcInsnNode( `${MOD_ID}:blocks/spawner_block` ) );
						clinitInstructions.add( new MethodInsnNode( Opcodes.INVOKESPECIAL, 'net/minecraft/util/ResourceLocation', '<init>', '(Ljava/lang/String;)V' ) );
						clinitInstructions.add( new FieldInsnNode( Opcodes.PUTSTATIC, classNameInternal, LOOT_TABLE.name, LOOT_TABLE.desc ) );
						clinitInstructions.add( new InsnNode( Opcodes.RETURN ) );
						classNode.methods.add( clinit );

						const getLootTable =
							new MethodNode(
								Opcodes.ACC_PUBLIC, // access
								getMethod( 'func_220068_i' ), // name
								'()Lnet/minecraft/util/ResourceLocation;', // descriptor
								null, // signature,
								[] // exceptions
							);
						const getLootTableInstructions = getLootTable.instructions = new InsnList;
						getLootTableInstructions.add( new FieldInsnNode( Opcodes.GETSTATIC, classNameInternal, LOOT_TABLE.name, LOOT_TABLE.desc ) );
						getLootTableInstructions.add( new InsnNode( Opcodes.ARETURN ) );
						classNode.methods.add( getLootTable );
						// debug.dumpClass( classNode );
						return classNode;
					} catch( ex ) {
						log( 'FATAL', '{}::SpawnerBlock::transformer {}', [ __filename, ex ] );
						throw ex;
					}
				}
			},
			getExpDrop: {
				target: {
					type: 'METHOD',
					class: className,
					methodName: 'getExpDrop',
					methodDesc: '(Lnet/minecraft/block/BlockState;Lnet/minecraft/world/IWorldReader;Lnet/minecraft/util/math/BlockPos;II)I'
				},
				transformer( methodNode ) {
					try {
						const jmpDest = new LabelNode;
						log( 'DEBUG', 'getExpDrop::transformer' );
						const instructions = new InsnList;
						instructions.add( new VarInsnNode( Opcodes.ILOAD, 5 ) );
						instructions.add( new JumpInsnNode( Opcodes.IFLE, jmpDest ) );
						instructions.add( new InsnNode( Opcodes.ICONST_0 ) );
						instructions.add( new InsnNode( Opcodes.IRETURN ) );
						instructions.add( jmpDest );
						methodNode.instructions.insertBefore( methodNode.instructions.getFirst(), instructions );
						// debug.dumpMethod( methodNode );
						return methodNode;
					} catch( ex ) {
						log( 'FATAL', '{}::getExpDrop::transformer {}', [ __filename, ex ] );
						throw ex;
					}
				}
			}
		};
	} catch( ex ) {
		log( 'FATAL', '{}::initializeCoreMod {}', [ __filename, ex ] );
		throw ex;
	}
};
