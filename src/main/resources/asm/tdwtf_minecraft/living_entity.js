globalThis.initializeCoreMod = () => {
	const className = 'net.minecraft.entity.LivingEntity';
	try {
		log( 'DEBUG', '{}::initializeCoreMod', [ __filename ] );
		return {
			LivingEntity: {
				target: {
					type: 'CLASS',
					name: className
				},
				transformer( classNode ) {
					try {
						log( 'DEBUG', 'LivingEntity::transformer' );
						// debug.dumpClass( classNode );
						return classNode;
					} catch( ex ) {
						log( 'FATAL', '{}::LivingEntity::transformer {}', [ __filename, ex ] );
						throw ex;
					}
				}
			},
			baseTick: {
				target: {
					type: 'METHOD',
					class: className,
					methodName: 'baseTick',
					methodDesc: '()V'
				},
				transformer( methodNode ) {
					try {
						log( 'DEBUG', 'baseTick::transformer' );
						const { instructions } = methodNode;
						const firstInsn = instructions.getFirst();
						let actions = [];
						for( let insn = firstInsn; insn; insn = insn.getNext() ) {
							if( insn.getType() === AbstractInsnNode.FIELD_INSN
							&& insn.opcode === Opcodes.GETSTATIC
							&& insn.owner === 'net/minecraft/tags/FluidTags'
							&& insn.name === getField( 'field_206959_a' )
							&& insn.desc === 'Lnet/minecraft/tags/Tag;'
							) {
								actions = [ ...actions, function( insn ) {
									instructions.set( insn, new FieldInsnNode( Opcodes.GETSTATIC, 'net/minecraft/tags/FluidTags', 'DROWNABLE', 'Lnet/minecraft/tags/Tag;' ) );
								}.bind( null, insn ) ];
							}
						}
						for( const action of actions ) {
							action();
						}
						// debug.dumpMethod( methodNode );
						return methodNode;
					} catch( ex ) {
						log( 'FATAL', '{}::baseTick::transformer {}', [ __filename, ex ] );
						throw ex;
					}
				}
			}
		};
	} catch( ex ) {
		log( 'FATAL', '{}::initializeCoreMod {}', [ __filename, ex ] );
		throw ex;
	}
};
