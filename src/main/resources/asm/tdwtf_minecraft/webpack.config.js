const TerserWebpackPlugin = require( 'terser-webpack-plugin' );
const _ = require ( 'lodash' );
const globby = require( 'globby' );
const fs = require( 'fs' );
const path = require( 'path' );
const upath = require( 'upath' );
const webpack = require( 'webpack' );

const resourcesPath = path.resolve( __dirname, '..', '..' );
const outputPath = path.resolve( __dirname, 'dist' );

const files = _.sortBy( globby.sync( [
	'**/*.js',
	'!dist/**/*',
	'!**/*.config.js',
	'!common.js',
	'!polyfills.js'
], {
	cwd: __dirname
} ).map( f => {
	const fullpath = path.resolve( __dirname, f );
	const filename = path.basename( fullpath );
	const name = upath.relative( __dirname, upath.removeExt( fullpath, '.js' ) );
	return { fullpath, filename, name };
 } ), ( { name } ) => name );


fs.writeFileSync(
	path.resolve( resourcesPath, 'META-INF', 'coremods.json' ), JSON.stringify(
		Object.fromEntries( files.map( ( { name } ) => [
			name,
			upath.relative( resourcesPath, path.resolve( outputPath, `${name}.js` ) )
		] ) ),
		null,
		2
	), { encoding: 'utf-8' }
);

const devMode = false;

const loaders = {
	babel: { loader: 'babel-loader' },
	debug: { loader: 'debug-loader' },
	yaml: { loader: 'yaml-loader' }
};

/** @type {import('webpack').Configuration} */
module.exports = {
	devtool: false,
	mode: devMode ? 'development' : 'production',
	module: {
		rules: [
			{ test: /\.js$/i, include: [ path.resolve( __dirname ) ], use: [ loaders.babel ] },
			{ test: /\.ya?ml$/i, include: [ path.resolve( __dirname ) ], use: [ loaders.yaml ], type: 'json' }
		]
	},
	optimization: {
		minimize: true,
		minimizer: [
			new TerserWebpackPlugin( {
				extractComments: false,
				terserOptions: {
					compress: {
						arrows: false,
						booleans: !devMode,
						comparisons: !devMode,
						conditionals: !devMode,
						dead_code: true,
						defaults: false,
						evaluate: true,
						if_return: !devMode,
						inline: !devMode,
						join_vars: !devMode,
						keep_fargs: true,
						properties: true,
						loops: !devMode,
						reduce_vars: true,
						sequences: !devMode,
						side_effects: !devMode,
						typeofs: false,
						unused: true
					},
					ecma: 5,
					mangle: !devMode,
					keep_classnames: true,
					keep_fnames: !devMode,
					output: {
						beautify: devMode,
						braces: true,
						comments: false,
						indent_level: 2,
						quote_style: 1
					}
				}
			} )
		],
		splitChunks: false
	},
	output: {
		ecmaVersion: 5,
		filename: '[name].js',
		globalObject: 'this',
		path: outputPath
	},
	target: 'node',
	plugins: [
		new webpack.DefinePlugin( {
			DEV: JSON.stringify( devMode ),
			ENABLE_LOGGING: JSON.stringify( true ),
			MOD_ID: JSON.stringify( 'tdwtf_minecraft' )
		} ),
		new webpack.ProvidePlugin( {
			ASMAPI: [ path.resolve( __dirname, 'common.js' ), 'ASMAPI' ],
			log: [ path.resolve( __dirname, 'common.js' ), 'log' ],
			debug: [ path.resolve( __dirname, 'common.js' ), 'debug' ],
			AbstractInsnNode: [ path.resolve( __dirname, 'common.js' ), 'AbstractInsnNode' ],
			Attribute: [ path.resolve( __dirname, 'common.js' ), 'Attribute' ],
			FieldInsnNode: [ path.resolve( __dirname, 'common.js' ), 'FieldInsnNode' ],
			FieldNode: [ path.resolve( __dirname, 'common.js' ), 'FieldNode' ],
			FrameNode: [ path.resolve( __dirname, 'common.js' ), 'FrameNode' ],
			Handle: [ path.resolve( __dirname, 'common.js' ), 'Handle' ],
			IincInsnNode: [ path.resolve( __dirname, 'common.js' ), 'IincInsnNode' ],
			InsnList: [ path.resolve( __dirname, 'common.js' ), 'InsnList' ],
			InsnNode: [ path.resolve( __dirname, 'common.js' ), 'InsnNode' ],
			IntInsnNode: [ path.resolve( __dirname, 'common.js' ), 'IntInsnNode' ],
			InvokeDynamicInsnNode: [ path.resolve( __dirname, 'common.js' ), 'InvokeDynamicInsnNode' ],
			JumpInsnNode: [ path.resolve( __dirname, 'common.js' ), 'JumpInsnNode' ],
			Label: [ path.resolve( __dirname, 'common.js' ), 'Label' ],
			LabelNode: [ path.resolve( __dirname, 'common.js' ), 'LabelNode' ],
			LdcInsnNode: [ path.resolve( __dirname, 'common.js' ), 'LdcInsnNode' ],
			LineNumberNode: [ path.resolve( __dirname, 'common.js' ), 'LineNumberNode' ],
			LocalVariableAnnotationNode: [ path.resolve( __dirname, 'common.js' ), 'LocalVariableAnnotationNode' ],
			LocalVariableNode: [ path.resolve( __dirname, 'common.js' ), 'LocalVariableNode' ],
			LookupSwitchInsnNode: [ path.resolve( __dirname, 'common.js' ), 'LookupSwitchInsnNode' ],
			MethodInsnNode: [ path.resolve( __dirname, 'common.js' ), 'MethodInsnNode' ],
			MethodNode: [ path.resolve( __dirname, 'common.js' ), 'MethodNode' ],
			MultiANewArrayInsnNode: [ path.resolve( __dirname, 'common.js' ), 'MultiANewArrayInsnNode' ],
			Opcodes: [ path.resolve( __dirname, 'common.js' ), 'Opcodes' ],
			ParameterNode: [ path.resolve( __dirname, 'common.js' ), 'ParameterNode' ],
			TableSwitchInsnNode: [ path.resolve( __dirname, 'common.js' ), 'TableSwitchInsnNode' ],
			TryCatchBlockNode: [ path.resolve( __dirname, 'common.js' ), 'TryCatchBlockNode' ],
			Type: [ path.resolve( __dirname, 'common.js' ), 'Type' ],
			TypeAnnotationNode: [ path.resolve( __dirname, 'common.js' ), 'TypeAnnotationNode' ],
			TypeInsnNode: [ path.resolve( __dirname, 'common.js' ), 'TypeInsnNode' ],
			TypePath: [ path.resolve( __dirname, 'common.js' ), 'TypePath' ],
			TypeReference: [ path.resolve( __dirname, 'common.js' ), 'TypeReference' ],
			VarInsnNode: [ path.resolve( __dirname, 'common.js' ), 'VarInsnNode' ],
			getField: [ path.resolve( __dirname, 'common.js' ), 'getField' ],
			getMethod: [ path.resolve( __dirname, 'common.js' ), 'getMethod' ]
		} )
	],
	node: {
		__dirname: true,
		__filename: true
	},
	entry: Object.fromEntries( files.map( ( { fullpath, name } ) => [
		name,
		[ path.resolve( __dirname, 'polyfills.js' ), fullpath ] ]
	) )
};
