let _asmapi;
export const ASMAPI = _asmapi = Java.type( 'net.minecraftforge.coremod.api.ASMAPI' );
let _log;
export const log = _log = ENABLE_LOGGING ? _asmapi.log : () => {};
export const AbstractInsnNode = Java.type( 'org.objectweb.asm.tree.AbstractInsnNode' );
const _abstractInsnNode = Java.type( 'org.objectweb.asm.tree.AbstractInsnNode' );
export const Attribute = Java.type( 'org.objectweb.asm.Attribute' );
export const FieldInsnNode = Java.type( 'org.objectweb.asm.tree.FieldInsnNode' );
export const FieldNode = Java.type( 'org.objectweb.asm.tree.FieldNode' );
export const FrameNode = Java.type( 'org.objectweb.asm.tree.FrameNode' );
export const Handle = Java.type( 'org.objectweb.asm.Handle' );
export const IincInsnNode = Java.type( 'org.objectweb.asm.tree.IincInsnNode' );
export const InsnList = Java.type( 'org.objectweb.asm.tree.InsnList' );
export const InsnNode = Java.type( 'org.objectweb.asm.tree.InsnNode' );
export const IntInsnNode = Java.type( 'org.objectweb.asm.tree.IntInsnNode' );
export const InvokeDynamicInsnNode = Java.type( 'org.objectweb.asm.tree.InvokeDynamicInsnNode' );
export const JumpInsnNode = Java.type( 'org.objectweb.asm.tree.JumpInsnNode' );
export const Label = Java.type( 'org.objectweb.asm.Label' );
export const LabelNode = Java.type( 'org.objectweb.asm.tree.LabelNode' );
export const LdcInsnNode = Java.type( 'org.objectweb.asm.tree.LdcInsnNode' );
export const LineNumberNode = Java.type( 'org.objectweb.asm.tree.LineNumberNode' );
export const LocalVariableAnnotationNode = Java.type( 'org.objectweb.asm.tree.LocalVariableAnnotationNode' );
export const LocalVariableNode = Java.type( 'org.objectweb.asm.tree.LocalVariableNode' );
export const LookupSwitchInsnNode = Java.type( 'org.objectweb.asm.tree.LookupSwitchInsnNode' );
export const MethodInsnNode = Java.type( 'org.objectweb.asm.tree.MethodInsnNode' );
export const MethodNode = Java.type( 'org.objectweb.asm.tree.MethodNode' );
export const MultiANewArrayInsnNode = Java.type( 'org.objectweb.asm.tree.MultiANewArrayInsnNode' );
let _opcodes;
export const Opcodes = _opcodes = Java.type( 'org.objectweb.asm.Opcodes' );
export const ParameterNode = Java.type( 'org.objectweb.asm.tree.ParameterNode' );
export const TableSwitchInsnNode = Java.type( 'org.objectweb.asm.tree.TableSwitchInsnNode' );
export const TryCatchBlockNode = Java.type( 'org.objectweb.asm.tree.TryCatchBlockNode' );
export const Type = Java.type( 'org.objectweb.asm.Type' );
export const TypeAnnotationNode = Java.type( 'org.objectweb.asm.tree.TypeAnnotationNode' );
export const TypeInsnNode = Java.type( 'org.objectweb.asm.tree.TypeInsnNode' );
export const TypePath = Java.type( 'org.objectweb.asm.TypePath' );
export const TypeReference = Java.type( 'org.objectweb.asm.TypeReference' );
export const VarInsnNode = Java.type( 'org.objectweb.asm.tree.VarInsnNode' );
export const DEV_MAPPINGS = Boolean( _asmapi.getSystemPropertyFlag( 'dev' ) );
export function getField( name ) {
	try {
		return DEV_MAPPINGS ? _asmapi.mapField( name ) : name;
	} catch( ex ) {
		_log( 'FATAL', '{}::getField {} {}', [ __filename, name, ex ] );
		throw ex;
	}
}
export function getMethod( name ) {
	try {
		return DEV_MAPPINGS ? _asmapi.mapMethod( name ) : name;
	} catch( ex ) {
		_log( 'FATAL', '{}::getMethod {} {}', [ __filename, name, ex ] );
		throw ex;
	}
}
export const debug = ( () => {
	try {
		const opcodesForward = {};
		const opcodesReverse = {};
		if( ENABLE_LOGGING ) {
			for( const opcodeName of require( './asm.yaml' ).opcodes ) {
				const opcode = _opcodes[ opcodeName ];
				if( opcode == null ) {
					_log( 'ERROR', 'unknown opcode {}', [ opcodeName ] );
				} else {
					opcodesForward[ opcodeName ] = opcode;
					opcodesReverse[ opcode ] = opcodeName;
				}
			}
			opcodesReverse[ -1 ] = 'n/a';
		}

		const debug = {
			dumpAttribute( attr ) {
				if( !ENABLE_LOGGING ) return;
				try {
					const { type } = attr;
					_log( 'DEBUG', '{}::debug::dumpAttribute {}', [ __filename, type ] );
				} catch( ex ) {
					_log( 'FATAL', '{}::debug::dumpAttribute {}', [ __filename, ex ] );
					throw ex;
				}
			},
			dumpClass( classNode ) {
				if( !ENABLE_LOGGING ) return;
				try {
					const { attrs, name, methods, fields } = classNode;
					_log( 'DEBUG', '{}::debug::dumpClass {}', [ __filename, name ] );
					if( attrs ) for( const attr of attrs ) {
						debug.dumpAttribute( attr );
					}
					_log( 'DEBUG', '{}', [ name ] );
					if( methods ) for( const method of methods ) {
						debug.dumpMethod( method );
					}
					if( fields ) for( const field of fields ) {
						debug.dumpField( field );
					}
				} catch( ex ) {
					_log( 'FATAL', '{}::debug::dumpClass {} {}', [ __filename, classNode?.name, ex ] );
					throw ex;
				}
			},
			dumpField( fieldNode ) {
				if( !ENABLE_LOGGING ) return;
				try {
					const { name, desc, signature, value, attrs } = fieldNode;
					_log( 'DEBUG', '{}::debug::dumpField {} {} {} {}', [ __filename, debug.friendlyFieldName( name ), desc, signature, value ] );
					if( attrs ) for( const attr of attrs ) {
						debug.dumpAttribute( attr );
					}
				} catch( ex ) {
					_log( 'FATAL', '{}::debug::dumpField {} {}', [ __filename, fieldNode?.name, ex ] );
					throw ex;
				}
			},
			dumpInstruction( instruction ) {
				if( !ENABLE_LOGGING ) return;
				try {
					const type = instruction.getType();
					const opcode = instruction.getOpcode();
					const opcodeName = opcodesReverse[ opcode ] ?? `${opcode} (unknown)`;
					switch( type ) {
					case _abstractInsnNode.FIELD_INSN: {
						const { owner, name, desc } = instruction;
						_log( 'DEBUG', '{} field {} {} {}', [ opcodeName, owner, debug.friendlyFieldName( name ), desc ] );
						break;
					}
					case _abstractInsnNode.FRAME:
						_log( 'DEBUG', '{} frame', [ opcodeName ] );
						break;
					case _abstractInsnNode.IINC_INSN: {
						const { incr, var: _var } = instruction;
						_log( 'DEBUG', '{} iinc {} {}', [ opcodeName, incr, _var ] );
						break;
					}
					case _abstractInsnNode.INSN:
						_log( 'DEBUG', '{} insn', [ opcodeName ] );
						break;
					case _abstractInsnNode.INT_INSN: {
						const { operand } = instruction;
						_log( 'DEBUG', '{} int {}', [ opcodeName, operand ] );
						break;
					}
					case _abstractInsnNode.INVOKE_DYNAMIC_INSN: {
						const { name, desc } = instruction;
						_log( 'DEBUG', '{} invoke_dynamic {} {}', [ opcodeName, name, desc ] );
						break;
					}
					case _abstractInsnNode.JUMP_INSN: {
						const label = instruction.label.getLabel();
						_log( 'DEBUG', '{} jump {}', [ opcodeName, label.toString() ] );
						break;
					}
					case _abstractInsnNode.LABEL: {
						const label = instruction.getLabel();
						_log( 'DEBUG', 'label {}', [ label.toString() ] );
						break;
					}
					case _abstractInsnNode.LDC_INSN: {
						const { cst } = instruction;
						_log( 'DEBUG', '{} ldc {}', [ opcodeName, cst ] );
						break;
					}
					case _abstractInsnNode.LINE: {
						const { line } = instruction;
						const start = instruction.start.getLabel();
						_log( 'DEBUG', 'line {} {}', [ line, start.toString() ] );
						break;
					}
					case _abstractInsnNode.LOOKUPSWITCH_INSN:
						_log( 'DEBUG', '{} lookupswitch', opcodeName );
						break;
					case _abstractInsnNode.METHOD_INSN: {
						const { desc, itf, name, owner } = instruction;
						_log( 'DEBUG', '{} method {} {} {} {}', [ opcodeName, owner, debug.friendlyMethodName( name ), desc, itf ] );
						break;
					}
					case _abstractInsnNode.MULTIANEWARRAY_INSN:
						_log( 'DEBUG', '{} multianewarray', [ opcodeName ] );
						break;
					case _abstractInsnNode.TABLESWITCH_INSN:
						_log( 'DEBUG', '{} tableswitch', [ opcodeName ] );
						break;
					case _abstractInsnNode.TYPE_INSN: {
						const { desc } = instruction;
						_log( 'DEBUG', '{} type {}', [ opcodeName, desc ] );
						break;
					}
					case _abstractInsnNode.VAR_INSN: {
						const { var: _var } = instruction;
						_log( 'DEBUG', '{} var {}', [ opcodeName, _var ] );
						break;
					}
					default:
						_log( 'DEBUG', 'Unknown instruction: {} {} {}', [ opcodeName, type, instruction ] );
						break;
					}
				} catch( ex ) {
					_log( 'FATAL', '{}::debug::dumpInstruction {}', [ __filename, ex ] );
					throw ex;
				}
			},
			dumpInstructions( instructions ) {
				if( !ENABLE_LOGGING ) return;
				try {
					const arr = instructions.toArray();
					for( const insn of arr ) {
						debug.dumpInstruction( insn );
					}
				} catch( ex ) {
					_log( 'FATAL', '{}::debug::dumpInstructions {}', [ __filename, ex ] );
					throw ex;
				}
			},
			friendlyFieldName( name ) {
				if( name == '' ) return '(empty)';
				const mapped = _asmapi.mapField( name );
				if( mapped === name ) return mapped;
				if( !mapped ) return name;
				else return `${name} (${mapped})`;

			},
			friendlyMethodName( name ) {
				if( name == '' ) return '(empty)';
				const mapped = _asmapi.mapMethod( name );
				if( mapped === name ) return mapped;
				if( !mapped ) return name;
				else return `${name} (${mapped})`;

			},
			dumpMethod( methodNode ) {
				if( !ENABLE_LOGGING ) return;
				try {
					const { attrs, instructions, name, desc, signature } = methodNode;
					_log( 'DEBUG', '{}::debug::dumpMethod {} {} {}', [ __filename, debug.friendlyMethodName( name ), desc, signature ] );
					if( attrs ) for( const attr of attrs ) {
						debug.dumpAttribute( attr );
					}
					if( instructions ) debug.dumpInstructions( instructions );
				} catch( ex ) {
					_log( 'FATAL', '{}::debug::dumpMethod {} {}', [ __filename, methodNode?.name, ex ] );
					throw ex;
				}
			}
		};
		return debug;
	} catch( ex ) {
		_log( 'FATAL', '{}::debug {}', [ __filename, ex ] );
		throw ex;
	}
} )();
