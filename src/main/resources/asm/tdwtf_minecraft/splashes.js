globalThis.initializeCoreMod = () => {
	const className = 'net.minecraft.client.util.Splashes';
	try {
		log( 'DEBUG', '{}::initializeCoreMod', [ __filename ] );
		return {
			'<clinit>': {
				target: {
					type: 'METHOD',
					class: className,
					methodName: '<clinit>',
					methodDesc: '()V'
				},
				transformer( methodNode ) {
					try {
						log( 'DEBUG', '<clinit>::transformer' );
						const { instructions } = methodNode;
						const firstInsn = instructions.getFirst();
						let actions = [];
						for( let insn = firstInsn; insn; insn = insn.getNext() ) {
							if( insn.getType() === AbstractInsnNode.LDC_INSN && insn.cst === 'texts/splashes.txt' ) {
								actions = [ ...actions, function( insn ) {
									instructions.set( insn, new LdcInsnNode( `${MOD_ID}:texts/splashes.txt` ) );
								}.bind( null, insn ) ];
							}
						}
						for( const action of actions ) {
							action();
						}
						debug.dumpMethod( methodNode );
						return methodNode;
					} catch( ex ) {
						log( 'FATAL', '{}::<clinit>::transformer {}', [ __filename, ex ] );
						throw ex;
					}
				}
			}
		};
	} catch( ex ) {
		log( 'FATAL', '{}::initializeCoreMod {}', [ __filename, ex ] );
		throw ex;
	}
};
