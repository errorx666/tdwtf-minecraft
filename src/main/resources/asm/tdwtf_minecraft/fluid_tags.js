globalThis.initializeCoreMod = () => {
	const className = 'net.minecraft.tags.FluidTags';
	const classNameInternal = className.replace( /\./g, '/' );
	try {
		log( 'DEBUG', '{}::initializeCoreMod', [ __filename ] );

		const fields = [ 'ABSORBABLE', 'DROWNABLE' ].map( name => {
			return new FieldNode(
				Opcodes.ACC_PUBLIC | Opcodes.ACC_STATIC | Opcodes.ACC_FINAL, // access
				name, // name
				'Lnet/minecraft/tags/Tag;', // descriptor
				'Lnet/minecraft/tags/Tag<Lnet/minecraft/fluid/Fluid;>;', // signature
				null // value
			);
		} );

		return {
			FluidTags: {
				target: {
					type: 'CLASS',
					name: className
				},
				transformer( classNode ) {
					try {
						log( 'DEBUG', 'FluidTags::transformer' );
						for( const node of fields ) {
							classNode.fields.add( node );
						}
						// debug.dumpClass( classNode );
						return classNode;
					} catch( ex ) {
						log( 'FATAL', '{}::FluidTags::transformer {}', [ __filename, ex ] );
						throw ex;
					}
				}
			},
			'<clinit>': {
				target: {
					type: 'METHOD',
					class: className,
					methodName: '<clinit>',
					methodDesc: '()V'
				},
				transformer( methodNode ) {
					try {
						log( 'DEBUG', '<clinit>::transformer' );
						const { instructions } = methodNode;
						const lastInsn = instructions.getLast();
						for( const node of fields ) {
							instructions.insertBefore( lastInsn, new LdcInsnNode( `${MOD_ID}:${node.name.toLowerCase()}` ) );
							instructions.insertBefore( lastInsn, new MethodInsnNode( Opcodes.INVOKESTATIC, 'net/minecraft/tags/FluidTags', getMethod( 'func_206956_a' ), '(Ljava/lang/String;)Lnet/minecraft/tags/Tag;' ) );
							instructions.insertBefore( lastInsn, new FieldInsnNode( Opcodes.PUTSTATIC, classNameInternal, node.name, node.desc ) );
						}
						// debug.dumpMethod( methodNode );
						return methodNode;
					} catch( ex ) {
						log( 'FATAL', '{}::<clinit>::transformer {}', [ __filename, ex ] );
						throw ex;
					}
				}
			}
		};
	} catch( ex ) {
		log( 'FATAL', '{}::initializeCoreMod {}', [ __filename, ex ] );
		throw ex;
	}
};
