globalThis.initializeCoreMod = () => {
	const className = 'net.minecraft.world.server.ServerWorld';
	try {
		log( 'DEBUG', '{}::initializeCoreMod', [ __filename ] );
		return {
			ServerWorld: {
				target: {
					type: 'CLASS',
					name: className
				},
				transformer( classNode ) {
					try {
						log( 'DEBUG', 'ServerWorld::transformer' );
						// debug.dumpClass( classNode );
						return classNode;
					} catch( ex ) {
						log( 'FATAL', '{}::ServerWorld::transformer {}', [ __filename, ex ] );
						throw ex;
					}
				}
			},
			tickChunk: {
				target: {
					type: 'METHOD',
					class: className,
					methodName: 'tickChunk',
					methodDesc: '()V'
				},
				transformer( methodNode ) {
					try {
						log( 'DEBUG', 'tickChunk::transformer' );
						// debug.dumpMethod( methodNode );
						return methodNode;
					} catch( ex ) {
						log( 'FATAL', '{}::tickChunk::transformer {}', [ __filename, ex ] );
						throw ex;
					}
				}
			}
		};
	} catch( ex ) {
		log( 'FATAL', '{}::initializeCoreMod {}', [ __filename, ex ] );
		throw ex;
	}
};
