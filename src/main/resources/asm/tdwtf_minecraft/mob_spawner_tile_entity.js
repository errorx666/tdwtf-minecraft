globalThis.initializeCoreMod = () => {
	const className = 'net.minecraft.tileentity.MobSpawnerTileEntity';
	try {
		log( 'DEBUG', '{}::initializeCoreMod', [ __filename ] );
		return {
			onlyOpsCanSetNbt: {
				target: {
					type: 'METHOD',
					class: className,
					methodName: 'func_183000_F',
					methodDesc: '()Z'
				},
				transformer( methodNode ) {
					try {
						log( 'DEBUG', 'onlyOpsCanSetNbt::transformer' );
						const instructions = methodNode.instructions = new InsnList;
						instructions.add( new InsnNode( Opcodes.ICONST_0 ) );
						instructions.add( new InsnNode( Opcodes.IRETURN ) );
						return methodNode;
					} catch( ex ) {
						log( 'FATAL', '{}::onlyOpsCanSetNbt::transformer {}', [ __filename, ex ] );
						throw ex;
					}
				}
			}
		};
	} catch( ex ) {
		log( 'FATAL', '{}::initializeCoreMod {}', [ __filename, ex ] );
		throw ex;
	}
};
