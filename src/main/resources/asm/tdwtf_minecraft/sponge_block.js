globalThis.initializeCoreMod = () => {
	const className = 'net.minecraft.block.SpongeBlock';
	try {
		log( 'DEBUG', '{}::initializeCoreMod', [ __filename ] );
		return {
			SpongeBlock: {
				target: {
					type: 'CLASS',
					name: className
				},
				transformer( classNode ) {
					try {
						log( 'DEBUG', 'SpongeBlock::transformer' );
						// debug.dumpClass( classNode );
						return classNode;
					} catch( ex ) {
						log( 'FATAL', '{}::SpongeBlock::transformer {}', [ __filename, ex ] );
						throw ex;
					}
				}
			},
			removeWaterBreadthFirstSearch: {
				target: {
					type: 'METHOD',
					class: className,
					methodName: 'func_176312_d',
					methodDesc: '(Lnet/minecraft/world/World;Lnet/minecraft/util/math/BlockPos;)Z'
				},
				transformer( methodNode ) {
					try {
						log( 'DEBUG', 'removeWaterBreadthFirstSearch::transformer' );
						const instructions = methodNode.instructions;
						const firstInsn = instructions.getFirst();
						let actions = [];
						for( let insn = firstInsn; insn; insn = insn.getNext() ) {
							if( insn.getType() === AbstractInsnNode.FIELD_INSN
							&& insn.opcode === Opcodes.GETSTATIC
							&& insn.owner === 'net/minecraft/tags/FluidTags'
							&& insn.name === getField( 'field_206959_a' )
							&& insn.desc === 'Lnet/minecraft/tags/Tag;'
							) {
								actions = [ ...actions, function( insn ) {
									instructions.set( insn, new FieldInsnNode( Opcodes.GETSTATIC, 'net/minecraft/tags/FluidTags', 'ABSORBABLE', 'Lnet/minecraft/tags/Tag;' ) );
								}.bind( null, insn ) ];
							}
						}
						for( const action of actions ) {
							action();
						}
						// debug.dumpMethod( methodNode );
						return methodNode;
					} catch( ex ) {
						log( 'FATAL', '{}::removeWaterBreadthFirstSearch::transformer {}', [ __filename, ex ] );
						throw ex;
					}
				}
			}
		};
	} catch( ex ) {
		log( 'FATAL', '{}::initializeCoreMod {}', [ __filename, ex ] );
		throw ex;
	}
};
