package net.error_unlimited.tdwtf_minecraft.inventory;

import lombok.val;
import lombok.var;
import lombok.extern.log4j.Log4j2;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;

@Log4j2
public class IllusionContainer extends Container {
	public static int SLOTS = 1;

	private final IInventory _inventory;

	public static IllusionContainer create( int windowId, PlayerInventory playerInventory ) {
		return new IllusionContainer( windowId, playerInventory, new Inventory( SLOTS ) );
	}

	public IllusionContainer( int windowId, PlayerInventory playerInventory, IInventory inventory ) {
		super( IllusionContainerType.CONTAINER_TYPE, windowId );
		LOGGER.debug( "{}::ctor", getClass().getName() );
		_inventory = inventory;
		_inventory.startOpen( playerInventory.player );
		addSlot( new IllusionSlot( inventory, 0, 12 + 4 * 18, 8 + 2 * 18 ) );
		val XSIZE = 184;
		val YSIZE = 184;
		val leftCol = ( XSIZE - 162 ) / 2 + 1;
		for( int playerInvRow = 0; playerInvRow < 3; ++playerInvRow ) {
			for( int playerInvCol = 0; playerInvCol < 9; ++playerInvCol ) {
				addSlot( new Slot( playerInventory, playerInvCol + playerInvRow * 9 + 9, leftCol + playerInvCol * 18, YSIZE - (4 - playerInvRow) * 18 - 10));
			}
		}
		for( int hotbarSlot = 0; hotbarSlot < 9; ++hotbarSlot) {
			addSlot( new Slot( playerInventory, hotbarSlot, leftCol + hotbarSlot * 18, YSIZE - 24 ) );
		}
	}

	@Override
	public ItemStack quickMoveStack( PlayerEntity player, int index ) {
		// LOGGER.debug( "{}::quickMoveStack {} {}", getClass().getName(), player, index );
		var newStack = ItemStack.EMPTY;
		val slot = slots.get( index );
		if( slot != null && slot.hasItem() ) {
			val oldStack = slot.getItem();
			newStack = oldStack.copy();
			if( index == 0 ) {
				if( !moveItemStackTo( oldStack, 1, slots.size(), true ) ) {
					return ItemStack.EMPTY;
				}
			} else if( !moveItemStackTo( oldStack, 0, 1, false ) ) {
				return ItemStack.EMPTY;
			}
			if( oldStack.isEmpty() ) {
				slot.set( ItemStack.EMPTY );
			} else {
				slot.setChanged();
			}
		}
		return newStack;
	}

	@Override
	public boolean canDragTo( Slot slotIn ) {
		return false;
	}

	@Override
	public boolean canTakeItemForPickAll( ItemStack stack, Slot slotIn ) {
		return false;
	}

	@Override
	public boolean stillValid( PlayerEntity player ) {
		return _inventory.stillValid( player );
	}


}
