package net.error_unlimited.tdwtf_minecraft.inventory;

import lombok.extern.log4j.Log4j2;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;

@Log4j2
public class IllusionSlot extends Slot {
	public IllusionSlot( IInventory inventory, int slotIndex, int xPos, int yPos ) {
		super( inventory, slotIndex, xPos, yPos );
		LOGGER.debug( "{}::ctor", getClass().getName() );
	}

	@Override
	public boolean mayPlace( ItemStack stack ) {
		return stack.isEmpty() || stack.getItem() instanceof BlockItem;
	}
}
