package net.error_unlimited.tdwtf_minecraft.inventory;

import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.TdwtfMinecraftMod;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Log4j2
public class IllusionContainerType extends ContainerType<IllusionContainer> {
	public static final String REGISTRY_NAME = "illusion_container_type";
	public static final ResourceLocation RESOURCE_LOCATION = new ResourceLocation( TdwtfMinecraftMod.MOD_ID, REGISTRY_NAME );
	public static final IllusionContainerType CONTAINER_TYPE = new IllusionContainerType();
	private IllusionContainerType() {
		super( IllusionContainer::create );
		setRegistryName( RESOURCE_LOCATION );
	}

	@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
	public static class Registration {
		@SubscribeEvent
		public static void registerContainerType( final RegistryEvent.Register<ContainerType<?>> e ) {
			LOGGER.debug( "{}::registerContainerType", Registration.class.getName() );
			e.getRegistry().register( CONTAINER_TYPE );
		}
	}
}
