package net.error_unlimited.tdwtf_minecraft;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.IEventBus;

import java.util.function.Consumer;

import org.apache.logging.log4j.Logger;

@UtilityClass
@Log4j2
public final class Utils {
	public static <T extends Event> Observable<T> observeEvent( IEventBus eventBus, Class<T> filter ) {
		return Observable.create( ( ObservableEmitter<T> observer ) -> {
			Consumer<T> handler = ( ( T e ) -> { observer.onNext( e ); } );
			eventBus.addListener( EventPriority.NORMAL, false, filter, handler );
			observer.setCancellable( () -> {
				eventBus.unregister( handler );
			} );
		} );
	}

	public static <T> Observer<T> logObserver( final Logger logger, String prefix ) {
		return new Observer<T>() {
			@Override
			public void onSubscribe( Disposable d ) {
				logger.info( "{} {}", prefix, "subscribe" );
			}

			@Override
			public void onComplete() {
				logger.info( "{} {}", prefix, "complete" );
			}

			@Override
			public void onError( Throwable t ) {
				logger.error( "{} {} {}", prefix, "error", t );
			}

			@Override
			public void onNext( T o ) {
				logger.info( "{} {} {}", prefix, "next", o );
			}
		};
	}
}
