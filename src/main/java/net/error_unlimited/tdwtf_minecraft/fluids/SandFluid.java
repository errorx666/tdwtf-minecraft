package net.error_unlimited.tdwtf_minecraft.fluids;

import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.TdwtfMinecraftMod;
import net.error_unlimited.tdwtf_minecraft.items.SandBucketItem;
import net.error_unlimited.tdwtf_minecraft.particles.SandDrippingParticleType;
import net.minecraft.block.Block;
import net.minecraft.block.FlowingFluidBlock;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.block.material.PushReaction;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.IFluidState;
import net.minecraft.particles.IParticleData;
import net.minecraft.state.StateContainer;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fluids.FluidAttributes;
import net.minecraftforge.fluids.ForgeFlowingFluid;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ForgeRegistries;

@Log4j2
public abstract class SandFluid extends ForgeFlowingFluid {
	public static final SandFluid FLOWING = Flowing.FLUID;
	public static final SandFluid SOURCE = Source.FLUID;
	public static final Material MATERIAL =
		new Material(
			MaterialColor.SAND, // color
			true, // liquid
			false, // solid
			false, // blocksMotion
			false, // solidBlocking
			false, // isAlwaysDestroyable
			false, // flammable
			true, // replaceable
			PushReaction.DESTROY // pushReaction
		);
	public static final FlowingFluidBlock BLOCK = new FlowingFluidBlock( () -> SOURCE, Block.Properties.of( MATERIAL ).noCollission().strength( 100.f ).noDrops() );
	static {
		BLOCK.setRegistryName( TdwtfMinecraftMod.MOD_ID, "flowing_sand_block" );
	}

	protected SandFluid() {
		super(
			new Properties(
				() -> SOURCE,
				() -> FLOWING,
				FluidAttributes.builder(
					new ResourceLocation( TdwtfMinecraftMod.MOD_ID, "fluids/sand_still" ),
					new ResourceLocation( TdwtfMinecraftMod.MOD_ID, "fluids/sand_flow" )
				)
				.overlay( new ResourceLocation( TdwtfMinecraftMod.MOD_ID, "fluids/sand_overlay" ) )
				.translationKey( "fluid.tdwtf_minecraft.sand_fluid" )
				.luminosity( 0 )
				.density( 1000 )
				.viscosity( 3000 )
				.temperature( 300 )
			)
			.explosionResistance( 100.f )
			.canMultiply()
			.block( () -> BLOCK )
			.bucket( () -> SandBucketItem.ITEM )
			.levelDecreasePerBlock( 1 )
			.slopeFindDistance( 4 )
			.tickRate( 15 )
		);
		LOGGER.debug( "{}::ctor", getClass().getName() );
	}

	@OnlyIn(Dist.CLIENT)
	public IParticleData getDripParticle() {
		return SandDrippingParticleType.PARTICLE_TYPE;
	}

	@Override
	protected boolean canBeReplacedWith( IFluidState state, IBlockReader world, BlockPos pos, Fluid fluidIn, Direction direction ) {
		return super.canBeReplacedWith( state, world, pos, fluidIn, direction );
	}

	private static class Flowing extends SandFluid {
		public static final String REGISTRY_NAME = "flowing_sand_fluid";
		public static final ResourceLocation RESOURCE_LOCATION = new ResourceLocation( TdwtfMinecraftMod.MOD_ID, REGISTRY_NAME );
		public static final RegistryObject<SandFluid> REGISTRY_OBJECT = RegistryObject.of( RESOURCE_LOCATION, ForgeRegistries.FLUIDS );
		public static final Flowing FLUID = new Flowing();

		private Flowing() {
			LOGGER.debug( "{}::ctor", getClass().getName() );
			registerDefaultState( getStateDefinition().any().setValue( LEVEL, 7 ) );
			setRegistryName( RESOURCE_LOCATION );
		}

		protected void createFluidStateDefinition( StateContainer.Builder<Fluid, IFluidState> builder ) {
			super.createFluidStateDefinition( builder );
			builder.add( LEVEL );
		}

		@Override
		public int getAmount( IFluidState fluidState ) {
			return fluidState.getValue( LEVEL );
		}

		@Override
		public boolean isSource( IFluidState fluidState ) {
			return false;
		}

		@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
		public static class Registration {
			@SubscribeEvent
			public static void registerFluid( final RegistryEvent.Register<Fluid> e ) {
				LOGGER.debug( "{}::registerFluid", Registration.class.getName() );
				e.getRegistry().register( FLUID );
			}
		}
	}

	private static class Source extends SandFluid {
		public static final String REGISTRY_NAME = "sand_fluid";
		public static final ResourceLocation RESOURCE_LOCATION = new ResourceLocation( TdwtfMinecraftMod.MOD_ID, REGISTRY_NAME );
		public static final RegistryObject<SandFluid> REGISTRY_OBJECT = RegistryObject.of( RESOURCE_LOCATION, ForgeRegistries.FLUIDS );
		public static final Source FLUID = new Source();

		private Source() {
			LOGGER.debug( "{}::ctor", getClass().getName() );
			setRegistryName( RESOURCE_LOCATION );
		}

		@Override
		public int getAmount( IFluidState fluidState ) {
			return 8;
		}

		@Override
		public boolean isSource( IFluidState fluidState ) {
			return true;
		}

		@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
		public static class Registration {
			@SubscribeEvent
			public static void registerFluid( final RegistryEvent.Register<Fluid> e ) {
				LOGGER.debug( "{}::registerFluid", Registration.class.getName() );
				e.getRegistry().register( FLUID );
			}
		}
	}

	@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
	public static class Registration {
		@SubscribeEvent
		public static void registerBlock( final RegistryEvent.Register<Block> e ) {
			LOGGER.debug( "{}::registerBlock", Registration.class.getName() );
			e.getRegistry().register( BLOCK );
		}
	}
}
