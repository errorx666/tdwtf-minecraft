package net.error_unlimited.tdwtf_minecraft.rules;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.reactivex.rxjava3.core.Observable;
import lombok.Getter;
import lombok.ToString;
import lombok.val;
import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.Region;
import net.error_unlimited.tdwtf_minecraft.TdwtfMinecraftMod;
import net.error_unlimited.tdwtf_minecraft.Utils;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.CommandEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLDedicatedServerSetupEvent;

@OnlyIn(Dist.DEDICATED_SERVER)
@Log4j2
public class NoCommandRule extends Rule<NoCommandRule.Config> {
	public static final String NAME = "no-command";

	public NoCommandRule( Config config ) {
		super( NAME, config );
	}

	@Override
	public void apply( Collection<Region> regions ) {
		val config = getConfig();
		LOGGER.debug( "{}::apply {}", getClass().getName(), config );
		val exemptPlayers = config.getExemptPlayers();
		val event$ = Utils.observeEvent( MinecraftForge.EVENT_BUS, CommandEvent.class );
		enabled$
		.distinctUntilChanged()
		.switchMap( e -> e ? event$ : Observable.empty() )
		.filter( e -> {
			val entity = e.getParseResults().getContext().getSource().getEntity();
			if( entity instanceof PlayerEntity ) {
				val playerEntity = (PlayerEntity)entity;
				if( exemptPlayers.contains( playerEntity.getScoreboardName() ) ) return false;
			}
			if( !regions.stream().anyMatch( r -> r.isInside( entity ) ) ) return false;
			return true;
		} )
		.subscribe( e -> {
			e.setCanceled( true );
		} );
	}

	@ToString
	public static class Config {
		@JsonCreator
		public Config( @JsonProperty Set<String> exemptPlayers ) {
			if( exemptPlayers == null ) exemptPlayers = Collections.emptySet();
			_exemptPlayers = exemptPlayers;
		}

		@Getter
		@ToString.Include( name = "exemptPlayers" )
		private final Set<String> _exemptPlayers;
	}

	@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD, value = { Dist.DEDICATED_SERVER })
	@OnlyIn(Dist.DEDICATED_SERVER)
	public static class Registration {
		@SubscribeEvent( priority = EventPriority.HIGH )
		public static void loadRegistry( FMLDedicatedServerSetupEvent e ) {
			LOGGER.debug( "{}::loadRegistry", Registration.class.getName() );
			Rules.INSTANCE.registerRule( NAME, ( Config c ) -> new NoCommandRule( c ) );
		}
	}
}
