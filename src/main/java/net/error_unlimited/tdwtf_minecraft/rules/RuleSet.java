package net.error_unlimited.tdwtf_minecraft.rules;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.Getter;
import lombok.val;
import net.error_unlimited.tdwtf_minecraft.Region;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.DEDICATED_SERVER)
public class RuleSet {
	public RuleSet( String name ) {
		_name = name;
	}

	@Getter
	private final String _name;
	private final Set<Rule<?>> _rules = new HashSet<>();
	private final List<Region> _regions = new ArrayList<>();

	public void addRule( Rule<?> rule ) {
		_rules.add( rule );
	}

	public void addRegion( Region region ) {
		_regions.add( region );
	}

	public void apply() {
		for( val rule : _rules ) {
			rule.apply( _regions );
		}
	}

	@Override
	public String toString() {
		return "RuleSet[" + _name + "]";
	}
}
