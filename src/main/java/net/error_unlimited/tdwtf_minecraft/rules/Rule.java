package net.error_unlimited.tdwtf_minecraft.rules;

import java.util.Collection;

import io.reactivex.rxjava3.subjects.BehaviorSubject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.error_unlimited.tdwtf_minecraft.Region;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@AllArgsConstructor
@OnlyIn(Dist.DEDICATED_SERVER)
public abstract class Rule<TConfig> {
	@Getter
	private final String _name;

	@Getter
	private final TConfig _config;

	protected final BehaviorSubject<Boolean> enabled$ = BehaviorSubject.createDefault( true );

	public boolean isEnabled() {
		return enabled$.getValue();
	}

	public void enable() {
		if( !enabled$.getValue() ) enabled$.onNext( true );
	}

	public void disable() {
		if( enabled$.getValue() ) enabled$.onNext( false );
	}

	public void remove() {
		enabled$.onNext( false );
		enabled$.onComplete();
	}

	public abstract void apply( Collection<Region> regions );

	@Override
	public String toString() {
		return "Rule[" + getName() + "]";
	}
}
