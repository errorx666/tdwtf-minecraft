package net.error_unlimited.tdwtf_minecraft.rules;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Sets;

import io.reactivex.rxjava3.core.Observable;
import lombok.Getter;
import lombok.ToString;
import lombok.val;
import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.Region;
import net.error_unlimited.tdwtf_minecraft.TdwtfMinecraftMod;
import net.error_unlimited.tdwtf_minecraft.Utils;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.eventbus.api.Event.Result;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLDedicatedServerSetupEvent;

@OnlyIn(Dist.DEDICATED_SERVER)
@Log4j2
public class NoSpawnRule extends Rule<NoSpawnRule.Config> {
	public static final String NAME = "no-spawn";

	public NoSpawnRule( Config config ) {
		super( NAME, config );
	}

	@Override
	public void apply( Collection<Region> regions ) {
		val config = getConfig();
		LOGGER.debug( "{}::apply {}", getClass().getName(), config );

		val event$ = Utils.observeEvent( MinecraftForge.EVENT_BUS, EntityJoinWorldEvent.class );
		val entities = config.getEntities();
		val isInvertEntities = config.isInvertEntities();
		val tags = config.getTags();
		val isInvertTags = config.isInvertTags();

		enabled$
		.distinctUntilChanged()
		.switchMap( e -> e ? event$ : Observable.empty() )
		.filter( e -> {
			val entity = e.getEntity();
			if( entity == null ) return false;
			if( entity instanceof PlayerEntity ) return false;
			if( !regions.stream().anyMatch( r -> r.isInside( entity ) ) ) return false;
			val entityType = entity.getType();
			if( ( !entities.contains( entityType.getRegistryName() ) ^ isInvertEntities )
			&& ( Sets.intersection( tags, entityType.getTags() ).isEmpty() ^ isInvertTags ) ) return false;
			return true;
		} )
		.subscribe( e -> {
			e.setResult( Result.DENY );
			e.setCanceled( true );
		} );
	}

	@ToString
	public static class Config {
		@JsonCreator
		public Config( @JsonProperty( "entities" ) Set<ResourceLocation> entities, @JsonProperty( "tags" ) Set<ResourceLocation> tags ) {
			if( entities == null ) entities = Collections.emptySet();
			_entities = Collections.unmodifiableSet( entities );
			if( tags == null ) tags = Collections.emptySet();
			_tags = Collections.unmodifiableSet( tags );
		}

		@Getter
		@JsonProperty( "invertEntities" )
		private boolean _invertEntities;

		@Getter
		@ToString.Include( name = "entities" )
		private Set<ResourceLocation> _entities;

		@Getter
		@JsonProperty( "invertTags" )
		private boolean _invertTags;

		@Getter
		@ToString.Include( name = "tags" )
		private Set<ResourceLocation> _tags;
	}

	@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD, value = { Dist.DEDICATED_SERVER })
	@OnlyIn(Dist.DEDICATED_SERVER)
	public static class Registration {
		@SubscribeEvent( priority = EventPriority.HIGH )
		public static void loadRegistry( FMLDedicatedServerSetupEvent e ) {
			LOGGER.debug( "{}::loadRegistry", Registration.class.getName() );
			Rules.INSTANCE.registerRule( NAME, ( Config c ) -> new NoSpawnRule( c ) );
		}
	}
}
