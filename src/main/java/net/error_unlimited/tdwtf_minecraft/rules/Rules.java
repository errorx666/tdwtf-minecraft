package net.error_unlimited.tdwtf_minecraft.rules;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import org.apache.commons.lang3.concurrent.ConcurrentException;
import org.apache.commons.lang3.concurrent.LazyInitializer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.val;
import lombok.extern.log4j.Log4j2;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.DEDICATED_SERVER)
@Log4j2
public class Rules {
	public static final Rules INSTANCE = new Rules();
	private final Map<String, RuleSupplier<?>> _ruleSuppliers = new HashMap<String, RuleSupplier<?>>();

	public <TConfig> void registerRule( String name, RuleSupplier<?> ruleSupplier ) {
		LOGGER.debug( "{}::registerRule {}", getClass().getName(), name );
		_ruleSuppliers.put( name, ruleSupplier );
	}

	@AllArgsConstructor
	private final class LazyRuleSupplier<TConfig> extends LazyInitializer<Rule<TConfig>> {
		@Getter
		private final String _name;

		@Getter
		private final TConfig _config;

		@Override
		@SuppressWarnings("unchecked")
		public Rule<TConfig> initialize() {
			val supplier = (RuleSupplier<TConfig>)_ruleSuppliers.get( _name );
			val rule = supplier.getRule( _config );
			return rule;
		}
	}

	public <TConfig> Supplier<Rule<?>> getRuleSupplier( String name, TConfig config ) {
		val lazy = new LazyRuleSupplier<TConfig>( name, config );
		return () -> {
			try {
				return lazy.get();
			} catch( ConcurrentException ex ) {
				throw new RuntimeException( ex );
			}
		};
	}

	@Override
	public String toString() {
		return "[Rules " + String.join( ", ", _ruleSuppliers.keySet() ) + "]";
	}
}
