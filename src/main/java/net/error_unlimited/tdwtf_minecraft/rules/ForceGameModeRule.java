package net.error_unlimited.tdwtf_minecraft.rules;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.reactivex.rxjava3.core.Observable;
import lombok.Getter;
import lombok.ToString;
import lombok.val;
import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.Region;
import net.error_unlimited.tdwtf_minecraft.TdwtfMinecraftMod;
import net.error_unlimited.tdwtf_minecraft.Utils;
import net.minecraft.world.GameType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.TickEvent.WorldTickEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLDedicatedServerSetupEvent;

@OnlyIn(Dist.DEDICATED_SERVER)
@Log4j2
public class ForceGameModeRule extends Rule<ForceGameModeRule.Config> {
	public static final String NAME = "force-game-mode";

	public ForceGameModeRule( Config config ) {
		super( NAME, config );
	}

	@Override
	public void apply( Collection<Region> regions ) {
		val config = getConfig();
		LOGGER.debug( "{}::apply {}", getClass().getName(), config );

		val gameMode = GameType.byName( config.getGameMode() );
		val event$ = Utils.observeEvent( MinecraftForge.EVENT_BUS, WorldTickEvent.class );

		val exemptPlayers = config.getExemptPlayers();

		enabled$
		.distinctUntilChanged()
		.switchMap( e -> e ? event$ : Observable.empty() )
		.throttleFirst( 1l, TimeUnit.SECONDS )
		.subscribe( e -> {
			for( val player : e.world.players() ) {
				if( exemptPlayers.contains( player.getScoreboardName() ) ) continue;
				if( !regions.stream().anyMatch( r -> r.isInside( player ) ) ) continue;
				player.setGameMode( gameMode );
			}
		} );
	}

	@ToString
	public static class Config {
		@JsonCreator
		public Config( @JsonProperty String gameMode, @JsonProperty Set<String> exemptPlayers ) {
			_gameMode = gameMode;
			if( exemptPlayers == null ) exemptPlayers = Collections.emptySet();
			_exemptPlayers = Collections.unmodifiableSet( exemptPlayers );
		}

		@Getter
		@ToString.Include( name = "gamemode" )
		private final String _gameMode;

		@Getter
		@ToString.Include( name = "exemptPlayers" )
		private final Set<String> _exemptPlayers;
	}

	@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD, value = { Dist.DEDICATED_SERVER })
	@OnlyIn(Dist.DEDICATED_SERVER)
	public static class Registration {
		@SubscribeEvent( priority = EventPriority.HIGH )
		public static void loadRegistry( FMLDedicatedServerSetupEvent e ) {
			LOGGER.debug( "{}::loadRegistry", Registration.class.getName() );
			Rules.INSTANCE.registerRule( NAME, ( Config c ) -> new ForceGameModeRule( c ) );
		}
	}
}
