package net.error_unlimited.tdwtf_minecraft.rules;

@FunctionalInterface
public interface RuleSupplier<TConfig> {
	Rule<TConfig> getRule( TConfig config );
}
