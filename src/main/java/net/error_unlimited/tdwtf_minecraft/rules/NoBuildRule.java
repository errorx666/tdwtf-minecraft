package net.error_unlimited.tdwtf_minecraft.rules;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Sets;

import io.reactivex.rxjava3.core.Observable;
import lombok.Getter;
import lombok.ToString;
import lombok.val;
import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.Region;
import net.error_unlimited.tdwtf_minecraft.TdwtfMinecraftMod;
import net.error_unlimited.tdwtf_minecraft.Utils;
import net.error_unlimited.tdwtf_minecraft.events.TdwtfMinecraftEventAdapter;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.eventbus.api.Event.Result;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLDedicatedServerSetupEvent;

@OnlyIn(Dist.DEDICATED_SERVER)
@Log4j2
public class NoBuildRule extends Rule<NoBuildRule.Config> {
	public static final String NAME = "no-build";

	public NoBuildRule( Config config ) {
		super( NAME, config );
	}

	@Override
	public void apply( Collection<Region> regions ) {
		val config = getConfig();
		LOGGER.debug( "{}::apply {}", getClass().getName(), config );
		val event$ = Observable.merge(
			Utils.observeEvent( MinecraftForge.EVENT_BUS, BlockEvent.EntityPlaceEvent.class ),
			Utils.observeEvent( MinecraftForge.EVENT_BUS, BlockEvent.FluidPlaceBlockEvent.class )
		).map( TdwtfMinecraftEventAdapter::adapt );

		val blocks = config.getBlocks();
		val isInvertBlocks = config.isInvertBlocks();
		val tags = config.getTags();
		val isInvertTags = config.isInvertTags();
		val exemptPlayers = config.getExemptPlayers();

		enabled$
		.distinctUntilChanged()
		.switchMap( e -> e ? event$ : Observable.empty() )
		.filter( e -> {
			val playerEntity = e.getPlayerEntity();
			if( playerEntity != null && ( playerEntity.isCreative() || exemptPlayers.contains( playerEntity.getScoreboardName() ) ) ) return false;
			if( !regions.stream().anyMatch( r -> r.isInside( e.getBlockPos(), e.getDimensionType() ) ) ) return false;
			val block = e.getBlock();
			if( block == null ) return false;
			if( ( !blocks.contains( block.getRegistryName() ) ^ isInvertBlocks )
			&& ( Sets.intersection( tags, block.getTags() ).isEmpty() ^ isInvertTags ) ) return false;
			return true;
		} )
		.subscribe( e -> {
			e.setResult( Result.DENY );
			e.setCanceled( true );
		} );
	}

	@ToString
	public static class Config {
		@JsonCreator
		public Config( @JsonProperty Set<ResourceLocation> blocks, @JsonProperty Set<ResourceLocation> tags, @JsonProperty Set<String> exemptPlayers ) {
			if( blocks == null ) blocks = Collections.emptySet();
			_blocks = Collections.unmodifiableSet( blocks );
			if( tags == null ) tags = Collections.emptySet();
			_tags = Collections.unmodifiableSet( tags );
			if( exemptPlayers == null ) exemptPlayers = Collections.emptySet();
			_exemptPlayers = Collections.unmodifiableSet( exemptPlayers );
		}

		@Getter
		@JsonProperty( "invertBlocks" )
		private boolean _invertBlocks;

		@Getter
		@ToString.Include( name = "blocks" )
		private Set<ResourceLocation> _blocks;

		@Getter
		@JsonProperty( "invertTags" )
		private boolean _invertTags;

		@Getter
		@ToString.Include( name = "tags" )
		private Set<ResourceLocation> _tags;

		@Getter
		@ToString.Include( name = "exemptPlayers" )
		private Set<String> _exemptPlayers;
	}

	@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD, value = { Dist.DEDICATED_SERVER })
	@OnlyIn(Dist.DEDICATED_SERVER)
	public static class Registration {
		@SubscribeEvent( priority = EventPriority.HIGH )
		public static void loadRegistry( FMLDedicatedServerSetupEvent e ) {
			LOGGER.debug( "{}::loadRegistry", Registration.class.getName() );
			Rules.INSTANCE.registerRule( NAME, ( Config c ) -> new NoBuildRule( c ) );
		}
	}
}
