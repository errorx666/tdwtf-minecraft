package net.error_unlimited.tdwtf_minecraft;

import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.EntityViewRenderEvent.FogColors;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.eventbus.api.Event.Result;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.loading.FMLEnvironment;
import net.minecraftforge.fml.loading.FMLPaths;
import lombok.Getter;
import lombok.val;
import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.fluids.SandFluid;
import net.minecraft.client.Minecraft;

import java.util.function.Supplier;

@Log4j2
@OnlyIn(Dist.CLIENT)
public class TdwtfMinecraftModClient {
	@Getter
	private TdwtfMinecraftModClientConfig _config;
	@Getter
	private Minecraft _minecraft;

	public TdwtfMinecraftModClient( TdwtfMinecraftModClientConfig config, Supplier<Minecraft> minecraftSupplier ) {
		LOGGER.debug( "{}::ctor", getClass().getName() );
		assert( FMLEnvironment.dist == Dist.CLIENT );
		_config = config;
		_minecraft = minecraftSupplier.get();
		assert( _config != null );
		assert( _minecraft != null );
	}

	public void run() {
		LOGGER.debug( "{}::run", getClass().getName() );

		Utils.observeEvent( MinecraftForge.EVENT_BUS, FogColors.class )
		.filter( e -> e.getInfo().getFluidInCamera().getType().isSame( SandFluid.SOURCE ) )
		.subscribe( e -> {
			e.setRed( 0xd5 );
			e.setGreen( 0xc9 );
			e.setBlue( 0xa7 );
			e.setResult( Result.ALLOW );
		} );
	}

	@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD, value = { Dist.CLIENT })
	@OnlyIn(Dist.CLIENT)
	public static class Registration {
		@SubscribeEvent
		public static void registerClientSetup( FMLClientSetupEvent e ) {
			LOGGER.debug( "{}::registerClientSetup", Registration.class.getName() );
			val config = TdwtfMinecraftModClientConfig.loadConfig( FMLPaths.CONFIGDIR.get().resolve( "tdwtf-client.toml" ) );
			val client = new TdwtfMinecraftModClient( config, e.getMinecraftSupplier() );
			client.run();
			val shared = new TdwtfMinecraftModShared( config );
			shared.run();
		}
	}
}
