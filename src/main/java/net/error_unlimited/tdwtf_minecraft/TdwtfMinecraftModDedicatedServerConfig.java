package net.error_unlimited.tdwtf_minecraft;

import com.electronwill.nightconfig.core.CommentedConfig;
import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import com.electronwill.nightconfig.core.io.WritingMode;

import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.loading.FMLEnvironment;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.val;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Log4j2
@ToString(callSuper=true)
@EqualsAndHashCode(callSuper=true)
public final class TdwtfMinecraftModDedicatedServerConfig extends TdwtfMinecraftModSharedConfig {
	@Override
	protected void buildSpec( ForgeConfigSpec.Builder builder ) {
		super.buildSpec( builder );
		builder.push( "errorBot" );
		builder.define( "enabled", false );
		builder.define( "logJetty", false );
		builder.defineList( "apiKeys", new ArrayList<String>(), o -> o instanceof String );
		builder.defineInRange( "port", DEFAULT_ERROR_BOT_PORT, 1, 65535 );
		builder.pop();

		builder.push( "ftpServer" );
		builder.define( "enabled", false );
		builder.defineInRange( "port", DEFAULT_FTP_SERVER_PORT, 1, 65535 );
		builder.define( "passivePorts", DEFAULT_FTP_SERVER_PASSIVE_PORTS );
		builder.define( "password", DEFAULT_FTP_SERVER_PASSWORD );
		builder.pop();

		val pyramid = new Region( "pyramid", DimensionType.OVERWORLD );
		pyramid.include( new AxisAlignedBB( -434.d, 137.d, 548.d, -310.d, 202.d, 671.d ) );
		builder.define( "regions",
			Json.stringify( new Region[] { pyramid } ),
			o -> o instanceof String
		);
	}

	@ToString.Exclude
	private final ForgeConfigSpec _configSpec;
	private static final int DEFAULT_ERROR_BOT_PORT = 32101;
	private static final int DEFAULT_FTP_SERVER_PORT = 32102;
	private static final String DEFAULT_FTP_SERVER_PASSIVE_PORTS = "32103-32150";
	private static final String DEFAULT_FTP_SERVER_PASSWORD = "hunter2";

	private TdwtfMinecraftModDedicatedServerConfig( CommentedFileConfig config ) {
		super();
		LOGGER.debug( "{}::ctor", getClass().getName() );
		assert( FMLEnvironment.dist == Dist.DEDICATED_SERVER );
		val builder = new ForgeConfigSpec.Builder();
		buildSpec( builder );
		_configSpec = builder.build();
	}

	@Override
	protected void loadConfig( CommentedConfig config ) {
		_configSpec.setConfig( config );
		super.loadConfig( config );
		_errorBotEnabled = config.get( "errorBot.enabled" );
		_errorBotLogJetty = config.get( "errorBot.logJetty" );
		_errorBotPort = config.getInt( "errorBot.port" );
		_errorBotApiKeys = new HashSet<String>(
			config.get( "errorBot.apiKeys" )
		);
		_ftpServerEnabled = config.get( "ftpServer.enabled" );
		_ftpServerPassword = config.get( "ftpServer.password" );
		_ftpServerPort = config.getInt( "ftpServer.port" );
		_ftpServerPassivePorts = config.get( "ftpServer.passivePorts" );
		for( val region : Json.parseList( config.get( "regions" ), Region.class ) ) {
			_regions.put( region.getName(), region );
		}
		LOGGER.debug( "{}::loadConfig {}", getClass().getName(), this );
	}

	@Getter
	private boolean _errorBotLogJetty;

	@Getter
	private boolean _errorBotEnabled;

	@Getter
	private int _errorBotPort;

	@Getter
	@ToString.Exclude
	private Set<String> _errorBotApiKeys;

	@Getter
	private boolean _ftpServerEnabled;

	@Getter
	private int _ftpServerPort;

	@Getter
	private String _ftpServerPassivePorts;

	@Getter
	@ToString.Exclude
	private String _ftpServerPassword;

	private Map<String, Region> _regions = new HashMap<>();

	public Region getRegion( String name ) {
		return _regions.get( name );
	}

	public static TdwtfMinecraftModDedicatedServerConfig loadConfig( Path path ) {
		try( final CommentedFileConfig configData =
				CommentedFileConfig.builder( path )
				.sync()
				.autosave()
				.writingMode( WritingMode.REPLACE )
				.build() ) {
			configData.load();
			val config = new TdwtfMinecraftModDedicatedServerConfig( configData );
			config.loadConfig( configData );
			return config;
		}
	}
}
