package net.error_unlimited.tdwtf_minecraft;

import com.electronwill.nightconfig.core.CommentedConfig;
import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import com.electronwill.nightconfig.core.io.WritingMode;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.fml.loading.FMLEnvironment;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.val;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;

@Log4j2
@ToString(callSuper=true)
@EqualsAndHashCode(callSuper=true)
public final class TdwtfMinecraftModClientConfig extends TdwtfMinecraftModSharedConfig {
	@Override
	protected void buildSpec( ForgeConfigSpec.Builder builder ) {
		super.buildSpec( builder );
		LOGGER.debug( "{}::buildSpec", getClass().getName() );
	}

	@ToString.Exclude
	private final ForgeConfigSpec _configSpec;

	private TdwtfMinecraftModClientConfig( CommentedFileConfig config ) {
		super();
		LOGGER.debug( "{}::ctor", getClass().getName() );
		assert( FMLEnvironment.dist == Dist.CLIENT );
		val builder = new ForgeConfigSpec.Builder();
		buildSpec( builder );
		_configSpec = builder.build();
		loadConfig( config );
	}

	@Override
	protected void loadConfig( CommentedConfig config ) {
		_configSpec.setConfig( config );
		super.loadConfig( config );
		LOGGER.debug( "{}::loadConfig {}", getClass().getName(), this );
	}

	public static TdwtfMinecraftModClientConfig loadConfig( Path path ) {
		try( val configData =
				CommentedFileConfig.builder( path )
				.sync()
				.autosave()
				.writingMode( WritingMode.REPLACE )
				.build() ) {
			configData.load();
			val config = new TdwtfMinecraftModClientConfig( configData );
			config.loadConfig( configData );
			return config;
		}
	}
}
