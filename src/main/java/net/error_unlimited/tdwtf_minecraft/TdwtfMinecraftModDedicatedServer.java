package net.error_unlimited.tdwtf_minecraft;

import lombok.Getter;
import lombok.val;
import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.errorBot.ErrorBotServer;
import net.error_unlimited.tdwtf_minecraft.errorBot.ErrorBotServerOptions;
import net.error_unlimited.tdwtf_minecraft.errorBot.messages.ErrorBotAdvancementMessage;
import net.error_unlimited.tdwtf_minecraft.errorBot.messages.ErrorBotLivingDeathMessage;
import net.error_unlimited.tdwtf_minecraft.errorBot.messages.ErrorBotPlayerLoggedInMessage;
import net.error_unlimited.tdwtf_minecraft.errorBot.messages.ErrorBotPlayerLoggedOutMessage;
import net.error_unlimited.tdwtf_minecraft.errorBot.messages.ErrorBotResponseMessage;
import net.error_unlimited.tdwtf_minecraft.errorBot.messages.ErrorBotServerChatMessage;
import net.error_unlimited.tdwtf_minecraft.ftp.FtpServerAPI;
import net.error_unlimited.tdwtf_minecraft.ftp.TdwtfMinecraftFileSystemFactory;
import net.error_unlimited.tdwtf_minecraft.ftp.TdwtfMinecraftRootFtpFile;
import net.error_unlimited.tdwtf_minecraft.ftp.TdwtfMinecraftUserManager;
import net.error_unlimited.tdwtf_minecraft.rules.ForceGameModeRule;
import net.error_unlimited.tdwtf_minecraft.rules.NoBuildRule;
import net.error_unlimited.tdwtf_minecraft.rules.NoCommandRule;
import net.error_unlimited.tdwtf_minecraft.rules.NoDestroyRule;
import net.error_unlimited.tdwtf_minecraft.rules.NoSpawnRule;
import net.error_unlimited.tdwtf_minecraft.rules.RuleSet;
import net.error_unlimited.tdwtf_minecraft.rules.Rules;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.dedicated.DedicatedServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ServerChatEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.player.AdvancementEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.PlayerLoggedOutEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLDedicatedServerSetupEvent;
import net.minecraftforge.fml.loading.FMLEnvironment;
import net.minecraftforge.fml.loading.FMLPaths;

import java.util.function.Supplier;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.impl.DefaultDataConnectionConfiguration;
import org.apache.ftpserver.impl.PassivePorts;
import org.apache.ftpserver.listener.ListenerFactory;
import org.apache.ftpserver.usermanager.impl.BaseUser;
import org.apache.ftpserver.usermanager.impl.ConcurrentLoginPermission;
import org.apache.ftpserver.usermanager.impl.TransferRatePermission;
import org.apache.ftpserver.usermanager.impl.WritePermission;

import dan200.computercraft.api.ComputerCraftAPI;

@Log4j2
public class TdwtfMinecraftModDedicatedServer {
	@Getter
	private final TdwtfMinecraftModDedicatedServerConfig _config;

	@Getter
	private final DedicatedServer _dedicatedServer;

	public TdwtfMinecraftModDedicatedServer( TdwtfMinecraftModDedicatedServerConfig config, Supplier<DedicatedServer> dedicatedServerSupplier ) {
		LOGGER.debug( "{}::ctor", getClass().getName() );
		assert( FMLEnvironment.dist == Dist.DEDICATED_SERVER );
		_config = config;
		_dedicatedServer = dedicatedServerSupplier.get();
		assert( _config != null );
		assert( _dedicatedServer != null );
	}

	public void run() {
		LOGGER.debug( "{}::run", getClass().getName() );
		if( _config.isErrorBotEnabled() ) {
			startErrorBot();
		}
		if( _config.isFtpServerEnabled() ) {
			startFtpServer();
		}

		LOGGER.debug( "{}::run rules {}", getClass().getName(), Rules.INSTANCE );

		val pyramid = _config.getRegion( "pyramid" );
		assert( pyramid != null );
		LOGGER.debug( "{}::run pyramid {}", getClass().getName(), pyramid );

		val ruleSet = new RuleSet( "test" );
		ruleSet.addRegion( pyramid );

		ruleSet.addRule( Rules.INSTANCE.getRuleSupplier( NoBuildRule.NAME, new NoBuildRule.Config(
			Sets.newHashSet(
				new ResourceLocation( "minecraft", "chiseled_sandstone" ),
				new ResourceLocation( "minecraft", "sand" ),
				new ResourceLocation( "minecraft", "sandstone" ),
				new ResourceLocation( "minecraft", "sandstone_slab" ),
				new ResourceLocation( "minecraft", "sandstone_stairs" ),
				new ResourceLocation( "minecraft", "sandstone_wall" )
			), // entities
			Sets.newHashSet( new ResourceLocation( TdwtfMinecraftMod.MOD_ID, "light_source" ) ), // tags
			null // exemptPlayers
		) ).get() );
		ruleSet.addRule( Rules.INSTANCE.getRuleSupplier( NoDestroyRule.NAME, new NoDestroyRule.Config(
			Sets.newHashSet(
				new ResourceLocation( "minecraft", "chiseled_sandstone" ),
				new ResourceLocation( "minecraft", "sand" ),
				new ResourceLocation( "minecraft", "sandstone" ),
				new ResourceLocation( "minecraft", "sandstone_slab" ),
				new ResourceLocation( "minecraft", "sandstone_stairs" ),
				new ResourceLocation( "minecraft", "sandstone_wall" )
			), // entities
			Sets.newHashSet( new ResourceLocation( TdwtfMinecraftMod.MOD_ID, "light_source" ) ), // tags
			null // exemptPlayers
		) ).get() );
		ruleSet.addRule( Rules.INSTANCE.getRuleSupplier( NoSpawnRule.NAME, new NoSpawnRule.Config(
			Sets.newHashSet(
				new ResourceLocation( "minecraft", "creeper" ),
				new ResourceLocation( "minecraft", "phantom" )
			), // entities
			null // tags
		) ).get() );
		ruleSet.addRule( Rules.INSTANCE.getRuleSupplier( ForceGameModeRule.NAME, new ForceGameModeRule.Config(
			"survival",
			Sets.newHashSet( "errorx666" ) // exemptPlayers
		) ).get() );
		ruleSet.addRule( Rules.INSTANCE.getRuleSupplier( NoCommandRule.NAME, new NoCommandRule.Config(
			Sets.newHashSet( "errorx666" ) // exemptPlayers
		) ).get() );
		LOGGER.debug( "{}::run ruleSet {}", getClass().getName(), ruleSet );
		ruleSet.apply();
	}

	private void startFtpServer() {
		LOGGER.debug( "{}::startFtpServer", getClass().getName() );
		val rootFile = new TdwtfMinecraftRootFtpFile();
		try {
			val serverFactory = new FtpServerFactory();
			val factory = new ListenerFactory();
			factory.setPort( _config.getFtpServerPort() );
			factory.setDataConnectionConfiguration( new DefaultDataConnectionConfiguration(
				0, // idleTime
				null, // ssl
				true, // activeEnabled
				false, // activeIpCheck
				null, // activeLocalADdress
				0, // activeLocalPort
				null, // passiveAddress
				new PassivePorts( _config.getFtpServerPassivePorts(), true ), // passivePorts
				null, // passiveExternalAddress
				false, // passiveIpCheck
				false // implicitSsl
			) );
			val root = new BaseUser();
			root.setName( "root" );
			root.setPassword( _config.getFtpServerPassword() );
			root.setEnabled( true );
			root.setHomeDirectory( "/" );
			root.setAuthorities( Lists.newArrayList(
				new ConcurrentLoginPermission( Integer.MAX_VALUE, Integer.MAX_VALUE ),
				new TransferRatePermission( Integer.MAX_VALUE, Integer.MAX_VALUE ),
				new WritePermission()
			) );
			val userManager = new TdwtfMinecraftUserManager();
			userManager.save( root );
			serverFactory.setUserManager( userManager );
			serverFactory.addListener( "default", factory.createListener() );
			serverFactory.setFileSystem( new TdwtfMinecraftFileSystemFactory( rootFile ) );
			val server = serverFactory.createServer();
			server.start();
		} catch( FtpException ex ) {
			throw new RuntimeException( ex );
		}

		ComputerCraftAPI.registerAPIFactory( computerSystem -> new FtpServerAPI( computerSystem, rootFile ) );
	}

	private void startErrorBot() {
		LOGGER.debug( "{}::startErrorBot", getClass().getName() );
		val worker = ErrorBotServer.SCHEDULER.createWorker();
		worker.schedule( () -> {
			LOGGER.debug( "{}::startErrorBot::worker", getClass().getName() );
			val options =
				new ErrorBotServerOptions(
					_config.getErrorBotPort(),
					_config.getErrorBotApiKeys(),
					_dedicatedServer.getKeyPair(),
					_config.isErrorBotLogJetty()
				);
			val server = new ErrorBotServer( options );

			Utils.observeEvent( MinecraftForge.EVENT_BUS, AdvancementEvent.class )
			.filter( e -> e.getAdvancement().getDisplay() != null )
			.map( e -> {
				val name = e.getPlayer().getDisplayName().getString();
				val advancement = e.getAdvancement().getChatComponent().getString();
				return new ErrorBotAdvancementMessage( name, advancement );
			} )
			.retry()
			.observeOn( ErrorBotServer.SCHEDULER )
			.subscribe( message -> { server.transmit( message ); } );

			Utils.observeEvent( MinecraftForge.EVENT_BUS, PlayerLoggedInEvent.class )
			.map( e -> {
				val name = e.getPlayer().getDisplayName().getString();
				return new ErrorBotPlayerLoggedInMessage( name );
			} )
			.retry()
			.observeOn( ErrorBotServer.SCHEDULER )
			.subscribe( message -> { server.transmit( message ); } );

			Utils.observeEvent( MinecraftForge.EVENT_BUS, PlayerLoggedOutEvent.class )
			.map( e -> {
				val name = e.getPlayer().getDisplayName().getString();
				return new ErrorBotPlayerLoggedOutMessage( name );
			} )
			.retry()
			.observeOn( ErrorBotServer.SCHEDULER )
			.subscribe( message -> { server.transmit( message ); } );

			Utils.observeEvent( MinecraftForge.EVENT_BUS, LivingDeathEvent.class )
			.filter( e -> e != null && e.getEntityLiving() instanceof PlayerEntity )
			.map( e -> {
				val ent = e.getEntityLiving();
				val playerEntity = (PlayerEntity)ent;
				val source = e.getSource();

				String damageType = null;
				String immediateSource = null;
				String deathMessage = null;
				String trueSource = null;
				if( source != null ) {
					damageType = source.getMsgId();
					val deathMessageTextComponent = source.getLocalizedDeathMessage( ent );
					if( deathMessageTextComponent != null ) {
						deathMessage = deathMessageTextComponent.getString();
					}
					val immediateSourceEntity = source.getDirectEntity();
					if( immediateSourceEntity != null ) {
						immediateSource = immediateSourceEntity.getDisplayName().getString();
					}
					val trueSourceEntity = source.getEntity();
					if( trueSourceEntity != null ) {
						trueSource = trueSourceEntity.getDisplayName().getString();
					}
				}
				val name = playerEntity.getDisplayName().getString();
				return new ErrorBotLivingDeathMessage( name, damageType, deathMessage, immediateSource, trueSource );
			} )
			.retry()
			.observeOn( ErrorBotServer.SCHEDULER )
			.subscribe( message -> { server.transmit( message ); } );

			Utils.observeEvent( MinecraftForge.EVENT_BUS, ServerChatEvent.class )
			.map( e -> {
				val username = e.getUsername();
				val message = e.getMessage();
				return new ErrorBotServerChatMessage( username, message );
			} )
			.retry()
			.observeOn( ErrorBotServer.SCHEDULER )
			.subscribe( message -> { server.transmit( message ); } );

			server.getMessages( ErrorBotResponseMessage.class )
			.subscribe( m -> {
				_dedicatedServer.getPlayerList().broadcastMessage( new StringTextComponent( m.getMessage() ) );
			} );

			server.start();
			worker.dispose();
			ErrorBotServer.SCHEDULER.shutdown();
		} );
	}

	@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD, value = { Dist.DEDICATED_SERVER } )
	@OnlyIn(Dist.DEDICATED_SERVER)
	public static class Registration {
		@SubscribeEvent
		public static void registerDedicatedServerSetup( FMLDedicatedServerSetupEvent e ) {
			LOGGER.debug( "{}::registerDedicatedServerSetup", Registration.class.getName() );
			val config = TdwtfMinecraftModDedicatedServerConfig.loadConfig( FMLPaths.CONFIGDIR.get().resolve( "tdwtf-server.toml" ) );
			val dedicatedServer = new TdwtfMinecraftModDedicatedServer( config, e.getServerSupplier() );
			dedicatedServer.run();
			val shared = new TdwtfMinecraftModShared( config );
			shared.run();
		}
	}
}
