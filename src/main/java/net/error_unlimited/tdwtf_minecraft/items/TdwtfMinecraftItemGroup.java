package net.error_unlimited.tdwtf_minecraft.items;

import lombok.extern.log4j.Log4j2;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@Log4j2
public class TdwtfMinecraftItemGroup extends ItemGroup {
	private TdwtfMinecraftItemGroup() {
		super( "tdwtf_minecraft" );
		LOGGER.debug( "{}::ctor", getClass().getName() );
	}

	public static TdwtfMinecraftItemGroup INSTANCE = new TdwtfMinecraftItemGroup();

	@Override
	@OnlyIn(Dist.CLIENT)
	public ItemStack makeIcon() {
		LOGGER.debug( "{}::makeIcon", getClass().getName() );
		return new ItemStack( TdwtfLogoItem.ITEM );
	}
}
