package net.error_unlimited.tdwtf_minecraft.items;

import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.TdwtfMinecraftMod;
import net.error_unlimited.tdwtf_minecraft.blocks.IllusionBlock;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ForgeRegistries;

@Log4j2
public class IllusionItem extends BlockItem {
	public static final String REGISTRY_NAME = "illusion_item";
	public static final ResourceLocation RESOURCE_LOCATION = new ResourceLocation( TdwtfMinecraftMod.MOD_ID, REGISTRY_NAME );
	public static final RegistryObject<IllusionItem> REGISTRY_OBJECT = RegistryObject.of( RESOURCE_LOCATION, ForgeRegistries.ITEMS );
	public static final IllusionItem ITEM = new IllusionItem();

	private IllusionItem() {
		super(
			IllusionBlock.BLOCK,
			new Properties()
			.tab( TdwtfMinecraftItemGroup.INSTANCE )
		);
		setRegistryName( RESOURCE_LOCATION );
	}

	@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
	public static class Registration {
		@SubscribeEvent
		public static void registerItem( final RegistryEvent.Register<Item> e ) {
			LOGGER.debug( "{}::registerItem", Registration.class.getName() );
			e.getRegistry().register( ITEM );
		}
	}
}
