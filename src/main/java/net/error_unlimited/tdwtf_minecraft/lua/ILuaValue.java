package net.error_unlimited.tdwtf_minecraft.lua;

import org.squiddev.cobalt.LuaValue;

public interface ILuaValue {
	LuaValue getLuaValue();
}
