package net.error_unlimited.tdwtf_minecraft;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.TextNode;

import lombok.val;
import lombok.extern.log4j.Log4j2;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.dimension.DimensionType;

@Log4j2
public class RegionDeserializer extends StdDeserializer<Region> {
	public RegionDeserializer() {
		super( Region.class );
	}

	@Override
	public Region deserialize( JsonParser p, DeserializationContext ctxt ) throws IOException, JsonProcessingException {
		val node = p.getCodec().readTree( p );
		val name = (TextNode)node.get( "name" );
		val dimensionType = (TextNode)node.get( "dimensionType" );
		Region region = new Region( name.asText(), DimensionType.getByName( new ResourceLocation( dimensionType.asText() ) ) );
		val include = (ArrayNode)node.get( "include" );
		for( val entry : include ) {
			val an = (ArrayNode)entry;
			val aabb = new AxisAlignedBB( an.get( 0 ).doubleValue(), an.get( 1 ).doubleValue(), an.get( 2 ).doubleValue(), an.get( 3 ).doubleValue(), an.get( 4 ).doubleValue(), an.get( 5 ).doubleValue() );
			region.include( aabb );
		}
		val exclude = (ArrayNode)node.get( "exclude" );
		for( val entry : exclude ) {
			val an = (ArrayNode)entry;
			val aabb = new AxisAlignedBB( an.get( 0 ).doubleValue(), an.get( 1 ).doubleValue(), an.get( 2 ).doubleValue(), an.get( 3 ).doubleValue(), an.get( 4 ).doubleValue(), an.get( 5 ).doubleValue() );
			region.exclude( aabb );
		}
		return region;
	}
}
