package net.error_unlimited.tdwtf_minecraft.reflection;

import net.minecraftforge.fml.common.ObfuscationReflectionHelper;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;

@Log4j2
@UtilityClass
public final class ReflectionUtils {
	public static <T> Field getFieldEditable( Class<? super T> klass, String fieldName ) {
		try {
			Field field = ObfuscationReflectionHelper.findField( klass, fieldName );
			assert( field != null );
			if( !field.isAccessible() ) {
				field.setAccessible( true );
			}
			int modifiers = field.getModifiers();
			if( ( modifiers & Modifier.FINAL ) != Modifier.FINAL ) return field;
			Field modifiersField = Field.class.getDeclaredField( "modifiers" );
			assert( modifiersField != null );
			if( !modifiersField.isAccessible() ) {
				modifiersField.setAccessible( true );
			}
			modifiersField.setInt( field, modifiers & ~Modifier.FINAL );
			return field;
		} catch( Exception ex ) {
			LOGGER.fatal( ex );
			throw new RuntimeException( ex );
		}
	}
}
