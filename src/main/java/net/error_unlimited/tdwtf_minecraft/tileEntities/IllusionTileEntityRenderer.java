package net.error_unlimited.tdwtf_minecraft.tileEntities;

import com.mojang.blaze3d.matrix.MatrixStack;

import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.TdwtfMinecraftMod;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.BlockRendererDispatcher;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.model.data.EmptyModelData;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

@Log4j2
@OnlyIn(Dist.CLIENT)
public class IllusionTileEntityRenderer extends TileEntityRenderer<IllusionTileEntity> {
	private final BlockRendererDispatcher _blockRendererDispatcher;

	public IllusionTileEntityRenderer( TileEntityRendererDispatcher tileEntityRendererDispatcher, BlockRendererDispatcher blockRendererDispatcher ) {
		super( tileEntityRendererDispatcher );
		LOGGER.debug( "{}::ctor", getClass().getName() );
		_blockRendererDispatcher = blockRendererDispatcher;
	}

	@Override
	public void render( IllusionTileEntity tileEntity, float partialTicks, MatrixStack matrixStack, IRenderTypeBuffer buffer, int combinedLight, int combinedOverlay ) {
		BlockState bs = tileEntity.getEmulatedBlockState();
		if( bs == null ) return;
		_blockRendererDispatcher.renderBlock( bs, matrixStack, buffer, combinedLight, combinedOverlay, EmptyModelData.INSTANCE );
	}

	@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD, value = { Dist.CLIENT })
	@OnlyIn(Dist.CLIENT)
	public static class Registration {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public static void registerClientSetup( final FMLClientSetupEvent e ) {
			LOGGER.debug( "{}::registerClientSetup", Registration.class.getName() );
			ClientRegistry.bindTileEntityRenderer( IllusionTileEntity.TILE_ENTITY_TYPE, dispatcher -> new IllusionTileEntityRenderer( dispatcher, e.getMinecraftSupplier().get().getBlockRenderer() ) );
		}
	}
}
