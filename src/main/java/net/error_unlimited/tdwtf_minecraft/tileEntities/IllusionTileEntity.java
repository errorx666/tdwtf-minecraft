package net.error_unlimited.tdwtf_minecraft.tileEntities;

import com.google.common.collect.Sets;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;
import lombok.val;
import lombok.var;
import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.TdwtfMinecraftMod;
import net.error_unlimited.tdwtf_minecraft.Utils;
import net.error_unlimited.tdwtf_minecraft.blocks.IllusionBlock;
import net.error_unlimited.tdwtf_minecraft.inventory.IllusionContainer;
import net.minecraft.block.AirBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.inventory.container.Container;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.state.IProperty;
import net.minecraft.tileentity.LockableTileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Direction;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.Explosion;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Log4j2
public class IllusionTileEntity extends LockableTileEntity {
	public static final ResourceLocation RESOURCE_LOCATION = new ResourceLocation( TdwtfMinecraftMod.MOD_ID, "illusion_tile_entity_type" );
	public static final TileEntityType<IllusionTileEntity> TILE_ENTITY_TYPE = new TileEntityType<IllusionTileEntity>( IllusionTileEntity::new, Sets.newHashSet( IllusionBlock.BLOCK ), null );
	static {
		TILE_ENTITY_TYPE.setRegistryName( RESOURCE_LOCATION );
	}

	private NonNullList<ItemStack> _contents;

	public static final int CONTAINER_SIZE = 1;

	private boolean _updatingEmulatedBlock = false;
	private Block _emulatedBlock = Blocks.AIR;

	public void refreshEmulatedBlock() {
		// LOGGER.debug( "{}::refreshEmulatedBlock", getClass().getName() );
		val itemStack = _contents.get( 0 );
		var block = Blocks.AIR;
		if( !itemStack.isEmpty() ) block = Block.byItem( itemStack.getItem() );
		setEmulatedBlock( block );
	}

	public void setEmulatedBlock( Block block ) {
		_updatingEmulatedBlock = true;
		assert( level != null );
		assert( block != null );
		// LOGGER.debug( "{}::setEmulatedBlock {}", getClass().getName(), block.getRegistryName() );
		if( !isLoaded() ) {
			LOGGER.error( "{}::setEmulatedBlock chunk not loaded", getClass().getName() );
			_updatingEmulatedBlock = false;
			return;
		}
		_emulatedBlock = block;
		_emulatedBlockState = null;
		if( level.isClientSide() ) {
			_updatingEmulatedBlock = false;
			return;
		}
		val oldIllusionState = getBlockState();
		val newIllusionState = oldIllusionState.setValue( IllusionBlock.VISIBLE, !( block instanceof AirBlock ) );
		level.setBlock( worldPosition, newIllusionState, Constants.BlockFlags.DEFAULT_AND_RERENDER | Constants.BlockFlags.IS_MOVING );
		if( block instanceof IllusionBlock ) {
			level.explode( null, DamageSource.MAGIC, worldPosition.getX() + .5D, worldPosition.getY() + .5D, worldPosition.getZ() + .5D, 5, false, Explosion.Mode.DESTROY );
		}
		_updatingEmulatedBlock = false;
	}

	public Block getEmulatedBlock() {
		return _emulatedBlock;
	}

	private boolean isLoaded() {
		if( level == null ) return false;
		if( level.restoringBlockSnapshots ) return false;
		if( !level.getChunkSource().isEntityTickingChunk( new ChunkPos( worldPosition ) ) ) return false;
		return true;
}

	private final Subject<Boolean> _needsRefresh = PublishSubject.create();
	private final Subject<Boolean> _unloaded = PublishSubject.create();
	public IllusionTileEntity() {
		super( TILE_ENTITY_TYPE );
		LOGGER.debug( "{}::ctor", getClass().getName() );
		_contents = NonNullList.<ItemStack>withSize( CONTAINER_SIZE, ItemStack.EMPTY );
	}

	private BlockState _emulatedBlockState;

	@SuppressWarnings("unchecked")
	private BlockState computeEmulatedBlockState( BlockState blockState, Block block ) {
		var emulatedBlockState = _emulatedBlock.defaultBlockState();
		val facingName = IllusionBlock.FACING.getName();
		val facingValue = blockState.getValue( IllusionBlock.FACING );
		for( IProperty<?> property : emulatedBlockState.getProperties() ) {
			String propertyName = property.getName();
			if( facingName.equals( propertyName ) ) {
				if( property.getPossibleValues().contains( facingValue ) ) {
					emulatedBlockState = emulatedBlockState.setValue( (IProperty<Direction>)property, facingValue );
				}
			}
		}
		return emulatedBlockState;
}

	public BlockState getEmulatedBlockState() {
		if( _emulatedBlockState == null && _emulatedBlock != null ) {
			val blockState = getBlockState();
			if( blockState != null ) {
				_emulatedBlockState = computeEmulatedBlockState( blockState, _emulatedBlock );
			}
		}
		return _emulatedBlockState;
	}


	@Override
	public boolean isEmpty() {
		for( val itemStack : _contents ) {
			if( !itemStack.isEmpty() ) return false;
		}
		return true;
	}

	public int getMaxStackSize() {
		return 1;
	}

	@Override
	public int getContainerSize() {
		return CONTAINER_SIZE;
	}

	@Override
	public boolean canPlaceItem( int index, ItemStack stack ) {
		assert( index == 0 );
		val isValid = stack.isEmpty() || stack.getItem() instanceof BlockItem;
		// LOGGER.debug( "{}::canPlaceItem {} {} => {}", getClass().getName(), index, stack, isValid );
		return isValid;
	}

	@Override
	public void setItem( int index, ItemStack stack ) {
		// LOGGER.debug( "{}::setItem {} {}", getClass().getName(), index, stack );
		assert( index == 0 );
		_contents.set( index, stack );
		if( stack.getCount() > getMaxStackSize() ) {
			stack.setCount( getMaxStackSize() );
		}
		setChanged();
		_needsRefresh.onNext( true );
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		val nbt = getUpdateTag();
		// LOGGER.debug( "{}::getUpdatePacket {}", getClass().getName(), nbt );
		return new SUpdateTileEntityPacket( worldPosition, -1, nbt );
	}

	@Override
	public ItemStack removeItem( int index, int count ) {
		// LOGGER.debug( "{}::removeItem {} {}", getClass().getName(), index, count );
		val itemStack = ItemStackHelper.removeItem( _contents, index, count );
		if( !itemStack.isEmpty() ) {
			setChanged();
		}
  		_needsRefresh.onNext( true );
		return itemStack;
	}

	@Override
	public ItemStack removeItemNoUpdate( int index ) {
		// LOGGER.debug( "{}::removeItemNoUpdate {}", getClass().getName(), index );
		val itemStack = ItemStackHelper.takeItem( _contents, index );
		if( !itemStack.isEmpty() ) {
			setChanged();
		}
		_needsRefresh.onNext( true );
		return itemStack;
	}

	private static Observable<TickEvent> _ticks = Utils.observeEvent( MinecraftForge.EVENT_BUS, TickEvent.class ).share();

	@Override
	public void onLoad() {
		// LOGGER.debug( "{}::onLoad", getClass().getName() );
		requestModelDataUpdate();
		_needsRefresh
		.switchMap( e ->
			e ? _ticks
				.filter( f -> !_updatingEmulatedBlock )
				.filter( f -> isLoaded() )
				.filter( f -> f instanceof TickEvent.ServerTickEvent || f instanceof TickEvent.ClientTickEvent )
				.map( f -> true )
				.take( 1 )
			: Observable.empty()
		)
		.takeUntil( _unloaded )
		.subscribe( e -> {
			refreshEmulatedBlock();
		} );
		_needsRefresh.onNext( true );
	}

	@Override
	public CompoundNBT getUpdateTag() {
		val updateTag = serializeNBT();
		// LOGGER.debug( "{}::getUpdateTag {}", getClass().getName(), updateTag );
		return updateTag;
	}

	@Override
	public void onDataPacket( NetworkManager net, SUpdateTileEntityPacket pkt ) {
		val tag = pkt.getTag();
		// LOGGER.debug( "{}::onDataPacket {}", getClass().getName(), tag );
		handleUpdateTag( tag );
	}

	@Override
	public void handleUpdateTag( CompoundNBT tag ) {
		// LOGGER.debug( "{}::handleUpdateTag {}", getClass().getName(), tag );
		if( !isLoaded() ) return;
		deserializeNBT( tag );
		_needsRefresh.onNext( true );
	}

	@Override
	public void onChunkUnloaded() {
		// LOGGER.debug( "{}::onChunkUnloaded", getClass().getName() );
		super.onChunkUnloaded();
		_unloaded.onNext( true );
		_unloaded.onComplete();
		_needsRefresh.onComplete();
	}

	@Override
	public void setRemoved() {
		// LOGGER.debug( "{}::setRemoved", getClass().getName() );
		super.setRemoved();
		_unloaded.onNext( true );
		_unloaded.onComplete();
		_needsRefresh.onComplete();
	}

	@Override
	public void setChanged() {
		// LOGGER.debug( "{}::setChanged", getClass().getName() );
		super.setChanged();
		_needsRefresh.onNext( true );
	}

	@Override
	protected ITextComponent getDefaultName() {
		return new TranslationTextComponent( "tileEntity." + TdwtfMinecraftMod.MOD_ID + ".illusion_tile_entity" );
	}

	@Override
	protected Container createMenu( int windowId, PlayerInventory playerInventory ) {
		return new IllusionContainer( windowId, playerInventory, this );
	}

	@Override
	public void load( CompoundNBT compound ) {
		// LOGGER.debug( "{}::load {}", getClass().getName(), compound );
		super.load( compound );
		_contents = NonNullList.withSize( getContainerSize(), ItemStack.EMPTY );
		ItemStackHelper.loadAllItems( compound, _contents );
	}

	@Override
	public CompoundNBT save( CompoundNBT compound ) {
		super.save( compound );
		ItemStackHelper.saveAllItems( compound, _contents );
		// LOGGER.debug( "{}::save {}", getClass().getName(), compound );
		return compound;
	}

	@Override
	public ItemStack getItem( int index ) {
		return _contents.get( index );
	}

	@Override
	public boolean stillValid( PlayerEntity player ) {
		if( level.getBlockEntity( worldPosition ) != this ) {
			return false;
		} else {
			return !( player.distanceToSqr( (double)worldPosition.getX() + .5D, (double)worldPosition.getY() + .5D, (double)worldPosition.getZ() + .5D) > 64.D );
		}
	}

	@Override
	public void clearCache() {
		super.clearCache();
		_needsRefresh.onNext( true );
	}

	@Override
	public void clearContent() {
		// LOGGER.debug( "{}::clearContent", getClass().getName() );
		_contents.clear();
		_needsRefresh.onNext( true );
	}

	@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
	public static class Registration {
		@SubscribeEvent
		public static void registerTileEntityType( final RegistryEvent.Register<TileEntityType<?>> e ) {
			LOGGER.debug( "{}::registerTileEntityType", Registration.class.getName() );
			e.getRegistry().register( TILE_ENTITY_TYPE );
		}
	}
}
