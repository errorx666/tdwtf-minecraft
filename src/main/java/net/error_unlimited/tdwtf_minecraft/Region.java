package net.error_unlimited.tdwtf_minecraft;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Getter;
import lombok.val;
import lombok.extern.log4j.Log4j2;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.common.util.BlockSnapshot;

@Log4j2
@JsonDeserialize(using = RegionDeserializer.class)
@JsonSerialize(using = RegionSerializer.class)
public class Region {
	@JsonCreator
	public Region( String name, DimensionType dimensionType ) {
		_name = name;
		_dimensionType = dimensionType;
	}

	@Getter
	private final DimensionType _dimensionType;

	@Getter
	private final String _name;

	public List<AxisAlignedBB> getInclude() {
		return Collections.unmodifiableList( _include );
	}

	public List<AxisAlignedBB> getExclude() {
		return Collections.unmodifiableList( _exclude );
	}

	private final List<AxisAlignedBB> _include = new ArrayList<AxisAlignedBB>();
	private final List<AxisAlignedBB> _exclude = new ArrayList<AxisAlignedBB>();

	@Override
	public boolean equals( Object obj ) {
		if( !( obj instanceof Region ) ) return false;
		val r = (Region)obj;
		if( r.getDimensionType().getId() != getDimensionType().getId() ) return false;
		if( !r.getName().equals( getName() ) ) return false;
		val include = getInclude();
		if( !r.getInclude().stream().allMatch( i -> include.contains( i ) ) ) return false;
		val exclude = getExclude();
		if( !r.getExclude().stream().allMatch( i -> exclude.contains( i ) ) ) return false;
		return true;
	}

	public Region include( AxisAlignedBB aabb ) {
		_exclude.removeIf( r -> r.equals( aabb ) );
		_include.add( aabb );
		return this;
	}

	public Region exclude( AxisAlignedBB aabb ) {
		_include.removeIf( r -> r.equals( aabb ) );
		_exclude.add( aabb );
		return this;
	}

	public boolean isInside( Vec3d vec, int dimId ) {
		if( dimId != _dimensionType.getId() ) return false;
		return _include.stream().anyMatch( v -> v.contains( vec ) ) && !_exclude.stream().anyMatch( v -> v.contains( vec ) );
	}

	public boolean isInside( Vec3d vec, DimensionType dimensionType ) {
		return isInside( vec, dimensionType.getId() );
	}

	public boolean isInside( BlockPos pos, int dimId ) {
		return isInside( new Vec3d( pos ), dimId );
	}

	public boolean isInside( BlockPos pos, DimensionType dimensionType ) {
		return isInside( new Vec3d( pos ), dimensionType );
	}

	public boolean isInside( BlockSnapshot snapshot ) {
		return isInside( new Vec3d( snapshot.getPos() ), snapshot.getDimId() );
	}

	public boolean isInside( Entity entity ) {
		return isInside( entity.position(), entity.dimension );
	}

	public boolean isOutside( Vec3d vec, int dimId ) {
		return !isInside( vec, dimId );
	}

	public boolean isOutside( Vec3d vec, DimensionType dimensionType ) {
		return !isInside( vec, dimensionType );
	}

	public boolean isOutside( BlockPos pos, int dimId ) {
		return !isInside( pos, dimId );
	}

	public boolean isOutside( BlockPos pos, DimensionType dimensionType ) {
		return !isInside( pos, dimensionType );
	}

	public boolean isOutside( BlockSnapshot snapshot ) {
		return !isInside( new Vec3d( snapshot.getPos() ), snapshot.getDimId() );
	}

	public boolean isOutside( Entity entity ) {
		return !isInside( entity.position(), entity.dimension );
	}

	@Override
	public String toString() {
		return "Region[" + _name + "]";
	}
}
