package net.error_unlimited.tdwtf_minecraft.particles;

import lombok.Getter;
import lombok.Setter;
import lombok.val;
import lombok.extern.log4j.Log4j2;
import net.minecraft.client.particle.IParticleRenderType;
import net.minecraft.client.particle.SpriteTexturedParticle;
import net.minecraft.fluid.Fluid;
import net.minecraft.particles.IParticleData;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@Log4j2
@OnlyIn(Dist.CLIENT)
public abstract class TdwtfMinecraftDripParticle extends SpriteTexturedParticle {
	private final Fluid _fluid;

	public TdwtfMinecraftDripParticle( World world, double x, double y, double z, Fluid fluid ) {
		super( world, x, y, z );
		setSize( .01f, .01f );
		gravity = .06f;
		_fluid = fluid;
	}

	@Override
	public IParticleRenderType getRenderType() {
		return IParticleRenderType.PARTICLE_SHEET_OPAQUE;
	}

	@Override
	public void tick() {
		xo = x;
		yo = y;
		zo = z;
		preMoveUpdate();
		if( !removed ) {
			yd -= (double)gravity;
			move( xd, yd, zd );
			postMoveUpdate();
			if( !removed ) {
				xd *= (double).98f;
				yd *= (double).98f;
				zd *= (double).98f;
				val blockpos = new BlockPos( x, y, z );
				val fluidState = level.getFluidState( blockpos );
				if( fluidState.getType() == _fluid && y < (double)( (float)blockpos.getY() + fluidState.getHeight( level, blockpos ) ) ) {
					remove();
				}
			}
		}
	}

	public float getGravity() {
		return gravity;
	}

	public void setGravity( float g ) {
		gravity = g;
	}

	protected void preMoveUpdate() {
		if( lifetime-- <= 0 ) remove();
	}

	protected void postMoveUpdate() {}

	@OnlyIn(Dist.CLIENT)
	static class Dripping extends TdwtfMinecraftDripParticle {
		protected final IParticleData fallingParticle;

		@Getter
		@Setter
		private SoundEvent _fallSound;

		public Dripping( World world, double x, double y, double z, Fluid fluid ) {
			this( world, x, y, z, fluid, null );
		}

		public Dripping( World world, double x, double y, double z, Fluid fluid, IParticleData fallingParticle ) {
			super( world, x, y, z, fluid );
			this.fallingParticle = fallingParticle;
			gravity *= .02f;
			lifetime = 40;
		}

		@Override
		protected void preMoveUpdate() {
			if( lifetime-- <= 0 ) {
				remove();
				if( fallingParticle != null ) level.addParticle( fallingParticle, x, y, z, xd, yd, zd );
				if( _fallSound != null ) level.playLocalSound( x + .5d, y, z + .5d, _fallSound, SoundCategory.BLOCKS, .3f + level.random.nextFloat() * 2.f / 3.f, 1.f, false );
			}
		}

		@Override
		protected void postMoveUpdate() {
			xd *= .02d;
			yd *= .02d;
			zd *= .02d;
		}
	}

	@OnlyIn(Dist.CLIENT)
	static class Falling extends TdwtfMinecraftDripParticle {
		protected final IParticleData landParticle;

		@Getter
		@Setter
		private SoundEvent _landSound;

		public Falling( World world, double x, double y, double z, Fluid fluid ) {
			this( world, x, y, z, fluid, null );
		}

		public Falling( World world, double x, double y, double z, Fluid fluid, IParticleData landParticle ) {
			super( world, x, y, z, fluid );
			lifetime = (int)( 64.d / ( Math.random() * .8d + .2d ) );
			this.landParticle = landParticle;
		}

		@Override
		protected void postMoveUpdate() {
			if( onGround ) {
				remove();
				if( landParticle != null ) level.addParticle( landParticle, x, y, z, .0d, .0d, 0.d );
				if( _landSound != null ) level.playLocalSound( x + .5d, y, z + .5d, _landSound, SoundCategory.BLOCKS, .3f + level.random.nextFloat() * 2.f / 3.f, 1.f, false );
			}
		}
	}

	@OnlyIn(Dist.CLIENT)
	static class Landing extends TdwtfMinecraftDripParticle {
		public Landing( World world, double x, double y, double z, Fluid fluid ) {
			super( world, x, y, z, fluid );
			lifetime = (int)( 16.d / ( Math.random() * .8d + .2d ) );
		}
	}
}
