package net.error_unlimited.tdwtf_minecraft.particles;

import lombok.val;
import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.TdwtfMinecraftMod;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.IAnimatedSprite;
import net.minecraft.client.particle.IParticleFactory;
import net.minecraft.client.particle.Particle;
import net.minecraft.fluid.Fluids;
import net.minecraft.particles.BasicParticleType;
import net.minecraft.particles.ParticleType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.ParticleFactoryRegisterEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Log4j2
public class SandFallingParticleType extends BasicParticleType {
	public static final String REGISTRY_NAME = "sand_falling_particle_type";
	public static final ResourceLocation RESOURCE_LOCATION = new ResourceLocation( TdwtfMinecraftMod.MOD_ID, REGISTRY_NAME );
	public static final SandFallingParticleType PARTICLE_TYPE = new SandFallingParticleType();

	private SandFallingParticleType() {
		super( false );
		setRegistryName( RESOURCE_LOCATION );
	}

	@OnlyIn(Dist.CLIENT)
	public static class Factory implements IParticleFactory<BasicParticleType> {
		private final IAnimatedSprite _sprite;

		public Factory( IAnimatedSprite sprite ) {
			_sprite = sprite;
		}

		public Particle createParticle( BasicParticleType particleType, World world, double x, double y, double z, double speedX, double speedY, double speedZ ) {
			val particle = new TdwtfMinecraftDripParticle.Falling( world, x, y, z, Fluids.EMPTY );
			particle.setGravity( .005f );
			particle.setLifetime( 100 );
			particle.setColor( .835f, .788f, .655f );
			particle.pickSprite( _sprite );
			return particle;
		}
	}

	@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
	public static class Registration {
		@SubscribeEvent
		public static void registerParticleType( final RegistryEvent.Register<ParticleType<?>> e ) {
			LOGGER.debug( "{}::registerParticleType", Registration.class.getName() );
			e.getRegistry().register( PARTICLE_TYPE );
		}

		@SuppressWarnings( "resource" )
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public static void registerParticleFactory( final ParticleFactoryRegisterEvent e ) {
			LOGGER.debug( "{}::registerParticleFactory", Registration.class.getName() );
			Minecraft.getInstance().particleEngine.register( PARTICLE_TYPE, Factory::new );
		}
	}
}
