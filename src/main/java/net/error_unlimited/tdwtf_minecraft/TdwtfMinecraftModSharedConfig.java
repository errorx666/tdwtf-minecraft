package net.error_unlimited.tdwtf_minecraft;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.electronwill.nightconfig.core.CommentedConfig;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.log4j.Log4j2;
import net.minecraft.entity.boss.WitherEntity;
import net.minecraft.entity.monster.CreeperEntity;
import net.minecraft.entity.monster.EndermanEntity;
import net.minecraft.entity.monster.GhastEntity;
import net.minecraft.entity.monster.RavagerEntity;
import net.minecraft.entity.monster.ZombieEntity;
import net.minecraft.entity.monster.ZombieVillagerEntity;
import net.minecraft.entity.projectile.DragonFireballEntity;
import net.minecraft.entity.projectile.WitherSkullEntity;
import net.minecraftforge.common.ForgeConfigSpec;

@Log4j2
@ToString
@EqualsAndHashCode
public abstract class TdwtfMinecraftModSharedConfig {
	@Getter
	private Set<String> _disableMobGriefingFor;

	protected TdwtfMinecraftModSharedConfig() {}

	protected void buildSpec( ForgeConfigSpec.Builder builder ) {
		LOGGER.debug( "{}::buildSpec", getClass().getName() );
		builder.defineList( "disableMobGriefingFor",
			Arrays.asList(
				CreeperEntity.class.getSimpleName(),
				DragonFireballEntity.class.getSimpleName(),
				EndermanEntity.class.getSimpleName(),
				GhastEntity.class.getSimpleName(),
				RavagerEntity.class.getSimpleName(),
				WitherEntity.class.getSimpleName(),
				WitherSkullEntity.class.getSimpleName(),
				ZombieEntity.class.getSimpleName(),
				ZombieVillagerEntity.class.getSimpleName()
			),
			o -> o instanceof String
		);
	}

	protected void loadConfig( CommentedConfig config ) {
		_disableMobGriefingFor = new HashSet<String>(
			config.get( "disableMobGriefingFor" )
		);
	}
}
