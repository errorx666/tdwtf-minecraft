package net.error_unlimited.tdwtf_minecraft.damageSources;

import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.TdwtfMinecraftMod;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;

@Log4j2
public class TdwtfMinecraftDamageSource extends DamageSource {
	public static final TdwtfMinecraftDamageSource PUNJI = new TdwtfMinecraftDamageSource( "punji" );

	private TdwtfMinecraftDamageSource( String msgId ) {
		super( msgId );
	}

	@Override
	public ITextComponent getLocalizedDeathMessage( LivingEntity entityLivingBase ) {
		LivingEntity livingEntity = entityLivingBase.getKillCredit();
		if( livingEntity == null ) {
			return new TranslationTextComponent( "death." + TdwtfMinecraftMod.MOD_ID + ".attack." + msgId, entityLivingBase.getDisplayName() );
		} else {
			return new TranslationTextComponent( "death." + TdwtfMinecraftMod.MOD_ID + ".attack." + msgId + ".player", entityLivingBase.getDisplayName(), livingEntity.getDisplayName() );
		}
	}

}
