package net.error_unlimited.tdwtf_minecraft.events;

import org.apache.commons.lang3.NotImplementedException;

import lombok.Getter;
import lombok.NonNull;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.eventbus.api.Event.Result;

public class TdwtfMinecraftEventAdapter implements TdwtfMinecraftEvent {
	private final Event _event;

	@Getter
	private Entity _entity;

	@Getter
	private BlockPos _blockPos;

	@Getter
	private BlockState _blockState;

	@Getter
	private ItemStack _itemStack;

	@Getter
	private IInventory _inventory;

	public void setCanceled( boolean canceled ) {
		_event.setCanceled( canceled );
	}

	public void setResult( Result result ) {
		if( _event instanceof PlayerEvent.HarvestCheck ) ((PlayerEvent.HarvestCheck)_event).setCanHarvest( result != Result.DENY );
		_event.setResult( result );
	}

	private TdwtfMinecraftEventAdapter( @NonNull Event event ) {
		_event = event;
	}

	private TdwtfMinecraftEventAdapter( @NonNull BlockEvent event ) {
		this( (Event)event );
		_entity = null;
		_blockPos = event.getPos();
		_blockState = event.getState();
	}

	private TdwtfMinecraftEventAdapter( @NonNull BlockEvent.EntityPlaceEvent event ) {
		this( (BlockEvent)event );
		_entity = event.getEntity();
	}

	private TdwtfMinecraftEventAdapter( @NonNull BlockEvent.BreakEvent event ) {
		this( (BlockEvent)event );
		_entity = event.getPlayer();
	}

	private TdwtfMinecraftEventAdapter( @NonNull PlayerEvent event ) {
		this( (Event)event );
		_entity = event.getPlayer();
	}

	private TdwtfMinecraftEventAdapter( @NonNull PlayerEvent.BreakSpeed event ) {
		this( (PlayerEvent) event );
		_blockPos = event.getPos();
		_blockState = event.getState();
	}

	private TdwtfMinecraftEventAdapter( @NonNull PlayerEvent.HarvestCheck event ) {
		this( (PlayerEvent) event );
		_blockPos = new BlockPos( getPlayerEntity().position() );
		_blockState = event.getTargetBlock();
	}

	private TdwtfMinecraftEventAdapter( @NonNull PlayerEvent.ItemPickupEvent event ) {
		this( (PlayerEvent) event );
		_itemStack = event.getStack();
	}

	private TdwtfMinecraftEventAdapter( @NonNull PlayerEvent.ItemCraftedEvent event ) {
		this( (PlayerEvent) event );
		_itemStack = event.getCrafting();
		_inventory = event.getInventory();
	}

	private TdwtfMinecraftEventAdapter( @NonNull PlayerEvent.ItemSmeltedEvent event ) {
		this( (PlayerEvent) event );
		_itemStack = event.getSmelting();
	}

	public static TdwtfMinecraftEvent adapt( Event e ) {
		if( e instanceof TdwtfMinecraftEvent ) return (TdwtfMinecraftEvent)e;
		if( e instanceof PlayerEvent ) {
			if( e instanceof PlayerEvent.BreakSpeed ) return new TdwtfMinecraftEventAdapter( (PlayerEvent.BreakSpeed)e );
			if( e instanceof PlayerEvent.ItemPickupEvent ) return new TdwtfMinecraftEventAdapter( (PlayerEvent.ItemPickupEvent)e );
			if( e instanceof PlayerEvent.ItemCraftedEvent ) return new TdwtfMinecraftEventAdapter( (PlayerEvent.ItemCraftedEvent)e );
			if( e instanceof PlayerEvent.ItemSmeltedEvent ) return new TdwtfMinecraftEventAdapter( (PlayerEvent.ItemSmeltedEvent)e );
		}
		if( e instanceof BlockEvent ) {
			if( e instanceof BlockEvent.BreakEvent ) return new TdwtfMinecraftEventAdapter( (BlockEvent.BreakEvent)e );
			if( e instanceof BlockEvent.EntityPlaceEvent ) return new TdwtfMinecraftEventAdapter( (BlockEvent.EntityPlaceEvent)e );
		}
		throw new NotImplementedException( e.getClass().getName() );
	}
}
