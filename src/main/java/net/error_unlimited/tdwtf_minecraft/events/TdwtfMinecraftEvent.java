package net.error_unlimited.tdwtf_minecraft.events;

import lombok.val;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.IWorld;
import net.minecraft.world.dimension.Dimension;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.eventbus.api.Event.Result;

public interface TdwtfMinecraftEvent {
	default Entity getEntity() { return null; }
	default LivingEntity getLivingEntity() {
		val e = getEntity();
		if( e instanceof LivingEntity ) return (LivingEntity)e;
		else return null;
	}
	default PlayerEntity getPlayerEntity() {
		val e = getLivingEntity();
		if( e instanceof PlayerEntity ) return (PlayerEntity)e;
		else return null;
	}
	default BlockState getBlockState() { return null; }
	default Block getBlock() {
		val blockState = getBlockState();
		if( blockState == null ) return null;
		else return blockState.getBlock();
	}
	default BlockPos getBlockPos() { return null; }
	default Vec3d getVec3d() {
		val blockPos = getBlockPos();
		if( blockPos == null ) {
			val entity = getEntity();
			if( entity == null ) return null;
			else return entity.position();
		} else return new Vec3d( blockPos );
	}
	default IWorld getWorld() {
		val entity = getEntity();
		if( entity == null ) return null;
		else return entity.level;
	}
	default ClientWorld getClientWorld() {
		val world = getWorld();
		if( world instanceof ClientWorld ) return (ClientWorld)world;
		else return null;
	}
	default ServerWorld getServerWorld() {
		val world = getWorld();
		if( world instanceof ServerWorld ) return (ServerWorld)world;
		else return null;
	}
	default Dimension getDimension() {
		val world = getWorld();
		if( world == null ) return null;
		else return world.getDimension();
	}
	default DimensionType getDimensionType() {
		val dim = getDimension();
		if( dim != null ) return dim.getType();
		else return null;
	}
	default ItemStack getItemStack() {
		return null;
	}
	default IInventory getInventory() {
		return null;
	}
	void setCanceled( boolean canceled );
	void setResult( Result result );
}
