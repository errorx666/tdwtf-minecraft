package net.error_unlimited.tdwtf_minecraft;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;

@Log4j2
@UtilityClass
public class Json {
	private static final ObjectMapper _objectMapper = new ObjectMapper();
	static {
		_objectMapper.setSerializationInclusion( JsonInclude.Include.ALWAYS );
		_objectMapper.setVisibility( PropertyAccessor.GETTER, JsonAutoDetect.Visibility.PUBLIC_ONLY );
		_objectMapper.setVisibility( PropertyAccessor.FIELD, JsonAutoDetect.Visibility.PUBLIC_ONLY );
	}

	public static <T> T parse( final String str, final Class<T> klass ) {
		try {
			return _objectMapper.readValue( str, klass );
		} catch( JsonProcessingException ex ) {
			throw new RuntimeException( ex );
		}
	}

	public static <T> List<T> parseList( final String str, final Class<T> klass ) {
		try {
			return _objectMapper.readerForListOf( klass ).readValue( str );
		} catch( JsonProcessingException ex ) {
			throw new RuntimeException( ex );
		}
	}

	public static String stringify( final Object obj ) {
		try {
			return _objectMapper.writeValueAsString( obj );
		} catch( JsonProcessingException ex ) {
			throw new RuntimeException( ex );
		}
	}
}
