package net.error_unlimited.tdwtf_minecraft;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import lombok.val;
import lombok.extern.log4j.Log4j2;
import net.minecraft.world.dimension.DimensionType;

@Log4j2
public class RegionSerializer extends StdSerializer<Region> {
	public RegionSerializer() {
		super( Region.class );
	}

	@Override
	public void serialize( Region value, JsonGenerator gen, SerializerProvider provider ) throws IOException {
		gen.writeStartObject();
		gen.writeStringField( "name", value.getName() );
		gen.writeStringField( "dimensionType", DimensionType.getName( value.getDimensionType() ).toString() );
		gen.writeArrayFieldStart( "include" );
		for( val aabb : value.getInclude() ) {
			gen.writeArray( new double[] { aabb.minX, aabb.minY, aabb.minZ, aabb.maxX, aabb.maxY, aabb.maxZ }, 0, 6 );
		}
		gen.writeEndArray();
		gen.writeArrayFieldStart( "exclude" );
		for( val aabb : value.getExclude() ) {
			gen.writeArray( new double[] { aabb.minX, aabb.minY, aabb.minZ, aabb.maxX, aabb.maxY, aabb.maxZ }, 0, 6 );
		}
		gen.writeEndArray();
		gen.writeEndObject();

	}
}
