package net.error_unlimited.tdwtf_minecraft.ftp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import com.google.common.collect.Lists;

import org.apache.ftpserver.ftplet.FtpFile;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RequiredArgsConstructor
public class TdwtfMinecraftInvalidFtpFile implements FtpFile {
	@Getter
	private final String _absolutePath;

	@Override
	public boolean isHidden() {
		return getName().startsWith( "." );
	}

	@Override
	public boolean doesExist() {
		return false;
	}

	@Override
	public boolean isReadable() {
		return false;
	}

	@Override
	public boolean isWritable() {
		return false;
	}

	@Override
	public boolean isRemovable() {
		return false;
	}

	@Override
	public String getOwnerName() {
		return "root";
	}

	@Override
	public String getGroupName() {
		return "root";
	}

	@Override
	public int getLinkCount() {
		return 0;
	}

	@Override
	public long getLastModified() {
		return 0L;
	}

	@Override
	public boolean setLastModified( long time ) {
		return false;
	}

	@Override
	public long getSize() {
		return 0L;
	}

	@Override
	public Object getPhysicalFile() {
		return null;
	}

	@Override
	public boolean mkdir() {
		return false;
	}

	@Override
	public boolean delete() {
		return false;
	}

	@Override
	public boolean move( FtpFile destination ) {
		return false;
	}

	@Override
	public List<? extends FtpFile> listFiles() {
		return Lists.newArrayList();
	}

	@Override
	public OutputStream createOutputStream( long offset ) throws IOException {
		return null;
	}

	@Override
	public InputStream createInputStream( long offset ) throws IOException {
		return null;
	}

	@Override
	public String getName() {
		assert _absolutePath.startsWith( "/" );
		return _absolutePath.substring( _absolutePath.lastIndexOf( "/" ) + 1 );
	}

	@Override
	public boolean isDirectory() {
		return false;
	}

	@Override
	public boolean isFile() {
		return true;
	}
}
