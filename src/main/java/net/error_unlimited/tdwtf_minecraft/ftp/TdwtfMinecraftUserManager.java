package net.error_unlimited.tdwtf_minecraft.ftp;

import java.util.HashMap;
import java.util.Map;

import org.apache.ftpserver.ftplet.Authentication;
import org.apache.ftpserver.ftplet.AuthenticationFailedException;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.User;
import org.apache.ftpserver.ftplet.UserManager;
import org.apache.ftpserver.usermanager.UsernamePasswordAuthentication;

import lombok.val;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class TdwtfMinecraftUserManager implements UserManager {
	private final Map<String, User> _users = new HashMap<>();

	@Override
	public User getUserByName( String username ) throws FtpException {
		return _users.get( username );
	}

	@Override
	public String[] getAllUserNames() throws FtpException {
		return (String[])_users.keySet().toArray();
	}

	@Override
	public void delete( String username ) throws FtpException {
		_users.remove( username );
	}

	@Override
	public void save( User user ) throws FtpException {
		_users.put( user.getName(), user );
	}

	@Override
	public boolean doesExist( String username ) throws FtpException {
		return _users.containsKey( username );
	}

	@Override
	public User authenticate( Authentication authentication ) throws AuthenticationFailedException {
		if( !(authentication instanceof UsernamePasswordAuthentication) ) throw new AuthenticationFailedException();
		val usernamePasswordAuthentication = (UsernamePasswordAuthentication)authentication;
		val user = _users.getOrDefault( usernamePasswordAuthentication.getUsername(), null );
		if( user == null ) throw new AuthenticationFailedException();
		if( !user.getEnabled() ) throw new AuthenticationFailedException();
		if( !user.getPassword().equals( usernamePasswordAuthentication.getPassword() ) ) throw new AuthenticationFailedException();
		return user;
	}

	@Override
	public String getAdminName() throws FtpException {
		return "root";
	}

	@Override
	public boolean isAdmin( String username ) throws FtpException {
		return getAdminName().equals( username );
	}
}
