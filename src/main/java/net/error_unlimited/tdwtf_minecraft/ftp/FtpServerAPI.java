package net.error_unlimited.tdwtf_minecraft.ftp;

import dan200.computercraft.api.lua.IComputerSystem;
import dan200.computercraft.api.lua.ILuaAPI;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RequiredArgsConstructor
public class FtpServerAPI implements ILuaAPI {
	@Getter
	private final String[] _names = {};

	@Getter
	private final IComputerSystem _computerSystem;

	@Getter
	private final TdwtfMinecraftRootFtpFile _rootFtpFile;

	@Override
	public void startup() {
		LOGGER.debug( "{}::startup", getClass().getName() );
		val fs = _computerSystem.getFileSystem();
		if( fs != null ) {
			_rootFtpFile.addComputerFileSystem( _computerSystem.getID(), fs );
		}
	}

	@Override
	public void shutdown() {
		LOGGER.debug( "{}::shutdown", getClass().getName() );
		val fs = _computerSystem.getFileSystem();
		if( fs != null ) {
			_rootFtpFile.removeComputerFileSystem( _computerSystem.getID() );
		}
	}
}
