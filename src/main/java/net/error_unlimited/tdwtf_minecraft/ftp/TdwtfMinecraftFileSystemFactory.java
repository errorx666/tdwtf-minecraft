package net.error_unlimited.tdwtf_minecraft.ftp;

import org.apache.ftpserver.ftplet.FileSystemFactory;
import org.apache.ftpserver.ftplet.FileSystemView;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.User;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RequiredArgsConstructor
public class TdwtfMinecraftFileSystemFactory implements FileSystemFactory {
	@Getter
	public final TdwtfMinecraftRootFtpFile _rootFtpFile;

	@Override
	public FileSystemView createFileSystemView( User user ) throws FtpException {
		return new TdwtfMinecraftFileSystemView( _rootFtpFile, user );
	}
}
