package net.error_unlimited.tdwtf_minecraft.ftp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.ftpserver.ftplet.FtpFile;

import dan200.computercraft.api.filesystem.IFileSystem;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class TdwtfMinecraftRootFtpFile implements FtpFile {
	private Map<Integer, IFileSystem> _fileSystems = new HashMap<>();

	public IFileSystem getFileSystem( int computerId ) {
		return _fileSystems.get( computerId );
	}

	public void addComputerFileSystem( int computerId, IFileSystem fileSystem ) {
		assert !_fileSystems.containsKey( computerId );
		_fileSystems.put( computerId, fileSystem );
	}

	public void removeComputerFileSystem( int computerId ) {
		_fileSystems.remove( computerId );
	}

	public String getAbsolutePath() {
		return "/";
	}

	@Override
	public boolean isHidden() {
		return false;
	}

	@Override
	public boolean doesExist() {
		return true;
	}

	@Override
	public boolean isReadable() {
		return true;
	}

	@Override
	public boolean isWritable() {
		return false;
	}

	@Override
	public boolean isRemovable() {
		return false;
	}

	@Override
	public String getOwnerName() {
		return "root";
	}

	@Override
	public String getGroupName() {
		return "root";
	}

	@Override
	public int getLinkCount() {
		return 1;
	}

	@Override
	public long getLastModified() {
		return 0L;
	}

	@Override
	public boolean setLastModified( long time ) {
		return false;
	}

	@Override
	public long getSize() {
		return 0L;
	}

	@Override
	public Object getPhysicalFile() {
		return null;
	}

	@Override
	public boolean mkdir() {
		return false;
	}

	@Override
	public boolean delete() {
		return false;
	}

	@Override
	public boolean move( FtpFile destination ) {
		return false;
	}

	@Override
	public List<? extends FtpFile> listFiles() {
		return _fileSystems
			.entrySet()
			.stream()
			.map( e -> new TdwtfMinecraftComputerFileSystemFtpFile( e.getKey(), e.getValue(), "/" ) )
			.collect( Collectors.toList() );
	}

	@Override
	public OutputStream createOutputStream( long offset ) throws IOException {
		return null;
	}

	@Override
	public InputStream createInputStream( long offset ) throws IOException {
		return null;
	}

	@Override
	public String getName() {
		return "/";
	}

	@Override
	public boolean isDirectory() {
		return true;
	}

	@Override
	public boolean isFile() {
		return false;
	}
}
