package net.error_unlimited.tdwtf_minecraft.ftp;

import java.util.regex.Pattern;

import org.apache.ftpserver.ftplet.FileSystemView;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.FtpFile;
import org.apache.ftpserver.ftplet.User;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.val;
import lombok.var;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RequiredArgsConstructor
public class TdwtfMinecraftFileSystemView implements FileSystemView {
	@Getter
	private final TdwtfMinecraftRootFtpFile _rootFtpFile;

	@Getter
	private final User _user;

	private FtpFile _cwd;

	@Override
	public FtpFile getHomeDirectory() throws FtpException {
		return _rootFtpFile;
	}

	@Override
	public FtpFile getWorkingDirectory() throws FtpException {
		if( _cwd == null || _cwd instanceof TdwtfMinecraftInvalidFtpFile ) return _rootFtpFile;
		else return _cwd;
	}

	@Override
	public boolean changeWorkingDirectory( String dir ) throws FtpException {
		val cwd = getFile( dir );
		if( cwd == null || cwd instanceof TdwtfMinecraftInvalidFtpFile ) return false;
		_cwd = cwd;
		return true;
	}

	private static final Pattern _patternPath = Pattern.compile( "^/(?:([0-9]+)(/.*?)?)?/?$" );
	@Override
	public FtpFile getFile( String file ) throws FtpException {
		val absolutePath = FtpPath.resolve( getWorkingDirectory().getAbsolutePath(), file );
		assert absolutePath.startsWith( "/" );
		if( "/".equals( absolutePath ) ) return _rootFtpFile;
		val matcher = _patternPath.matcher( absolutePath );
		if( !matcher.matches() ) return new TdwtfMinecraftInvalidFtpFile( absolutePath );
		val computerId = Integer.valueOf( matcher.group( 1 ), 10 );
		val fs = _rootFtpFile.getFileSystem( computerId );
		var internalPath = matcher.group( 2 );
		if( internalPath == null ) internalPath = "/";
		assert internalPath.startsWith( "/" );
		return new TdwtfMinecraftComputerFileSystemFtpFile( computerId, fs, internalPath );
	}

	@Override
	public boolean isRandomAccessible() throws FtpException {
		return false;
	}

	@Override
	public void dispose() {}
}
