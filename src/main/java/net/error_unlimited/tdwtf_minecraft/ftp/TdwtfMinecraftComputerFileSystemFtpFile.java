package net.error_unlimited.tdwtf_minecraft.ftp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.Channels;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;

import org.apache.ftpserver.ftplet.FtpFile;

import dan200.computercraft.api.filesystem.IFileSystem;
import lombok.Getter;
import lombok.val;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class TdwtfMinecraftComputerFileSystemFtpFile implements FtpFile {
	@Getter
	private final int _computerId;

	@Getter
	private final IFileSystem _fileSystem;

	@Getter
	private final String _internalPath;

	public TdwtfMinecraftComputerFileSystemFtpFile( int computerId, IFileSystem fileSystem, String internalPath ) {
		_computerId = computerId;
		_fileSystem = fileSystem;
		_internalPath = FtpPath.resolve( "/", internalPath );
	}

	public String getAbsolutePath() {
		return FtpPath.join( Integer.valueOf( _computerId ).toString(), _internalPath );
	}

	@Override
	public boolean isHidden() {
		return getName().startsWith( "." );
	}

	@Override
	public boolean doesExist() {
		try {
			return _fileSystem.exists( _internalPath );
		} catch( IOException ex ) {
			throw new RuntimeException( ex );
		}
	}

	@Override
	public boolean isReadable() {
		return doesExist();
	}

	public boolean isRoot() {
		return "/".equals( _internalPath );
	}

	public boolean isRom() {
		return _internalPath.startsWith( "/rom/" ) || "/rom".equals( _internalPath );
	}

	@Override
	public boolean isWritable() {
		return !isRoot() && !isRom();
	}

	@Override
	public boolean isRemovable() {
		return isWritable();
	}

	@Override
	public String getOwnerName() {
		return "root";
	}

	@Override
	public String getGroupName() {
		return "root";
	}

	@Override
	public int getLinkCount() {
		return doesExist() ? 1 : 0;
	}

	@Override
	public long getLastModified() {
		try {
			return _fileSystem.getAttributes( _internalPath ).creationTime().toMillis();
		} catch( IOException ex ) {
			throw new RuntimeException( ex );
		}
	}

	@Override
	public boolean setLastModified( long time ) {
		return false;
	}

	@Override
	public long getSize() {
		try {
			return _fileSystem.getSize( _internalPath );
		} catch( IOException ex ) {
			throw new RuntimeException( ex );
		}
	}

	@Override
	public Object getPhysicalFile() {
		return null;
	}

	@Override
	public boolean mkdir() {
		try {
			_fileSystem.makeDirectory( _internalPath );
			return true;
		} catch( IOException ex ) {
			LOGGER.error( ex );
			return false;
		}
	}

	@Override
	public boolean delete() {
		if( !isWritable() ) return false;
		try {
			_fileSystem.delete( _internalPath );
			return true;
		} catch( IOException ex ) {
			LOGGER.error( ex );
			return false;
		}
	}

	@Override
	public boolean move( FtpFile destination ) {
		val path = destination.getAbsolutePath();
		LOGGER.debug( "{}::move {} -> {}", getClass().getName(), getAbsolutePath(), path );
		val prefix = "/" + _computerId + "/";
		if( !path.startsWith( prefix ) ) return false;
		try {
			_fileSystem.move( _internalPath, FtpPath.resolve( "/", path.substring( prefix.length() ) ) );
			return true;
		} catch( IOException ex ) {
			LOGGER.error( ex );
			return false;
		}
	}

	@Override
	public List<? extends FtpFile> listFiles() {
		try {
			val list = Lists.<String>newArrayList();
			_fileSystem.list( _internalPath, list );
			return
				list
				.stream()
				.map( f -> new TdwtfMinecraftComputerFileSystemFtpFile( _computerId, _fileSystem, FtpPath.resolve( _internalPath, f ) ) )
				.collect( Collectors.toList() );
		} catch( IOException ex ) {
			throw new RuntimeException( ex );
		}
	}

	@Override
	public OutputStream createOutputStream( long offset ) throws IOException {
		LOGGER.debug( "{}::createOutputStream {} {}", getClass().getName(), getAbsolutePath(), offset );
		assert offset == 0L;
		return Channels.newOutputStream( _fileSystem.openForWrite( _internalPath ) );
	}

	@Override
	public InputStream createInputStream( long offset ) throws IOException {
		LOGGER.debug( "{}::createInputStream {} {}", getClass().getName(), getAbsolutePath(), offset );
		val stream = Channels.newInputStream( _fileSystem.openForRead( _internalPath ) );
		stream.skip( offset );
		return stream;
	}

	@Override
	public String getName() {
		val path = getAbsolutePath();
		assert path.startsWith( "/" );
		return path.substring( path.lastIndexOf( "/" ) + 1 );
	}

	@Override
	public boolean isDirectory() {
		try {
			// return _fileSystem.isDirectory( _internalPath );

			// HACK: workaround for bug - fixed in future version
			if( !_fileSystem.exists( _internalPath ) ) return false;
			if( !_fileSystem.isDirectory( _internalPath ) ) return false;
			try {
				_fileSystem.list( _internalPath, Lists.newArrayList() );
				return true;
			} catch( IOException ex ) {
				return false;
			}
		} catch( IOException ex ) {
			throw new RuntimeException( ex );
		}
	}

	@Override
	public boolean isFile() {
		return !isDirectory();
	}
}
