package net.error_unlimited.tdwtf_minecraft.ftp;

import java.util.ArrayList;
import java.util.Arrays;

import com.google.common.collect.Lists;

import lombok.val;
import lombok.var;
import lombok.experimental.UtilityClass;

@UtilityClass
public class FtpPath {
	public static String join( String... parts ) {
		parts =
			Arrays.stream( parts )
			.flatMap( p -> Arrays.stream( p.split( "[\\/]" ) ) )
			.toArray( String[]::new );
		val list = new ArrayList<String>( parts.length );
		var first = true;
		for( val part : parts ) {
			if( first ) {
				first = false;
				if( "~".equals( part ) ) continue; // use / for $HOME
			}
			if( part.length() == 0 || ".".equals( part ) ) continue;
			else if( "..".equals( part ) ) list.remove( list.size() - 1 );
			else list.add( part );
		}
		return "/" + String.join( "/", list );
	}

	public static String resolve( String cwd, String... parts ) {
		for( var part : parts ) {
			if( part.length() == 0 ) continue;
			if( part.startsWith( "/" ) || part.startsWith( "~/" ) || "~".equals( part ) ) return join( parts );
			break;
		}
		return join( Lists.asList( cwd, parts ).stream().toArray( String[]::new ) );
	}
}
