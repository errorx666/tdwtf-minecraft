package net.error_unlimited.tdwtf_minecraft.entities;

import com.google.common.collect.ImmutableList;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import lombok.extern.log4j.Log4j2;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@Log4j2
@OnlyIn(Dist.CLIENT)
public class ZipLineModel extends Model {
	private final ModelRenderer _head;
	private final ModelRenderer _hair;
	private final ModelRenderer _body;
	private final ModelRenderer _leg0;
	private final ModelRenderer _leg1;
	private final ModelRenderer _leg2;
	private final ModelRenderer _leg3;

	public static final RenderType RENDER_TYPE = RenderType.translucent();

	private static RenderType getRenderType( ResourceLocation resourceLocation ) {
		return RENDER_TYPE;
	}

	public ZipLineModel() {
		super( ZipLineModel::getRenderType );
		LOGGER.debug( "{}::ctor", getClass().getName() );
		float f = 0.f;
		_head = new ModelRenderer(this, 0, 0);
		_head.addBox(-4.0F, -8.0F, -4.0F, 8.0F, 8.0F, 8.0F, f );
		_head.setPos(0.0F, 6.0F, 0.0F);
		_hair = new ModelRenderer(this, 32, 0);
		_hair.addBox(-4.0F, -8.0F, -4.0F, 8.0F, 8.0F, 8.0F, f + 0.5F);
		_hair.setPos(0.0F, 6.0F, 0.0F);
		_body = new ModelRenderer(this, 16, 16);
		_body.addBox(-4.0F, 0.0F, -2.0F, 8.0F, 12.0F, 4.0F, f);
		_body.setPos(0.0F, 6.0F, 0.0F);
		_leg0 = new ModelRenderer(this, 0, 16);
		_leg0.addBox(-2.0F, 0.0F, -2.0F, 4.0F, 6.0F, 4.0F, f);
		_leg0.setPos(-2.0F, 18.0F, 4.0F);
		_leg1 = new ModelRenderer(this, 0, 16);
		_leg1.addBox(-2.0F, 0.0F, -2.0F, 4.0F, 6.0F, 4.0F, f);
		_leg1.setPos(2.0F, 18.0F, 4.0F);
		_leg2 = new ModelRenderer(this, 0, 16);
		_leg2.addBox(-2.0F, 0.0F, -2.0F, 4.0F, 6.0F, 4.0F, f);
		_leg2.setPos(-2.0F, 18.0F, -4.0F);
		_leg3 = new ModelRenderer(this, 0, 16);
		_leg3.addBox(-2.0F, 0.0F, -2.0F, 4.0F, 6.0F, 4.0F, f);
		_leg3.setPos(2.0F, 18.0F, -4.0F);
	}

	public Iterable<ModelRenderer> parts() {
		return ImmutableList.of( _head, _body, _leg0, _leg1, _leg2, _leg3 );
	}

	public void setupAnim( ZipLineEntity entity, float p_225597_2_, float p_225597_3_, float p_225597_4_, float p_225597_5_, float p_225597_6_ ) {
		_head.yRot = p_225597_5_ * ((float)Math.PI / 180F);
		_head.xRot = p_225597_6_ * ((float)Math.PI / 180F);
		_leg0.xRot = MathHelper.cos(p_225597_2_ * 0.6662F) * 1.4F * p_225597_3_;
		_leg1.xRot = MathHelper.cos(p_225597_2_ * 0.6662F + (float)Math.PI) * 1.4F * p_225597_3_;
		_leg2.xRot = MathHelper.cos(p_225597_2_ * 0.6662F + (float)Math.PI) * 1.4F * p_225597_3_;
		_leg3.xRot = MathHelper.cos(p_225597_2_ * 0.6662F) * 1.4F * p_225597_3_;
	}

	@Override
	public void renderToBuffer( MatrixStack matrixStack, IVertexBuilder vb, int p_225598_3_, int p_225598_4_, float p_225598_5_, float p_225598_6_, float p_225598_7_, float p_225598_8_ ) {
		this.parts().forEach( part -> {
			part.render( matrixStack, vb, p_225598_3_, p_225598_4_, p_225598_5_, p_225598_6_, p_225598_7_, p_225598_8_ );
		} );
	}
}
