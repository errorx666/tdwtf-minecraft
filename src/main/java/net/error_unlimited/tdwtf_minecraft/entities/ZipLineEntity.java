package net.error_unlimited.tdwtf_minecraft.entities;

import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.TdwtfMinecraftMod;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.network.play.server.SSpawnObjectPacket;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Log4j2
public class ZipLineEntity extends Entity {
	public static final ResourceLocation RESOURCE_LOCATION = new ResourceLocation( TdwtfMinecraftMod.MOD_ID, "zip_line_entity_type" );

	public static final EntityType<ZipLineEntity> ENTITY_TYPE =
		EntityType.Builder.of( ZipLineEntity::new, EntityClassification.MISC )
		.noSave()
		// .noSummon()
		.fireImmune()
		.sized( 5.f, 8.f )
		.build( "" );

	static {
		ENTITY_TYPE.setRegistryName( RESOURCE_LOCATION );
	}

	public static final DataParameter<Integer> DATA_FOO_ID = EntityDataManager.defineId( ZipLineEntity.class, DataSerializers.INT );

	private int getFoo() {
		return entityData.get( DATA_FOO_ID );
	}

	private void setFoo( int foo ) {
		entityData.set( DATA_FOO_ID, foo );
	}

	public ZipLineEntity( EntityType<?> entityType, World world ) {
		super( entityType, world );
		LOGGER.debug( "{}::ctor", getClass().getName() );
	}
	@Override
	protected void defineSynchedData() {
		entityData.define( DATA_FOO_ID, 80 );
	}

	@Override
	protected void readAdditionalSaveData( CompoundNBT tag ) {
		setFoo( tag.getShort( "foo" ) );
	}

	@Override
	protected void addAdditionalSaveData( CompoundNBT tag ) {
		tag.putShort( "foo", (short)getFoo() );
	}

	@Override
	public IPacket<?> getAddEntityPacket() {
		return new SSpawnObjectPacket( this );
	}

	@Override
	public void tick() {

	}

	@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
	public static class Registration {
		@SubscribeEvent
		public static void registerEntityType( final RegistryEvent.Register<EntityType<?>> e ) {
			LOGGER.debug( "{}::registerEntityType", Registration.class.getName() );
			e.getRegistry().register( ENTITY_TYPE );
		}
	}
}
