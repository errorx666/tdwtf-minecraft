package net.error_unlimited.tdwtf_minecraft.entities;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.TdwtfMinecraftMod;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.culling.ClippingHelperImpl;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.model.CreeperModel;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

@Log4j2
@OnlyIn(Dist.CLIENT)
public class ZipLineEntityRenderer extends EntityRenderer<ZipLineEntity> {
	public static ResourceLocation TEXTURE_LOCATION = new ResourceLocation( "minecraft", "textures/entity/end_crystal/end_crystal.png" );
	private static final RenderType RENDER_TYPE = RenderType.translucent();

	// private ZipLineModel _model = new ZipLineModel();
	private CreeperModel<ZipLineEntity> _model = new CreeperModel<>();

	private ZipLineEntityRenderer( EntityRendererManager entityRendererManager ) {
		super( entityRendererManager );
		LOGGER.debug( "{}::ctor", getClass().getName() );
	}

	// public ZipLineModel getModel() {
	// 	return _model;
	// }

	@Override
	public void render( ZipLineEntity entity, float p_225623_2_, float time, MatrixStack matrixStack, IRenderTypeBuffer buffer, int uv2 ) {
		IVertexBuilder vb = buffer.getBuffer( RENDER_TYPE );
		int overlayCoords = OverlayTexture.pack( OverlayTexture.u( 0.f ), OverlayTexture.v( false ) );
		_model.renderToBuffer( matrixStack, vb, uv2, overlayCoords, 1.f, 1.f, 1.f, 1.f );
		super.render( entity, p_225623_2_, time, matrixStack, buffer, uv2 );
	}

	@Override
	public boolean shouldRender( ZipLineEntity entity, ClippingHelperImpl clippingHelper, double p_225626_3_, double p_225626_5_, double p_225626_7_ ) {
		return true;
	}

	@Override
	public ResourceLocation getTextureLocation( ZipLineEntity entity ) {
		return TEXTURE_LOCATION;
	}

	@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
	public static class Registration {
		@OnlyIn(Dist.CLIENT)
		@SubscribeEvent
		public static void registerClientSetup( FMLClientSetupEvent e ) {
			LOGGER.debug( "{}::registerClientSetup", Registration.class.getName() );
			EntityRendererManager erm = e.getMinecraftSupplier().get().getEntityRenderDispatcher();
			erm.register( ZipLineEntity.ENTITY_TYPE, new ZipLineEntityRenderer( erm ) );
		}
	}
}
