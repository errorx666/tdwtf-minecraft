package net.error_unlimited.tdwtf_minecraft.ai.goals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import lombok.val;
import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.blocks.IllusionBlock;
import net.minecraft.block.Block;
import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.ai.RandomPositionGenerator;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.pathfinding.Path;
import net.minecraft.pathfinding.PathNavigator;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

@Log4j2
public class AvoidBlockGoal<T extends Block> extends Goal {
	private final CreatureEntity _creatureEntity;
	private final Class<T> _classToAvoid;
	private final double _avoidDistance;
	private final double _nearSpeed;
	private final double _farSpeed;
	private final PathNavigator _navigation;
	private Path _path;
	private Vec3d _avoidTarget;

	public AvoidBlockGoal( final CreatureEntity creatureEntity, final Class<T> classToAvoid, final float avoidDistance, final double nearSpeed, final double farSpeed ) {
		_creatureEntity = creatureEntity;
		_classToAvoid = classToAvoid;
		_avoidDistance = avoidDistance;
		_nearSpeed = nearSpeed;
		_farSpeed = farSpeed;
		_navigation = creatureEntity.getNavigation();
	}

	@Override
	public boolean canUse() {
		val w = _creatureEntity.level;
		assert( w != null );
		val aabb = _creatureEntity.getBoundingBox().inflate( _avoidDistance, 3.0D, _avoidDistance );
		val minPos = new BlockPos( aabb.minX, aabb.minY, aabb.minZ );
		val maxPos = new BlockPos( aabb.maxX, aabb.maxY, aabb.maxZ );

		final List<Vec3d> positionsToAvoid =
			BlockPos.betweenClosedStream( minPos, maxPos )
			.filter( pos -> {
				val bs = w.getBlockState( pos );
				if( bs == null ) return false;
				val b = bs.getBlock();
				if( b == null ) return false;
				if( b instanceof IllusionBlock ) {
					val eb = ((IllusionBlock)b).getEmulatedBlock( w, pos );
					if( eb.getClass() == _classToAvoid ) return true;
				}
				return b.getClass() == _classToAvoid;
			} )
			.map( Vec3d::new )
			.collect( Collectors.toList() );

		if( positionsToAvoid.isEmpty() ) {
			_avoidTarget = null;
			return false;
		}
		final Map<Vec3d, Double> distanceSqCache = new HashMap<Vec3d, Double>();

		for( final Vec3d positionToAvoid : positionsToAvoid ) {
			distanceSqCache.put( positionToAvoid, _creatureEntity.distanceToSqr( positionToAvoid ) );
		}

		positionsToAvoid.sort( ( final Vec3d v1, final Vec3d v2 ) -> {
			final double d1 = distanceSqCache.get( v1 );
			final double d2 = distanceSqCache.get( v2 );
			return ( d1 == d2 ) ? 0 : ( d1 < d2 ) ? -1 : 1;
		} );

		_avoidTarget = positionsToAvoid.get( 0 );
		final Vec3d vec3d = RandomPositionGenerator.getPosAvoid( _creatureEntity, 16, 7, _avoidTarget );
		if( vec3d == null || _avoidTarget.distanceToSqr( vec3d ) < distanceSqCache.get( _avoidTarget ) ) {
			return false;
		} else {
			_path = _navigation.createPath( vec3d.x, vec3d.y, vec3d.z, 0 );
			return _path != null;
		}
	}

	@Override
	public boolean canContinueToUse() {
		return !_navigation.isDone();
	}

	@Override
	public void start() {
		_navigation.moveTo( _path, _farSpeed );
	}

	@Override
	public void stop() {
		_avoidTarget = null;
	}

	@Override
	public void tick() {
		if( _creatureEntity.distanceToSqr( _avoidTarget ) < 49.D ) {
			_creatureEntity.getNavigation().setSpeedModifier( _nearSpeed );
		} else {
			_creatureEntity.getNavigation().setSpeedModifier( _farSpeed );
		}
	}
}
