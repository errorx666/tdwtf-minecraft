package net.error_unlimited.tdwtf_minecraft.errorBot;

import dan200.computercraft.api.lua.IComputerSystem;
import dan200.computercraft.api.lua.ILuaAPI;
import dan200.computercraft.api.lua.LuaException;
import dan200.computercraft.api.lua.LuaFunction;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.errorBot.messages.ErrorBotComputerRxMessage;
import net.error_unlimited.tdwtf_minecraft.errorBot.messages.ErrorBotComputerTxMessage;

@Log4j2
@RequiredArgsConstructor
public class ErrorBotAPI implements ILuaAPI {
	@Getter
	private final String[] _names = { "errorBot" };

	@Getter
	private final IComputerSystem _computerSystem;

	@Getter
	private final ErrorBotServer _errorBotServer;

	private boolean _startupCalled = false;
	private boolean _shutdownCalled = false;
	private final Subject<Boolean> _shutdown = PublishSubject.create();

	@Override
	public void startup() {
		LOGGER.debug( "{}::startup", getClass().getName() );
		assert( !_startupCalled );
		assert( !_shutdownCalled );
		_startupCalled = true;
		_errorBotServer.getMessages( ErrorBotComputerRxMessage.class )
		.filter( m -> m.getComputerId() == _computerSystem.getID() )
		.takeUntil( _shutdown )
		.subscribe( message -> {
			_computerSystem.queueEvent( "errorBot", message.getLuaValue() );
		} );
	}

	@Override
	public void shutdown() {
		LOGGER.debug( "{}::shutdown", getClass().getName() );
		assert( _startupCalled );
		assert( !_shutdownCalled );
		_shutdownCalled = true;
		_shutdown.onNext( true );
		_shutdown.onComplete();
	}

	@LuaFunction
	public void send( String message ) throws LuaException {
		assert( _startupCalled );
		assert( !_shutdownCalled );
		_errorBotServer.transmit( new ErrorBotComputerTxMessage(
			_computerSystem.getID(),
			_computerSystem.getLabel(),
			message
		) );
	}
}
