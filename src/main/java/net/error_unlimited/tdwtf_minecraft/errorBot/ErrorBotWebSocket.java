package net.error_unlimited.tdwtf_minecraft.errorBot;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketAdapter;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class ErrorBotWebSocket extends WebSocketAdapter {
	private ErrorBotWebSocketContext _context;
	public ErrorBotWebSocket( ErrorBotWebSocketContext ctx ) {
		LOGGER.debug( "{}::ctor", getClass().getName() );
		_context = ctx;
	}

	private Subject<Boolean> _closed = PublishSubject.create();
	public Observable<Boolean> getClosed() {
		return _closed;
	}

	@Getter
	private Subject<String> _messagesTx = PublishSubject.create();

	private Subject<String> _messagesRxSubject = PublishSubject.create();

	@Getter
	private Observable<String> _messagesRx = _messagesRxSubject.share();

	@Override
	public void onWebSocketConnect( Session sess ) {
		LOGGER.debug( "{}::onWebSocketConnect {}", getClass().getName(), sess.getRemoteAddress().getHostName() );
		_messagesTx
		.takeUntil( _closed )
		.onErrorComplete()
		.doOnComplete( () -> {
			sess.close();
		} )
		.subscribe( message -> {
			getRemote().sendString( message );
		} );
		_context.getSockets().onNext( this );
		super.onWebSocketConnect( sess );
	}

	@Override
	public void onWebSocketText( String message ) {
		// LOGGER.debug( "{}::onWebSocketText {}> {}", getClass().getName(), getSession().getRemoteAddress().getHostName(), message );
		_messagesRxSubject.onNext( message );
		super.onWebSocketText( message );
	}

	@Override
	public void onWebSocketClose( int statusCode, String reason ) {
		LOGGER.debug( "{}::onWebSocketClose {}> {} {}", getClass().getName(), getSession().getRemoteAddress().getHostName(), statusCode, reason );
		_closed.onNext( true );
		_closed.onComplete();
		_messagesRxSubject.onComplete();
		_messagesTx.onComplete();
		super.onWebSocketClose( statusCode, reason );
	}

	@Override
	public void onWebSocketError( Throwable cause ) {
		LOGGER.fatal( "{}::onWebSocketError {}> {}", getClass().getName(), getSession().getRemoteAddress().getHostName(), cause );
		_closed.onNext( true );
		_closed.onComplete();
		_messagesRxSubject.onComplete();
		_messagesTx.onComplete();
		super.onWebSocketError( cause );
	}
}
