package net.error_unlimited.tdwtf_minecraft.errorBot;

// import sun.security.x509.AlgorithmId;
// import sun.security.x509.CertificateSerialNumber;
// import sun.security.x509.CertificateSubjectName;
// import sun.security.x509.CertificateIssuerName;
// import sun.security.x509.CertificateValidity;
// import sun.security.x509.CertificateVersion;
// import sun.security.x509.CertificateX509Key;
// import sun.security.x509.CertificateAlgorithmId;
// import sun.security.x509.X500Name;
// import sun.security.x509.X509CertImpl;
// import sun.security.x509.X509CertInfo;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.plugins.RxJavaPlugins;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.reactivex.rxjava3.subjects.BehaviorSubject;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;
import lombok.val;
import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.Json;
import net.error_unlimited.tdwtf_minecraft.errorBot.messages.ErrorBotAuthMessage;
import net.error_unlimited.tdwtf_minecraft.errorBot.messages.ErrorBotAuthRequestMessage;
import net.error_unlimited.tdwtf_minecraft.errorBot.messages.ErrorBotPingMessage;
import net.error_unlimited.tdwtf_minecraft.errorBot.messages.ErrorBotPongMessage;
import net.error_unlimited.tdwtf_minecraft.errorBot.messages.ErrorBotUnidentifiedMessage;

// import org.eclipse.jetty.http.HttpVersion;
import org.eclipse.jetty.server.Connector;
// import org.eclipse.jetty.server.HttpConfiguration;
// import org.eclipse.jetty.server.HttpConnectionFactory;
// import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
// import org.eclipse.jetty.server.SslConnectionFactory;
// import org.eclipse.jetty.util.ssl.SslContextFactory;

import dan200.computercraft.api.ComputerCraftAPI;

// import java.io.IOException;
// import java.math.BigInteger;
import java.nio.ByteBuffer;
// import java.security.InvalidKeyException;
// import java.security.KeyPair;
// import java.security.KeyStore;
// import java.security.KeyStoreException;
// import java.security.NoSuchAlgorithmException;
// import java.security.NoSuchProviderException;
// import java.security.SecureRandom;
// import java.security.SignatureException;
// import java.security.cert.Certificate;
// import java.security.cert.CertificateException;
// import java.util.Date;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;


@Log4j2
public class ErrorBotServer {
	public static final Scheduler SCHEDULER = Schedulers.newThread();
	static {
		LOGGER.debug( "{}::static", ErrorBotServer.class.getName() );
		RxJavaPlugins.createNewThreadScheduler( new ThreadFactory(){
			@Override
			public Thread newThread( Runnable r ) {
				Thread thread = newThread( r );
				thread.setName( "Error Bot Thread" );
				return thread;
			}
		} );
	}

	private static final ByteBuffer EMPTY_BYTES = ByteBuffer.allocate( 0 );
	private final Subject<Boolean> _shutdown = PublishSubject.create();
	private final Subject<String> _messagesTx = PublishSubject.create();
	private final Subject<ErrorBotUnidentifiedMessage> _messagesRx = PublishSubject.create();

	private final ErrorBotServerOptions _options;
	public ErrorBotServer( ErrorBotServerOptions options ) {
		LOGGER.debug( "{}::ctor", getClass().getName(), options );
		assert( options != null );
		assert( options.getPort() > 0 );
		assert( options.getApiKeys() != null );
		_options = options;
	}

	// @SuppressWarnings( "sunapi" )
	// private Certificate generateCertificate( KeyPair keyPair ) {
	// 	try {
	// 		val info = new X509CertInfo();
	// 		val from = new Date();
	// 		val to = new Date( from.getTime() + 365l * 24l * 60l * 60l * 1000l );
	// 		val interval = new CertificateValidity( from, to );
	// 		val sn = new BigInteger( 64, new SecureRandom() );
	// 		val owner = new X500Name( "localhost" );
	// 		val algorithm = new AlgorithmId( AlgorithmId.sha512WithECDSA_oid );

	// 		info.set( X509CertInfo.VALIDITY, interval );
	// 		info.set( X509CertInfo.SERIAL_NUMBER, new CertificateSerialNumber( sn ) );
	// 		info.set( X509CertInfo.SUBJECT, new CertificateSubjectName( owner ) );
	// 		info.set( X509CertInfo.ISSUER, new CertificateIssuerName( owner ) );
	// 		info.set( X509CertInfo.KEY, new CertificateX509Key( keyPair.getPublic() ) );
	// 		info.set( X509CertInfo.VERSION, new CertificateVersion( CertificateVersion.V3 ) );
	// 		info.set( X509CertInfo.ALGORITHM_ID, new CertificateAlgorithmId( algorithm ) );

	// 		val cert = new X509CertImpl( info );
	// 		cert.sign( keyPair.getPrivate(), algorithm.getName() );
	// 		return cert;
	// 	} catch( CertificateException|SignatureException|IOException|NoSuchProviderException|InvalidKeyException|NoSuchAlgorithmException ex ) {
	// 		throw new RuntimeException( ex );
	// 	}
	// }

	public void start() {
		LOGGER.debug( "{}::start", getClass().getName() );

		ComputerCraftAPI.registerAPIFactory( computerSystem -> new ErrorBotAPI( computerSystem, this ) );
		if( _options.isLogJetty() ) {
			org.eclipse.jetty.util.log.Log.setLog( new JettyLog4jLogger( LOGGER.getName(), LOGGER ) );
		}

		// val certAlias = "jetty";
		// KeyStore keyStore;
		// try {
		// 	val keyPair = _options.getKeyPair();
		// 	keyStore = KeyStore.getInstance( KeyStore.getDefaultType() );
		// 	keyStore.load( null, null );
		// 	keyStore.setKeyEntry(
		// 		certAlias,
		// 		keyPair.getPrivate().getEncoded(),
		// 		new Certificate[] { generateCertificate( keyPair ) }
		// 	);
		// } catch( CertificateException|KeyStoreException|IOException|NoSuchAlgorithmException ex ) {
		// 	throw new RuntimeException( ex );
		// }

		// val httpsConfiguration = new HttpConfiguration();
		// httpsConfiguration.setSecureScheme( "https" );
		// httpsConfiguration.setSecurePort( _options.getPort() );
		// httpsConfiguration.addCustomizer( new SecureRequestCustomizer( false ) );

		// val sslContextFactory = new SslContextFactory.Server();
		// sslContextFactory.setSniRequired( false );
		// sslContextFactory.setTrustAll( true );
		// sslContextFactory.setHostnameVerifier( null );
		// sslContextFactory.setKeyStore( keyStore );
		// sslContextFactory.setCertAlias( certAlias );

		val server = new Server();

		val sockets = PublishSubject.<ErrorBotWebSocket>create();
		val ctx = new ErrorBotWebSocketContext( sockets );

		val handler = new ErrorBotWebSocketHandler( ctx );
		server.setHandler( handler );

		val connector = new ServerConnector(
			server
			// new SslConnectionFactory( sslContextFactory, HttpVersion.HTTP_1_1.asString() ),
			// new HttpConnectionFactory( httpsConfiguration )
		);
		connector.setPort( _options.getPort() );
		server.setConnectors( new Connector[] { connector } );

		sockets
		.observeOn( SCHEDULER )
		.subscribe( socket -> {
			val session = socket.getSession();
			assert( session != null );
			val host = session.getRemoteAddress().getHostString();
			val authenticated = BehaviorSubject.createDefault( false );
			val closed = socket.getClosed();
			val messagesTx = socket.getMessagesTx();
			closed.subscribe( e -> {
				LOGGER.info( "{}: Connection closed with {}.", getClass().getName(), host );
			} );

			val messagesRx =
				getMessagesStr( socket.getMessagesRx() )
				.takeUntil( _shutdown )
				.takeUntil( closed );

			getMessagesUnk( messagesRx, ErrorBotAuthMessage.class )
			.observeOn( SCHEDULER )
			.subscribe( m -> {
				authenticated.onNext( _options.getApiKeys().contains( m.getKey() ) );
				if( authenticated.getValue() ) {
					LOGGER.info( "{}: {} is authenticated.", getClass().getName(), host );
				} else {
					LOGGER.info( "{}: {} failed authentication.", getClass().getName(), host );
					session.close();
				}
			} );

			_messagesTx
			.takeUntil( _shutdown )
			.takeUntil( closed )
			.observeOn( SCHEDULER )
			.subscribe( msg -> {
				// LOGGER.info( "tx {}", msg );
				if( authenticated.getValue() ) {
					messagesTx.onNext( msg );
				}
			} );

			messagesRx
			.takeUntil( _shutdown )
			.takeUntil( closed )
			.observeOn( SCHEDULER )
			.subscribe( m -> {
				// LOGGER.info( "rx {}", m.raw );
				if( authenticated.getValue() ) {
					_messagesRx.onNext( m );
				}
			} );

			getMessagesUnk( messagesRx, ErrorBotPingMessage.class )
			.observeOn( SCHEDULER )
			.subscribe( m -> {
				messagesTx.onNext( Json.stringify( new ErrorBotPongMessage() ) );
			} );

			_shutdown
			.takeUntil( closed )
			.observeOn( SCHEDULER )
			.subscribe( e -> {
				messagesTx.onComplete();
				session.close();
			} );

			Observable.interval( 60, TimeUnit.SECONDS )
			.takeUntil( closed )
			.onErrorComplete()
			.observeOn( SCHEDULER )
			.subscribe( e -> {
				messagesTx.onNext( Json.stringify( new ErrorBotPingMessage() ) );
				socket.getRemote().sendPing( EMPTY_BYTES );
			} );

			Observable.timer( 15, TimeUnit.SECONDS )
			.take( 1 )
			.takeUntil( closed )
			.observeOn( SCHEDULER )
			.subscribe( e -> {
				if( !authenticated.getValue() ) {
					LOGGER.info( "{}: {} did not authenticate.", getClass().getName(), host );
					messagesTx.onComplete();
					session.close();
				}
			} );

			messagesTx.onNext( Json.stringify( new ErrorBotAuthRequestMessage() ) );
		} );

		try {
			server.start();
			LOGGER.info( "{}: listening on port {}...", getClass().getName(), _options.getPort() );
			server.join();
		} catch( Exception ex ) {
			LOGGER.fatal( ex );
		}
	}

	public void transmit( Object msg ) {
		Observable.just( msg )
		.map( m -> Json.stringify( m ) )
		.doOnError( LOGGER::fatal )
		.observeOn( ErrorBotServer.SCHEDULER )
		.subscribe( _messagesTx::onNext );
	}

	private <T> Observable<T> getMessagesUnk( Observable<ErrorBotUnidentifiedMessage> raw, Class<T> messageType ) {
		try {
			val typeField = messageType.getField( "TYPE" );
			val type = (String)typeField.get( null );
			assert( type != null );
			return raw
			.filter( m -> type.equals( m.getType() ) )
			.map( m -> Json.parse( m.getRaw(), messageType ) )
			.doOnError( LOGGER::fatal )
			.retry();
		} catch( Exception ex ) {
			LOGGER.fatal( ex );
			throw new RuntimeException( ex );
		}
	}

	private Observable<ErrorBotUnidentifiedMessage> getMessagesStr( Observable<String> raw) {
		return raw.map( m -> {
			ErrorBotUnidentifiedMessage message = Json.parse( m, ErrorBotUnidentifiedMessage.class );
			message.setRaw( m );
			return message;
		} )
		.doOnError( LOGGER::fatal )
		.retry();
	}

	private <T> Observable<T> getMessagesStr( Observable<String> raw, Class<T> messageType ) {
		return getMessagesUnk( getMessagesStr( raw ), messageType );
	}

	public <T> Observable<T> getMessages( Class<T> messageType ) {
		return getMessagesUnk( _messagesRx, messageType );
	}

	public void shutdown() {
		assert( !_shutdown.hasComplete() );
		LOGGER.debug( "{}::shutdown", getClass().getName() );
		_shutdown.onNext( true );
		_shutdown.onComplete();
		_messagesTx.onComplete();
		_messagesRx.onComplete();
	}
}
