package net.error_unlimited.tdwtf_minecraft.errorBot;

import io.reactivex.rxjava3.core.Observer;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class ErrorBotWebSocketContext {
	@Getter
	private final Observer<ErrorBotWebSocket> _sockets;
}
