package net.error_unlimited.tdwtf_minecraft.errorBot;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.eclipse.jetty.util.log.AbstractLogger;
import org.eclipse.jetty.util.log.Logger;

public class JettyLog4jLogger extends AbstractLogger {
	private final String _name;
	private final org.apache.logging.log4j.Logger _logger;

	public JettyLog4jLogger( String name, org.apache.logging.log4j.Logger logger ) {
		_name = name;
		_logger = logger;
	}

	@Override
	protected Logger newLogger( String fullname ) {
		return new JettyLog4jLogger( fullname, LogManager.getLogger( fullname ) );
	}

	@Override
	public String getName() {
		return _name;
	}

	@Override
	public void warn( String msg, Object... args ) {
		_logger.warn( msg, args );
	}

	@Override
	public void warn( Throwable thrown ) {
		_logger.warn( thrown );
	}

	@Override
	public void warn( String msg, Throwable thrown ) {
		_logger.warn( msg, thrown );
	}

	@Override
	public void info( String msg, Object... args ) {
		_logger.info( msg, args );
	}

	@Override
	public void info(Throwable thrown) {
		_logger.info( thrown );
	}

	@Override
	public void info(String msg, Throwable thrown) {
		_logger.info( msg, thrown );
	}

	@Override
	public boolean isDebugEnabled() {
		return true;
	}

	@Override
	public void setDebugEnabled( boolean enabled ) {
		throw new RuntimeException( "not implemented" );
	}

	@Override
	public void debug( String msg, Object... args ) {
		_logger.debug( msg, args );
	}

	@Override
	public void debug( Throwable thrown ) {
		_logger.debug( thrown );
	}

	@Override
	public void debug( String msg, Throwable thrown ) {
		_logger.debug( msg, thrown );
	}

	@Override
	public void ignore( Throwable ignored ) {
		_logger.catching( Level.TRACE, ignored );
	}
}
