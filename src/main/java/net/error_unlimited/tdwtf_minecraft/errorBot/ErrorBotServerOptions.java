package net.error_unlimited.tdwtf_minecraft.errorBot;

import java.security.KeyPair;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorBotServerOptions {
	private int _port;
	@ToString.Exclude
	private Set<String> _apiKeys;
	@ToString.Exclude
	private KeyPair _keyPair;
	private boolean _logJetty;
}
