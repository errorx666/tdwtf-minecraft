package net.error_unlimited.tdwtf_minecraft.errorBot;

import org.eclipse.jetty.websocket.api.WebSocketPolicy;
import org.eclipse.jetty.websocket.server.WebSocketHandler;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.val;
import lombok.extern.log4j.Log4j2;

@Log4j2
@AllArgsConstructor
public class ErrorBotWebSocketHandler extends WebSocketHandler {
	@Getter
	private final ErrorBotWebSocketContext _context;

	@Override
	public void configure( WebSocketServletFactory factory ) {
		val creator = new ErrorBotWebSocketCreator( _context );
		factory.setCreator( creator );
	}

	@Override
	public void configurePolicy( WebSocketPolicy policy ) {
		policy.setIdleTimeout( Integer.MAX_VALUE );
	}
}
