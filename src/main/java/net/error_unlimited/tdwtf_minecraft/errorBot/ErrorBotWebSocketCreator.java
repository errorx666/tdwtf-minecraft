package net.error_unlimited.tdwtf_minecraft.errorBot;

import org.eclipse.jetty.websocket.servlet.ServletUpgradeRequest;
import org.eclipse.jetty.websocket.servlet.ServletUpgradeResponse;
import org.eclipse.jetty.websocket.servlet.WebSocketCreator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.val;
import lombok.extern.log4j.Log4j2;

@Log4j2
@AllArgsConstructor
public class ErrorBotWebSocketCreator implements WebSocketCreator {
	@Getter
	private final ErrorBotWebSocketContext _context;

	@Override
	public Object createWebSocket( ServletUpgradeRequest req, ServletUpgradeResponse res ) {
		LOGGER.debug( "{}::createWebSocket", getClass().getName() );

		val socket = new ErrorBotWebSocket( _context );
		return socket;
	}
}
