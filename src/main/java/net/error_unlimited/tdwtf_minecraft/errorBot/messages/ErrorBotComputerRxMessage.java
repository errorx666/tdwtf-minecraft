package net.error_unlimited.tdwtf_minecraft.errorBot.messages;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.squiddev.cobalt.LuaInteger;
import org.squiddev.cobalt.LuaString;
import org.squiddev.cobalt.LuaTable;
import org.squiddev.cobalt.LuaValue;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.val;
import net.error_unlimited.tdwtf_minecraft.lua.ILuaValue;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorBotComputerRxMessage implements ILuaValue {
	public static final String TYPE = "computerRx";
	public String getType() { return TYPE; }
	public void setType( String type ) { assert( TYPE.equals( type ) ); }

	private int _computerId;
	private String _message;

	public LuaValue getLuaValue() {
		val table = new LuaTable();
		table.rawset( "computerId", LuaInteger.valueOf( getComputerId() ) );
		table.rawset( "message", LuaString.valueOf( getMessage() ) );
		table.rawset( "type", LuaString.valueOf( getType() ) );
		return table;
	}
}
