package net.error_unlimited.tdwtf_minecraft.errorBot.messages;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorBotUnidentifiedMessage {
	private String _type;
	private String _raw;
}
