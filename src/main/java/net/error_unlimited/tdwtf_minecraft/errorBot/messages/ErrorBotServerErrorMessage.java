package net.error_unlimited.tdwtf_minecraft.errorBot.messages;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorBotServerErrorMessage {
	public final static String TYPE = "error";
	public String getType() { return TYPE; }
	public void setType( String type ) { assert( TYPE.equals( type ) ); }
	private String _message;
	private String _stack;
}
