package net.error_unlimited.tdwtf_minecraft.errorBot.messages;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorBotAuthRequestMessage {
	public static final String TYPE = "authRequest";
	public String getType() { return TYPE; }
	public void setType( String type ) { assert( TYPE.equals( type ) ); }
}
