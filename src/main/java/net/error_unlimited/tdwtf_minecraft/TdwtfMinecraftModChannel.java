package net.error_unlimited.tdwtf_minecraft;

import lombok.extern.log4j.Log4j2;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;

@Log4j2
public class TdwtfMinecraftModChannel {
	private TdwtfMinecraftModChannel() {}
	public static final TdwtfMinecraftModChannel INSTANCE = new TdwtfMinecraftModChannel();
	private static final String CHANNEL_NAME = "main";
	private static final String PROTOCOL_VERSION = "1";

	private SimpleChannel _channel;

	public void initialize() {
		LOGGER.debug( "{}::initialize", getClass().getName() );
		assert( _channel == null );
		_channel = NetworkRegistry.newSimpleChannel( new ResourceLocation( TdwtfMinecraftMod.MOD_ID, CHANNEL_NAME ), () -> PROTOCOL_VERSION, PROTOCOL_VERSION::equals, PROTOCOL_VERSION::equals );
	}
}
