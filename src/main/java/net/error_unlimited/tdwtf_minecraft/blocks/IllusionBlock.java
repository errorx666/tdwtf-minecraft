package net.error_unlimited.tdwtf_minecraft.blocks;

import net.error_unlimited.tdwtf_minecraft.TdwtfMinecraftMod;
import net.error_unlimited.tdwtf_minecraft.tileEntities.IllusionTileEntity;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.block.material.PushReaction;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.IFluidState;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.ILightReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.registries.ForgeRegistries;
import lombok.val;
import lombok.extern.log4j.Log4j2;

import java.util.Random;

@Log4j2
public class IllusionBlock extends ContainerBlock {
	public static final String REGISTRY_NAME = "illusion_block";
	public static final ResourceLocation RESOURCE_LOCATION = new ResourceLocation( TdwtfMinecraftMod.MOD_ID, REGISTRY_NAME );
	public static final RegistryObject<IllusionBlock> REGISTRY_OBJECT = RegistryObject.of( RESOURCE_LOCATION, ForgeRegistries.BLOCKS );
	public static final DirectionProperty FACING = BlockStateProperties.FACING;
	public static final BooleanProperty VISIBLE = BooleanProperty.create( "visible" );
	public static final Material MATERIAL = new Material(
		MaterialColor.NONE, // color
		false, // isLiquid
		false, // isSolid
		false, // blocksMovement,
		false, // isOpaque
		false, // requiresNoTool
		false, // canBurn
		false, // isReplacable
		PushReaction.NORMAL // pushReaction
	);
	public static final IllusionBlock BLOCK = new IllusionBlock();

	private IllusionBlock() {
		super(
			Properties.of( MATERIAL )
			.strength( 1.f, 1.f )
			.harvestTool( ToolType.PICKAXE )
			.noOcclusion()
			.lightLevel( 1 )
			.sound( SoundType.GLASS )
		);
		LOGGER.debug( "{}::ctor", getClass().getName() );
		setRegistryName( RESOURCE_LOCATION );
		registerDefaultState(
			stateDefinition.any()
			.setValue( FACING, Direction.NORTH )
			.setValue( VISIBLE, false )
		);
	}

	@Override
	@OnlyIn(Dist.CLIENT)
	public boolean isSuffocating( BlockState state, IBlockReader world, BlockPos pos ) {
		return !state.getValue( VISIBLE );
	}

	@Override
	public boolean hasTileEntity( BlockState state ) {
		return true;
	}

	@Override
	public TileEntity createTileEntity( BlockState state, IBlockReader world ) {
		return newBlockEntity( world );
	}

	@Override
	@OnlyIn(Dist.CLIENT)
	public boolean skipRendering( BlockState state, BlockState adjacentBlockState, Direction side ) {
		return !state.getValue( VISIBLE );
	}

	@Override
	protected void createBlockStateDefinition( Builder<Block, BlockState> builder ) {
		builder.add( FACING );
		builder.add( VISIBLE );
	}

	@Override
	public VoxelShape getShape( BlockState state, IBlockReader world, BlockPos pos, ISelectionContext context ) {
		return VoxelShapes.block();
	}

	@Override
	public VoxelShape getCollisionShape( BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context ) {
		if( state.getValue( VISIBLE ) ) {
			return VoxelShapes.empty();
		} else {
			return VoxelShapes.block();
		}
	}

	@Override
	public VoxelShape getOcclusionShape( BlockState state, IBlockReader world, BlockPos pos ) {
		if( state.getValue( VISIBLE ) ) {
			return getEmulatedBlockState( world, pos ).getOcclusionShape( world, pos );
		} else {
			return VoxelShapes.empty();
		}
	}

	@Override
	public VoxelShape getInteractionShape( BlockState state, IBlockReader world, BlockPos pos ) {
		if( state.getValue( VISIBLE ) ) {
			return getEmulatedBlockState( world, pos ).getInteractionShape( world, pos );
		} else {
			return VoxelShapes.empty();
		}
	}

	@Override
	public BlockState rotate( BlockState state, Rotation rot ) {
		return state.setValue( FACING, rot.rotate( state.getValue( FACING ) ) );
	}

	@Override
	public BlockState getStateForPlacement( BlockItemUseContext context ) {
		return defaultBlockState().setValue(
			FACING,
			context.getNearestLookingDirection().getOpposite()
		);
	}

	@Override
	public BlockRenderType getRenderShape( BlockState state ) {
		if( state.getValue( VISIBLE ) ) {
			return BlockRenderType.ENTITYBLOCK_ANIMATED;
		} else {
			return BlockRenderType.INVISIBLE;
		}
	}

	@Override
	@OnlyIn(Dist.CLIENT)
	public void animateTick( BlockState state, World world, BlockPos pos, Random rand ) {
		getEmulatedBlock( world, pos ).animateTick( state, world, pos, rand );
	}

	@Override
	public boolean propagatesSkylightDown( BlockState state, IBlockReader world, BlockPos pos ) {
		return getEmulatedBlockState( world, pos ).propagatesSkylightDown( world, pos );
	}

	@Override
	public boolean isAir( BlockState state, IBlockReader world, BlockPos pos ) {
		return false;
	}

	@Override
	@OnlyIn(Dist.CLIENT)
	public float getShadeBrightness( BlockState state, IBlockReader world, BlockPos pos ) {
	  return getEmulatedBlockState( world, pos ).getShadeBrightness( world, pos );
	}

	@Override
	@OnlyIn(Dist.CLIENT)
	public boolean isViewBlocking( BlockState state, IBlockReader world, BlockPos pos ) {
		return getEmulatedBlockState( world, pos ).isViewBlocking( world, pos );
	}

	@Override
	public boolean isPossibleToRespawnInThis() {
		return false;
	}

	@Override
	public boolean useShapeForLightOcclusion( BlockState state ) {
		return !state.getValue( VISIBLE );
	}

	@Override
	public ActionResultType use( BlockState state, World world, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit ) {
		if( world.isClientSide() ) return ActionResultType.PASS;
		val menuProvider = getMenuProvider( state, world, pos );
		if( menuProvider == null ) return ActionResultType.PASS;
		player.openMenu( menuProvider );
		return ActionResultType.SUCCESS;
	}

	@Override
	public int getLightValue( BlockState state, IBlockReader world, BlockPos pos ) {
		return getEmulatedBlockState( world, pos ).getLightValue( world, pos );
	}

	@Override
	public int getLightBlock( BlockState state, IBlockReader world, BlockPos pos ) {
		return getEmulatedBlockState( world, pos ).getLightBlock( world, pos );
	}

	private IllusionTileEntity getBlockEntity( IBlockReader world, BlockPos pos ) {
		val te = world.getBlockEntity( pos );
		if( te == null  ) return null;
		if( !( te instanceof IllusionTileEntity ) ) {
			LOGGER.error( "{}::getTileEntity wrong entity type: {}", getClass().getName(), te.getClass().getName() );
			return null;
		}
		return (IllusionTileEntity) te;
	}

	public Block getEmulatedBlock( IBlockReader world, BlockPos pos ) {
		if( world instanceof IWorld ) {
			val provider = ((IWorld)world).getChunkSource();
			if( !provider.isEntityTickingChunk( new ChunkPos( pos ) ) ) {
				LOGGER.error( "{}::getEmulatedBlock chunk not loaded: {}", getClass().getName(), pos );
				return Blocks.AIR;
			}
		}
		val te = getBlockEntity( world, pos );
		if( te == null ) return Blocks.AIR;
		val b = te.getEmulatedBlock();
		if( b == this ) return Blocks.AIR;
		return b;
	}

	public BlockState getEmulatedBlockState( IBlockReader world, BlockPos pos ) {
		if( world instanceof IWorld ) {
			val provider = ((IWorld)world).getChunkSource();
			if( !provider.isEntityTickingChunk( new ChunkPos( pos ) ) ) {
				LOGGER.error( "{}::getEmulatedBlockState chunk not loaded: {}", getClass().getName(), pos );
				return Blocks.AIR.defaultBlockState();
			}
		}
		val te = getBlockEntity( world, pos );
		if( te == null ) return Blocks.AIR.defaultBlockState();
		val emulatedBlockState = te.getEmulatedBlockState();
		if( emulatedBlockState == null ) return Blocks.AIR.defaultBlockState();
		if( emulatedBlockState.getBlock() == this ) return Blocks.AIR.defaultBlockState();
		return emulatedBlockState;
	}

	@Override
	@SuppressWarnings("deprecation")
	public void onRemove( BlockState state, World world, BlockPos pos, BlockState newState, boolean isMoving ) {
		if( !isMoving && !world.isClientSide() ) {
			val te = getBlockEntity( world, pos );
			if( te != null ) {
				InventoryHelper.dropContents( world, pos, te );
			}
		}
		super.onRemove( state, world, pos, newState, isMoving );
	}

	@Override
	public boolean shouldDisplayFluidOverlay( BlockState state, ILightReader world, BlockPos pos, IFluidState fluidState ) {
		return super.shouldDisplayFluidOverlay( state, world, pos, fluidState ) || super.shouldDisplayFluidOverlay( getEmulatedBlockState( world, pos ), world, pos, fluidState );
	}

	@Override
	public TileEntity newBlockEntity( IBlockReader world ) {
		return new IllusionTileEntity();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void neighborChanged( BlockState state, World world, BlockPos pos, Block block, BlockPos fromPos, boolean isMoving ) {
		val te = (IllusionTileEntity)world.getBlockEntity( pos );
		if( te != null ) te.refreshEmulatedBlock();
		super.neighborChanged( state, world, pos, block, fromPos, isMoving );
	}

	@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
	public static class Registration {
		@SubscribeEvent
		public static void registerBlock( final RegistryEvent.Register<Block> e ) {
			LOGGER.debug( "{}::registerBlock", Registration.class.getName() );
			e.getRegistry().register( BLOCK );
		}

		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public static void registerClientSetup( final FMLClientSetupEvent e ) {
			LOGGER.debug( "{}::registerClientSetup", Registration.class.getName() );
			RenderTypeLookup.setRenderLayer( BLOCK, RenderType.cutout() );
		}
	}
}
