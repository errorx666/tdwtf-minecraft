package net.error_unlimited.tdwtf_minecraft.blocks;

import java.util.Random;

import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.TdwtfMinecraftMod;
import net.error_unlimited.tdwtf_minecraft.Utils;
import net.error_unlimited.tdwtf_minecraft.ai.goals.AvoidBlockGoal;
import net.error_unlimited.tdwtf_minecraft.items.MagicPowderItem;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.block.material.PushReaction;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.monster.CreeperEntity;
import net.minecraft.entity.passive.CatEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.Fluid;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ForgeRegistries;

@Log4j2
public class CatIdolBlock extends Block {
	public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;
	public static final String REGISTRY_NAME = "cat_idol_block";
	public static final ResourceLocation RESOURCE_LOCATION = new ResourceLocation( TdwtfMinecraftMod.MOD_ID, REGISTRY_NAME );
	public static final RegistryObject<CatIdolBlock> REGISTRY_OBJECT = RegistryObject.of( RESOURCE_LOCATION, ForgeRegistries.BLOCKS );
	public static final Material MATERIAL = new Material(
		MaterialColor.GOLD, // color
		false, // isLiquid
		true, // isSolid
		true, // blocksMovement,
		true, // isOpaque
		false, // requiresNoTool
		false, // canBurn
		false, // isReplacable
		PushReaction.NORMAL // pushReaction
	);
	public static final CatIdolBlock BLOCK = new CatIdolBlock();

	private CatIdolBlock() {
		super(
			Properties.of( MATERIAL )
			.strength( 1.f, 1.f )
			.noOcclusion()
			.harvestTool( ToolType.PICKAXE )
			.sound( SoundType.METAL )
		);
		LOGGER.debug( "{}::ctor", getClass().getName() );
		setRegistryName( RESOURCE_LOCATION );

		registerDefaultState(
			stateDefinition.any()
			.setValue( FACING, Direction.NORTH )
		);

		Utils.observeEvent( MinecraftForge.EVENT_BUS, EntityJoinWorldEvent.class )
		.filter( e -> e.getEntity() instanceof CreeperEntity )
		.subscribe( e -> {
			CreeperEntity c = (CreeperEntity)e.getEntity();
			c.goalSelector.addGoal( 3, new AvoidBlockGoal<CatIdolBlock>( c, CatIdolBlock.class, 6.0F, 1.0D, 1.2D ) );
		} );
	}

	@Override
	public BlockState rotate( BlockState state, Rotation rot ) {
		return state.setValue( FACING, rot.rotate( state.getValue( FACING ) ) );
	}

	@Override
	protected void createBlockStateDefinition( Builder<Block, BlockState> builder ) {
		builder.add( FACING );
	}

	@Override
	public BlockState getStateForPlacement( BlockItemUseContext context ) {
		return defaultBlockState().setValue(
			FACING,
			context.getHorizontalDirection().getOpposite()
		);
	}

	@Override
	public boolean propagatesSkylightDown( BlockState state, IBlockReader reader, BlockPos pos ) {
		return true;
	}

	@Override
	@OnlyIn(Dist.CLIENT)
	public void animateTick( BlockState state, World world, BlockPos pos, Random rand ) {
		if( world.getLightEmission( pos.above() ) >= 15 && !world.isRainingAt( pos.above() ) ) {
			world.addParticle( ParticleTypes.INSTANT_EFFECT, pos.getX() + .5D, pos.getY() + .7D, pos.getZ() + .5D, .0D, .0D, .0D );
		}
	}

	@Override
	public boolean canBeReplaced( BlockState state, Fluid fluid ) {
		return false;
	}

	@Override
	public int getTickDelay( IWorldReader worldReader ) {
		return 20;
	}

	private final Random rand = new Random();

	@Override
	public ActionResultType use( BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockRayTraceResult hit ) {
		if( world.isClientSide() ) return ActionResultType.PASS;
		ItemStack itemStack = player.getItemInHand( hand );
		if( itemStack.isEmpty() || !( itemStack.getItem() instanceof MagicPowderItem ) ) return ActionResultType.PASS;
		itemStack.setCount( itemStack.getCount() - 1 );
		world.removeBlock( pos, false );
		CatEntity cat = new CatEntity( EntityType.CAT, world );
		cat.setCatType( rand.nextInt( 11 ) );
		final int STEPS = 8;
		final double STEP_SIZE = ( 360.0D / (double)STEPS );
		final double R = .5;
		for( int step = 0; step < STEPS; ++step ) {
			double theta = step * STEP_SIZE;
			world.addParticle( ParticleTypes.INSTANT_EFFECT, pos.getX() + .5D + ( Math.cos( theta ) * R ), pos.getY() + 0.7D, pos.getZ() + 0.5D + ( Math.sin( theta ) * R ), 0.0D, 0.0D, 0.0D );
		}
		Direction facing = state.getValue( FACING );
		cat.moveTo( pos.getX() + .5D, pos.getY() + .5D, pos.getZ() + .5D, facing.toYRot() - 90.f, 0.f );
		world.addFreshEntity( cat );
		cat.setInLove( player );
		return ActionResultType.SUCCESS;
	}

	@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
	public static class Registration {
		@SubscribeEvent
		public static void registerBlock( final RegistryEvent.Register<Block> e ) {
			LOGGER.debug( "{}::registerBlock", Registration.class.getName() );
			e.getRegistry().register( BLOCK );
		}
	}
}
