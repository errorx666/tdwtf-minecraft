package net.error_unlimited.tdwtf_minecraft.blocks;

import java.util.AbstractMap;
import java.util.Collections;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.val;
import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.TdwtfMinecraftMod;
import net.error_unlimited.tdwtf_minecraft.damageSources.TdwtfMinecraftDamageSource;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.block.material.PushReaction;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.EntitySpawnPlacementRegistry.PlacementType;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.pathfinding.PathType;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ForgeRegistries;

@Log4j2
public class PunjiBlock extends Block {
	public static final DirectionProperty FACING = BlockStateProperties.FACING;
	public static final String REGISTRY_NAME = "punji_block";
	public static final ResourceLocation RESOURCE_LOCATION = new ResourceLocation( TdwtfMinecraftMod.MOD_ID, REGISTRY_NAME );
	public static final RegistryObject<PunjiBlock> REGISTRY_OBJECT = RegistryObject.of( RESOURCE_LOCATION, ForgeRegistries.BLOCKS );
	public static final DamageSource DAMAGE_SOURCE = TdwtfMinecraftDamageSource.PUNJI.bypassArmor();
	public static final Material MATERIAL = new Material(
		MaterialColor.COLOR_GREEN, // color
		false, // isLiquid
		true, // isSolid
		false, // blocksMovement,
		true, // isOpaque
		false, // requiresNoTool
		false, // canBurn
		false, // isReplacable
		PushReaction.NORMAL // pushReaction
	);
	public static final PunjiBlock BLOCK = new PunjiBlock();

	private PunjiBlock() {
		super(
			Properties.of( MATERIAL )
			.strength( 1.f, 1.f )
			.speedFactor( .7f )
			.noCollission()
			.noOcclusion()
			.harvestTool( ToolType.AXE )
			.sound( SoundType.BAMBOO )
		);
		LOGGER.debug( "{}::ctor", getClass().getName() );
		setRegistryName( RESOURCE_LOCATION );

		registerDefaultState(
			stateDefinition.any()
			.setValue( FACING, Direction.UP )
		);
	}

	public static final VoxelShape SHAPE_UP = Block.box( 1.D, 0.D, 1.D, 15.D, 4.D, 15.D );
	public static final VoxelShape SHAPE_NORTH = Block.box( 1.D, 1.D, 12.D, 15.D, 15.D, 16.D );
	public static final VoxelShape SHAPE_EAST = Block.box( 0.D, 1.D, 1.D, 4.D, 15.D, 15.D );
	public static final VoxelShape SHAPE_SOUTH = Block.box( 1.D, 1.D, 0.D, 15.D, 15.D, 4.D );
	public static final VoxelShape SHAPE_WEST = Block.box( 12.D, 1.D, 1.D, 16.D, 15.D, 15.D );
	public static final VoxelShape SHAPE_DOWN = Block.box( 1.D, 12.D, 1.D, 15.D, 16.D, 15.D );

	public static final Map<Direction, VoxelShape> SHAPES =
		Collections.unmodifiableMap(
			Stream.of(
				new AbstractMap.SimpleImmutableEntry<>( Direction.UP, SHAPE_UP ),
				new AbstractMap.SimpleImmutableEntry<>( Direction.NORTH, SHAPE_NORTH ),
				new AbstractMap.SimpleImmutableEntry<>( Direction.EAST, SHAPE_EAST ),
				new AbstractMap.SimpleImmutableEntry<>( Direction.SOUTH, SHAPE_SOUTH ),
				new AbstractMap.SimpleImmutableEntry<>( Direction.WEST, SHAPE_WEST ),
				new AbstractMap.SimpleImmutableEntry<>( Direction.DOWN, SHAPE_DOWN )
			).collect( Collectors.toMap( Map.Entry::getKey, Map.Entry::getValue ) )
		);
	@Override
	public VoxelShape getShape( BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context ) {
		return SHAPES.get( state.getValue( FACING ) );
	}

	@Override
	public boolean isPathfindable( BlockState state, IBlockReader worldIn, BlockPos pos, PathType type ) {
		return true;
	}

	@Override
	public boolean canCreatureSpawn( BlockState state, IBlockReader world, BlockPos pos, PlacementType type, EntityType<?> entityType ) {
		return false;
	}

	public static boolean canBePlacedOn( BlockState state, IWorldReader world, BlockPos pos ) {
		if( world.isEmptyBlock( pos ) ) return false;
		val blockState = world.getBlockState( pos );
		val block = blockState.getBlock();
		if( block instanceof PunjiBlock ) return false;
		val direction = state.getValue( FACING ).getOpposite();
		return blockState.isFaceSturdy( world, pos, direction );
	}

	@Override
	public boolean canSurvive( BlockState state, IWorldReader world, BlockPos pos ) {
		return canBePlacedOn( state, world, pos.relative( state.getValue( FACING ).getOpposite() ) );
	}

	@Override
	public BlockState updateShape( BlockState state, Direction facing, BlockState facingState, IWorld world, BlockPos currentPos, BlockPos facingPos ) {
		if( !state.canSurvive( world, currentPos ) ) return Blocks.AIR.defaultBlockState();
		return state;
	}

	@Override
	public void entityInside( BlockState state, World world, BlockPos pos, Entity entity ) {
		if( !( entity instanceof LivingEntity ) ) return;
		if( world.isClientSide() ) return;
		val livingEntity = (LivingEntity)entity;
		if( livingEntity.isCrouching() ) return;
		val movedX = livingEntity.xOld - livingEntity.getX();
		val movedY = livingEntity.yOld - livingEntity.getY();
		val movedZ = livingEntity.zOld - livingEntity.getZ();
		val movedSq = Math.pow( movedX, 2 ) + Math.pow( movedY, 2 ) + Math.pow( movedZ, 2 );
		val damage = (float)( Math.round( movedSq * 400.D ) / 10.D );
		if( damage < .5f ) return;
		livingEntity.hurt( DAMAGE_SOURCE, damage );
	}

	@Override
	public void stepOn( World world, BlockPos pos, Entity entity ) {
		if( !( entity instanceof LivingEntity ) ) return;
		if( world.isClientSide() ) return;
		val livingEntity = (LivingEntity)entity;
		if( livingEntity.isCrouching() ) return;
		val movedX = livingEntity.xOld - livingEntity.getX();
		val movedY = livingEntity.yOld - livingEntity.getY();
		val movedZ = livingEntity.zOld - livingEntity.getZ();
		val movedSq = Math.pow( movedX, 2 ) + Math.pow( movedY, 2 ) + Math.pow( movedZ, 2 );
		val damage = (float)( Math.round( movedSq * 250.D ) / 10.D );
		if( damage < .5f ) return;
		livingEntity.hurt( DAMAGE_SOURCE, damage );
	}

	@Override
	public void fallOn( World world, BlockPos pos, Entity entity, float fallDistance ) {
		if( !( entity instanceof LivingEntity ) ) return;
		if( world.isClientSide() ) return;
		val livingEntity = (LivingEntity)entity;
		val damage = (float)( Math.round( fallDistance * 100.D ) / 10.D );
		if( damage < .5f ) return;
		livingEntity.hurt( DAMAGE_SOURCE, damage );
	}

	@Override
	public PathNodeType getAiPathNodeType( BlockState state, IBlockReader world, BlockPos pos, MobEntity entity ) {
		return PathNodeType.DANGER_CACTUS;
	}

	@Override
	public BlockState rotate( BlockState state, Rotation rot ) {
		return state.setValue( FACING, rot.rotate( state.getValue( FACING ) ) );
	}

	@Override
	protected void createBlockStateDefinition( Builder<Block, BlockState> builder ) {
		builder.add( FACING );
	}

	@Override
	public BlockState getStateForPlacement( BlockItemUseContext context ) {
		return defaultBlockState().setValue(
			FACING,
			context.getNearestLookingDirection().getOpposite()
		);
	}

	@Override
	public boolean propagatesSkylightDown( BlockState state, IBlockReader reader, BlockPos pos ) {
		return true;
	}

	@Override
	public void tick( BlockState state, ServerWorld world, BlockPos pos, Random rand ) {
		if( !canSurvive( state, world, pos ) ) {
			world.destroyBlock( pos, true );
			return;
		}
	}

	@Override
	public void onNeighborChange( BlockState state, IWorldReader world, BlockPos pos, BlockPos neighbor ) {
		if( world.isClientSide() ) return;
		if( !canSurvive( state, world, pos ) ) {
			if( world instanceof IWorld ) {
				((IWorld)world).getBlockTicks().scheduleTick( pos, this, 1 );
			}
		}
	}

	@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
	public static class Registration {
		@SubscribeEvent
		public static void registerBlock( final RegistryEvent.Register<Block> e ) {
			LOGGER.debug( "{}::registerBlock", Registration.class.getName() );
			e.getRegistry().register( BLOCK );
		}
	}
}
