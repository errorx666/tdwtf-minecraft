package net.error_unlimited.tdwtf_minecraft.blocks;

import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.val;
import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.TdwtfMinecraftMod;
import net.error_unlimited.tdwtf_minecraft.Utils;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.IGrowable;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.block.material.PushReaction;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.minecraft.world.gen.Heightmap;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.TickEvent.WorldTickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ForgeRegistries;

@Log4j2
public class ChiraliumCrystalBlock extends Block implements IGrowable {
	public static final String REGISTRY_NAME = "chiralium_crystal_block";
	public static final ResourceLocation RESOURCE_LOCATION = new ResourceLocation( TdwtfMinecraftMod.MOD_ID, REGISTRY_NAME );
	public static final RegistryObject<ChiraliumCrystalBlock> REGISTRY_OBJECT = RegistryObject.of( RESOURCE_LOCATION, ForgeRegistries.BLOCKS );
	public static final Material MATERIAL
		= new Material(
			MaterialColor.GOLD, // color
			false, // isLiquid
			false, // isSolid
			false, // blocksMovement,
			true, // isOpaque
			false, // requiresNoTool
			false, // canBurn
			false, // isReplacable
			PushReaction.DESTROY // pushReaction
		);
	public static final int MIN_AGE = 0;
	public static final int MAX_AGE = 7;

	public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;
	public static final IntegerProperty AGE = BlockStateProperties.AGE_7;
	public static final ChiraliumCrystalBlock BLOCK = new ChiraliumCrystalBlock();
	private static final VoxelShape[] SHAPE_BY_AGE = new VoxelShape[] {
		Block.box( 7.5D, 0.D, 7.5D, 8.5D, 2.D, 8.5D ),
		Block.box( 7.D, 0.D, 7.D, 9.D, 3.D, 9.D ),
		Block.box( 7.D, 0.D, 7.D, 9.D, 4.D, 9.D ),
		Block.box( 6.5D, 0.D, 6.5D, 9.5D, 5.D, 9.5D ),
		Block.box( 6.D, 0.D, 6.D, 10.5D, 6.D, 10.5D ),
		Block.box( 5.5D, 0.D, 5.5D, 11.D, 7.D, 11.D ),
		Block.box( 5.5D, 0.D, 5.5D, 11.D, 8.D, 11.D ),
		Block.box( 5.5D, 0.D, 5.5D, 11.D, 9.D, 11.D )
	};

	private ChiraliumCrystalBlock() {
		super(
			Properties.of( MATERIAL )
			.strength( .15f, .15f )
			.harvestTool( ToolType.PICKAXE )
			.lightLevel( 1 )
			.sound( SoundType.GLASS )
			.randomTicks()
		);
		LOGGER.debug( "{}::ctor", getClass().getName() );
		setRegistryName( RESOURCE_LOCATION );
		registerDefaultState(
			stateDefinition.any()
			.setValue( FACING, Direction.NORTH )
			.setValue( AGE, MIN_AGE )
		);

		Utils.observeEvent( MinecraftForge.EVENT_BUS, WorldTickEvent.class )
		.filter( e -> e.side == LogicalSide.SERVER )
		.filter( e -> RANDOM.nextInt( 128 ) == 0 )
		.subscribe( e -> {
			assert( e.world instanceof ServerWorld );
			final double SPAWN_RADIUS = 48.D;
			val world = (ServerWorld)e.world;
			if( world.restoringBlockSnapshots ) return;
			val player = world.getRandomPlayer();
			if( player == null ) return;
			if( !world.canSeeSky( new BlockPos( player.position() ) ) ) return;
			val chunkProvider = world.getChunkSource();
			val x = (int)Math.round( player.getX() + RANDOM.nextGaussian() * SPAWN_RADIUS );
			val z = (int)Math.round( player.getZ() + RANDOM.nextGaussian() * SPAWN_RADIUS );
			val blockPos = world.getHeightmapPos( Heightmap.Type.WORLD_SURFACE, new BlockPos( x, 0, z ) );
			if( !chunkProvider.isEntityTickingChunk( new ChunkPos( blockPos ) ) ) return;
			val prevBlockState = world.getBlockState( blockPos );
			if( !prevBlockState.canBeReplaced( Fluids.EMPTY ) ) return;
			val blockState = defaultBlockState().setValue( FACING, Direction.Plane.HORIZONTAL.getRandomDirection( RANDOM ) );
			if( !canSpawnAt( blockState, world, blockPos ) ) return;
			world.setBlock( blockPos, blockState, Constants.BlockFlags.DEFAULT_AND_RERENDER );
			world.markAndNotifyBlock( blockPos, null, prevBlockState, blockState, Constants.BlockFlags.DEFAULT_AND_RERENDER );
		} );
	}

	@Override
	public BlockState rotate( BlockState state, Rotation rot ) {
		return state.setValue( FACING, rot.rotate( state.getValue( FACING ) ) );
	}

	@Override
	public boolean isToolEffective( BlockState state, ToolType tool ) {
		return true;
	}

	@Override
	protected void createBlockStateDefinition( Builder<Block, BlockState> builder ) {
		builder.add( AGE );
		builder.add( FACING );
	}

	@Override
	public BlockState getStateForPlacement( BlockItemUseContext context ) {
		return defaultBlockState().setValue(
			FACING,
			context.getHorizontalDirection().getOpposite()
		);
	}

	@Override
	@OnlyIn(Dist.CLIENT)
	public float getShadeBrightness( BlockState state, IBlockReader world, BlockPos pos ) {
		return 1.f;
	}

	@Override
	public boolean propagatesSkylightDown( BlockState state, IBlockReader reader, BlockPos pos ) {
		return true;
	}

	private static final Set<Block> BLOCKS_CAN_GROW_ON =
		Stream.of(
			Blocks.ANDESITE,
			Blocks.COARSE_DIRT,
			Blocks.COBBLESTONE,
			Blocks.CLAY,
			Blocks.DIORITE,
			Blocks.DIRT,
			Blocks.FROSTED_ICE,
			Blocks.GRANITE,
			Blocks.GRASS_BLOCK,
			Blocks.GRAVEL,
			Blocks.ICE,
			Blocks.MOSSY_COBBLESTONE,
			Blocks.OBSIDIAN,
			Blocks.PODZOL,
			Blocks.RED_SAND,
			Blocks.RED_SANDSTONE,
			Blocks.SNOW,
			Blocks.SNOW_BLOCK,
			Blocks.SAND,
			Blocks.SANDSTONE,
			Blocks.STONE
		).collect( Collectors.toSet() );

	public static boolean canGrowOn( BlockState state, World world, BlockPos pos ) {
		return false; // temporarily disable
		// if( world.isEmptyBlock( pos ) ) return false;
		// BlockState blockState = world.getBlockState( pos );
		// Block block = blockState.getBlock();
		// return BLOCKS_CAN_GROW_ON.contains( block );
	}

	public static boolean canGrowAt( BlockState state, World world, BlockPos pos ) {
		return false; // temporarily disable
		// if( world.isWaterAt( pos ) ) return false;
		// if( !canGrowOn( state, world, pos.below() ) ) return false;
		// return world.getBiome( pos ).getPrecipitation() != Biome.RainType.NONE;
	}

	public static boolean canSpawnAt( BlockState state, World world, BlockPos pos ) {
		return false; // temporarily disable
		// if( !canGrowAt( state, world, pos ) ) return false;
		// if( world.getHeightmapPos( Heightmap.Type.MOTION_BLOCKING, pos ).getY() > pos.getY() ) return false;
		// if( world instanceof World ) {
		// 	World w = (World)world;
		// 	Biome b = w.getBiome( pos );
		// 	if( !w.isRainingAt( pos ) && !b.shouldSnow( w, pos ) ) return false;
		// 	if( !w.canSeeSky( pos ) ) return false;
		// 	if( !w.isEmptyBlock( pos.above() ) ) return false;
		// }
		// return true;
	}

	@Override
	public boolean canSurvive( BlockState state, IWorldReader world, BlockPos pos ) {
		return world instanceof World && canGrowAt( state, ((World)world), pos );
	}

	@Override
	public boolean isValidBonemealTarget( IBlockReader world, BlockPos pos, BlockState state, boolean isClient ) {
		if( state.getValue( AGE ) >= MAX_AGE ) return false;
		assert( world instanceof World );
		return canSpawnAt( state, ((World)world), pos );
	}

	@Override
	public BlockState updateShape( BlockState state, Direction facing, BlockState facingState, IWorld world, BlockPos currentPos, BlockPos facingPos ) {
		if( !state.canSurvive( world, currentPos ) ) return Blocks.AIR.defaultBlockState();
		return state;
	}

	@Override
	public boolean isBonemealSuccess( World world, Random rand, BlockPos pos, BlockState state ) {
		return false;
	}

	@Override
	public VoxelShape getShape( BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context ) {
		return SHAPE_BY_AGE[ state.getValue( AGE ) ];
	}

	@Override
	public void performBonemeal( ServerWorld world, Random rand, BlockPos pos, BlockState state ) {
		val prevGrowth = state.getValue( AGE );
		val newGrowth = Math.min( prevGrowth + 1, MAX_AGE );
		if( newGrowth > prevGrowth ) {
			val newState = state.setValue( AGE, newGrowth );
			world.setBlock( pos, newState, Constants.BlockFlags.DEFAULT_AND_RERENDER );
			world.markAndNotifyBlock( pos, null, state, newState, Constants.BlockFlags.DEFAULT_AND_RERENDER );
		}
	}

	@Override
	public void tick( BlockState state, ServerWorld world, BlockPos pos, Random rand ) {
		if( !canSurvive( state, world, pos ) ) {
			world.destroyBlock( pos, true );
			return;
		}
		if( rand.nextInt( 4 ) == 0 && isValidBonemealTarget( world, pos, state, false ) ) {
			performBonemeal( world, rand, pos, state );
		}
	}

	@Override
	public void onNeighborChange( BlockState state, IWorldReader world, BlockPos pos, BlockPos neighbor ) {
		if( world.isClientSide() ) return;
		if( !canSurvive( state, world, pos ) ) {
			if( world instanceof IWorld ) {
				((IWorld)world).getBlockTicks().scheduleTick( pos, this, 1 );
			}
		}
	}

	@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
	public static class Registration {
		@SubscribeEvent
		public static void registerBlock( final RegistryEvent.Register<Block> e ) {
			LOGGER.debug( "{}::registerBlock", Registration.class.getName() );
			e.getRegistry().register( BLOCK );
		}
	}
}
