package net.error_unlimited.tdwtf_minecraft;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.EntityMobGriefingEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.eventbus.api.Event.Result;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;

import java.util.Set;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class TdwtfMinecraftModShared {
	private TdwtfMinecraftModSharedConfig _config;
	public TdwtfMinecraftModShared( TdwtfMinecraftModSharedConfig config ) {
		LOGGER.debug( "{}::ctor", getClass().getName() );
		_config = config;
		assert( _config != null );
	}

	public void run() {
		LOGGER.debug( "{}::run", getClass().getName() );
		Set<String> disableMobGriefingFor = _config.getDisableMobGriefingFor();
		if( disableMobGriefingFor.size() > 0 ) disableMobGriefing( disableMobGriefingFor );
	}

	private void disableMobGriefing( Set<String> disableMobGriefingFor ) {
		LOGGER.debug( "{}::disableMobGriefing", getClass().getName() );
		Utils.observeEvent( MinecraftForge.EVENT_BUS, EntityMobGriefingEvent.class )
		.filter( e -> disableMobGriefingFor.contains( e.getEntity().getClass().getSimpleName() ) )
		.subscribe( e -> {
			e.setResult( Result.DENY );
		} );
	}

	@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
	public static class Registration {
		@SubscribeEvent
		public static void registerCommonSetup( FMLCommonSetupEvent e ) {
			LOGGER.debug( "{}::registerCommonSetup", Registration.class.getName() );
			TdwtfMinecraftModChannel.INSTANCE.initialize();
		}
	}
}
