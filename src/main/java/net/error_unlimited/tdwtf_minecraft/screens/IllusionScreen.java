package net.error_unlimited.tdwtf_minecraft.screens;

import com.mojang.blaze3d.systems.RenderSystem;

import lombok.val;
import lombok.extern.log4j.Log4j2;
import net.error_unlimited.tdwtf_minecraft.TdwtfMinecraftMod;
import net.error_unlimited.tdwtf_minecraft.inventory.IllusionContainer;
import net.error_unlimited.tdwtf_minecraft.inventory.IllusionContainerType;
import net.minecraft.client.gui.ScreenManager;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

@Log4j2
@OnlyIn(Dist.CLIENT)
public class IllusionScreen extends ContainerScreen<IllusionContainer> {
	public static final ResourceLocation GUI_TEXTURE = new ResourceLocation( TdwtfMinecraftMod.MOD_ID, "textures/gui/illusion_screen.png" );
	public static final int GUI_TEXTURE_WIDTH = 256;
	public static final int GUI_TEXTURE_HEIGHT = 256;
	public static final int TEXT_COLOR = 0x404040;

	public IllusionScreen( IllusionContainer container, PlayerInventory playerInventory, ITextComponent title ) {
		super( container, playerInventory, title );
		LOGGER.debug( "{}::ctor", getClass().getName() );
		imageWidth = imageHeight = 184;
		passEvents = false;
	}

	@Override
	public void render( int mouseX, int mouseY, float partialTicks ) {
		renderBackground();
		super.render( mouseX, mouseY, partialTicks );
		renderTooltip( mouseX, mouseY );
	}

	@Override
	protected void renderLabels( int mouseX, int mouseY ) {
		font.draw( title.getColoredString(), 8.0F, 6.0F, TEXT_COLOR );
		font.draw( inventory.getDisplayName().getColoredString(), 8.0F, (float)( imageHeight - 96 + 2 ), TEXT_COLOR );
	}

	@Override
	protected void renderBg( float partialTicks, int mouseX, int mouseY ) {
		RenderSystem.color4f( 1.0F, 1.0F, 1.0F, 1.0F );
		minecraft.getTextureManager().bind( GUI_TEXTURE );
		val x = ( width - imageWidth ) / 2;
		val y = ( height - imageHeight ) / 2;
		blit( x, y, 0, 0, imageWidth, imageHeight, GUI_TEXTURE_WIDTH, GUI_TEXTURE_HEIGHT );
	}

	@Mod.EventBusSubscriber(modid = TdwtfMinecraftMod.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD, value = { Dist.CLIENT })
	@OnlyIn(Dist.CLIENT)
	public static class Registration {
		@SubscribeEvent
		@OnlyIn(Dist.CLIENT)
		public static void registerClientSetup( final FMLClientSetupEvent e ) {
			LOGGER.debug( "{}::registerClientSetup", Registration.class.getName() );
			ScreenManager.register( IllusionContainerType.CONTAINER_TYPE, IllusionScreen::new );
		}
	}
}
