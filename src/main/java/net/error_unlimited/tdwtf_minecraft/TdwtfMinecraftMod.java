package net.error_unlimited.tdwtf_minecraft;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Mod(TdwtfMinecraftMod.MOD_ID)
public class TdwtfMinecraftMod {
	public static final String MOD_ID = "tdwtf_minecraft";

	public TdwtfMinecraftMod() {
		LOGGER.debug( "{}::ctor", getClass().getName() );
		MinecraftForge.EVENT_BUS.register( this );
	}
}
