const { Rcon } = require( 'rcon-client' );

const { host, port, password, command } =
	require( 'yargs' )
	.options( {
		host: {
			alias: 'h',
			type: 'string',
			required: true
		},
		password: {
			type: 'string',
			required: true
		},
		port: {
			alias: 'p',
			type: 'number',
			default: 25575
		},
		command: {
			alias: 'c',
			type: 'string',
			required: true
		}
	} ).argv;

( async () => {
	try {
		const rcon = new Rcon( { host, port, password } );
		await rcon.connect();
		const response = await rcon.send( command );
		process.stdout.write( response );
		await rcon.end();
	} catch( ex ) {
		console.error( ex );
		process.exit( 1 );
	}
} )();
