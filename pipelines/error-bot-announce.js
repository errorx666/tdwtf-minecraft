const path = require( 'path' );

const { server, port, commit, apiKey } =
	require( 'yargs' )
	.options( {
		server: {
			type: 'string',
			required: true
		},
		port: {
			type: 'number',
			required: true
		},
		commit: {
			type: 'string',
			required: true
		},
		apiKey: {
			type: 'string',
			required: true
		}
	} ).argv;


const request = require( 'request-promise' );

( async () => {
	const git = require( 'simple-git/promise' )( path.join( __dirname, '..' ) );
	let status = 0;
	try {
		const result = await git.log( {} );
		const commits = result.all.map( ( { hash, date, author_name, author_email, message } ) => ( {
			hash,
			date,
			message,
			author: {
				name: author_name,
				email: author_email
			}
		} ) );
		await request( `http://${encodeURIComponent(server)}:${port}/webhooks/v1/minecraft/deploy`, {
			json: {
				commit,
				commits
			},
			encoding: 'utf8',
			method: 'POST',
			headers: {
				Connection: 'close',
				'X-API-Key': apiKey
			}
		} );
	} catch( ex ) {
		console.error( ex );
		status = 1;
	} finally {
		process.exit( status );
	}
} )();
