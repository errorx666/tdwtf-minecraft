#!/usr/bin/env bash
trap popd EXIT
pushd $(realpath "$(dirname "${BASH_SOURCE[0]}")")
. ./.local.sh
cd ..
yarn run asm
./gradlew clean build dist installer --info --stacktrace --warning-mode=all --build-cache -PisCI=$isCI
