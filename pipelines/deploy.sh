#!/usr/bin/env bash
trap popd EXIT
pushd $(realpath "$(dirname "${BASH_SOURCE[0]}")")
. ./.local.sh
./error-bot-announce.sh --server $ERROR_BOT_SERVER --port $ERROR_BOT_PORT --commit `git rev-parse HEAD` --apiKey $ERROR_BOT_API_KEY
cd ../installer
chown root:root /root/.ssh
chmod 0600 /root/.ssh/*
scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -C -v -p -i $SCP_IDENTITY -P $SCP_PORT tdwtf-minecraft-mods.zip $SCP_USER@$SCP_HOST:$SCP_REMOTE_PATH
lftp -d -p $FTP_PORT -u $FTP_USER,$FTP_PASSWORD $FTP_HOST << EOF
open
mrm serversync-*.jar
mput serversync-*.jar
cd /mods
lcd mods
mrm *
mput *.jar
lcd ../servermods
mput *.jar
cd /clientmods
lcd ../clientmods
mrm *
mput *.jar
cd /config/serversync
lcd ../config/serversync
mput serversync-server.cfg
cd /TDWTF/serverconfig
lcd ../../TDWTF/serverconfig
mput *
lcd ../..
bye
EOF
cd ../pipelines
./restart-server.sh --username $WEB_USER --password $WEB_PASSWORD
popd
