const puppeteer = require( 'puppeteer' );

const { username, password } =
	require( 'yargs' )
	.options( {
		username: {
			type: 'string',
			required: true
		},
		password: {
			type: 'string',
			required: true
		}
	} ).argv;


( async () => {
	const browser = await puppeteer.launch( {
		args: [ '--no-sandbox', '--disable-setuid-sandbox' ]
	} );
	let status = 0;
	try {
		const page = await browser.newPage();
		await page.goto( 'https://www.minecraft-hosting.pro/login' );
		await page.type( '#Login2Mail', username );
		await page.type( '#Login2Password', password );
		await page.click( '#Login2' );
		await page.waitForNavigation( { waitUntil: 'networkidle2' } ),
		await page.click( '[id^="restart_"].executeClass' );
		await page.waitForNavigation( { waitUntil: 'networkidle2', timeout: 5 * 60 * 1000 } );
		await browser.close();
	} catch( ex ) {
		console.error( ex );
		status = 1;
	}
	try {
		await browser.close();
	} catch( ex ) {
		console.error( ex );
		status = 1;
	} finally {
		process.exit( status );
	}
} )();
