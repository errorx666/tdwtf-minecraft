const { promisify } = require( 'util' );
const assert = require( 'assert' );
const fs = require( 'fs-extra' );
const path = require( 'path' );
const _ = require( 'lodash' );
const { createHash } = require( 'crypto' );
const { v4: uuid } = require( 'uuid' );
const { Client: yggdrasil } = require( 'yggdrasil/es6' );
const { Extract } = require( 'unzipper' );
const { CLIENT_TOKEN, EXTRA_ARGS, JAVA, MINECRAFT_HOME, PASSWORD, USERNAME } = require( './.local' );
const { get } = require( 'https' );
const { spawn } = require( 'child_process' );
const gradleParser = require( 'gradle-to-js/lib/parser' );
const _os = require( 'os' );

process.on( 'unhandledRejection', ex => {
	console.error( ex );
	process.exit( 1 );
} );

process.on( 'uncaughtException', ex => {
	console.error( ex );
	process.exit( 1 );
} );

( async () => {
	const { mc_version: minecraftVersion } = await gradleParser.parseFile( path.resolve( __dirname, '..', 'gradle.properties' ) );
	const forgeVersion = '31.2.33';

	const os = {
		arch: _os.arch(),
		name: ( () => {
			const type = _os.type();
			switch( type ) {
			case 'Windows_NT': return 'windows';
			default: throw new Error( `Unsupport platform: ${type}` );
			}
		} )(),
		version: _os.release()
	};

	const features = {
		has_custom_resolution: false,
		is_demo_user: false
	};

	function rulesMatch( rules ) {
		assert.ok( Array.isArray( rules ) );
		for( const rule of rules ) {
			const { action } = rule;
			let match = true;
			assert.ok( [ 'allow', 'disallow' ].includes( action ) );
			if( 'features' in rule ) {
				for( const [ key, value ] of Object.entries( rule.features ) ) {
					if( !( typeof value === 'string' ? features[ key ].match( value ) : value === features[ key ] ) ) {
						match = false;
						break;
					}
				}
			}
			if( 'os' in rule ) {
				for( const [ key, value ] of Object.entries( rule.os ) ) {
					if( !( typeof value === 'string' ? os[ key ].match( value ) : value === os[ key ] ) ) {
						match = false;
						break;
					}
				}
			}
			const result = ( action == 'allow' ) ? match : !match;
			if( !result ) return false;
		}
		return true;
	}

	function getValue( e ) {
		if( typeof e === 'string' ) return [ e ];
		if( !rulesMatch( e.rules ) ) return [];
		return e.value;
	}

	async function loadLauncherJson( name ) {
		let config = JSON.parse( await fs.readFile( path.resolve( MINECRAFT_HOME, 'versions', name, name + '.json' ), { encoding: 'utf8' } ) );
		if( config.inheritsFrom ) {
			const parentConfig = await loadLauncherJson( config.inheritsFrom );
			config = _.merge( {},
				_.omit( parentConfig, 'arguments', 'libraries' ),
				_.omit( config, 'arguments', 'libraries' ),
				{
					arguments: {
						game: [
							...( parentConfig.arguments?.game ?? [] ),
							...( config.arguments?.game ?? [] )
						],
						jvm: [
							...( parentConfig.arguments?.jvm ?? [] ),
							...( config.arguments?.jvm ?? [] )
						]
					},
					libraries: [
						...( parentConfig.libraries ?? [] ),
						...( config.libraries ?? [] )
					]
				} );
		}
		config.arguments.game = ( config.arguments?.game ?? [] ).flatMap( getValue );
		config.arguments.jvm = ( config.arguments?.jvm ?? [] ).flatMap( getValue );
		config.libraries =
			( config.libraries ?? [] )
			.filter( l => l.rules == null || rulesMatch( l.rules ) )
			.map( l => _.omit( l, 'rules' ) );
		return config;
	}

	async function login() {
		const cacheFile = path.resolve( __dirname, '.credentials.json' );
		const validate = promisify( yggdrasil.validate ).bind( yggdrasil );
		const refresh = promisify( yggdrasil.refresh ).bind( yggdrasil );
		const auth = promisify( yggdrasil.auth ).bind( yggdrasil );
		let accessToken = null;
		let clientToken = CLIENT_TOKEN;
		let name = null;
		if( await fs.exists( cacheFile ) ) {
			try {
				( { accessToken, clientToken, name } = JSON.parse( await fs.readFile( cacheFile, 'utf8' ) ) );
				assert.equal( clientToken, CLIENT_TOKEN );
				if( !await validate( accessToken ) ) accessToken = await refresh( accessToken, clientToken );
				await fs.writeFile( cacheFile, JSON.stringify( { clientToken, accessToken, name }, null, '\t' ) );
				return { name, accessToken, clientToken };
			} catch( ex ) {
				console.error( ex );
			}
		}
		const resp = await auth( {
			token: CLIENT_TOKEN,
			user: USERNAME,
			pass: PASSWORD,
			requestUser: true
		} );
		( { clientToken, accessToken, selectedProfile: { name } } = resp );
		await fs.writeFile( cacheFile, JSON.stringify( { clientToken, accessToken, name }, null, '\t' ) );
		return { clientToken, accessToken, name };
	}

	function expandVars( str, vars ) {
		return str.replace( /\$\{([a-z0-9_]+)\}/ig, ( str, varname ) => {
			const value = vars[ varname ];
			if( value == null ) {
				console.error( `Unknown var: ${varname}` );
			}
			return value;
		} );
	}

	function eventPromise( stream, resolveEvents = [ 'end', 'done', 'finish', 'complete' ], rejectEvents = [ 'error' ] ) {
		return new Promise( ( resolve, reject ) => {
			let cleanup;
			const res = e => {
				cleanup();
				resolve( e );
			};
			const rej = e => {
				cleanup();
				reject( e );
			};
			cleanup = () => {
				for( const e of resolveEvents ) {
					stream.removeListener( e, res );
				}
				for( const e of rejectEvents ) {
					stream.removeListener( e, rej );
				}
			};
			for( const e of resolveEvents ) {
				stream.addListener( e, res );
			}
			for( const e of rejectEvents ) {
				stream.addListener( e, rej );
			}
		} );
	}

	async function downloadFile( { path: filePath, sha1, size, url } ) {
		filePath = path.resolve( MINECRAFT_HOME, 'libraries', filePath );
		if( await fs.exists( filePath ) ) {
			if( ( await fs.stat( filePath ) )?.size === size ) {
				const hash = createHash( 'sha1' );
				try {
					const f = fs.createReadStream( filePath, {
						encoding: null
					} );
					try {
						f.pipe( hash );
						await eventPromise( f );
					} finally {
						f.close();
						f.destroy();
					}
					if( sha1 === hash.digest( 'hex' ) ) return filePath;
				} finally {
					hash.destroy();
				}
			}
		}
		console.log( `Downloading ${url}...` );
		await fs.mkdirp( path.dirname( filePath ) );
		const f = fs.createWriteStream( filePath, { encoding: null } );
		try {
			const response = await new Promise( resolve => {
				get( url, resolve );
			} );
			response.pipe( f );
			await eventPromise( f );
		} finally {
			f.close();
			f.destroy();
		}
		return filePath;
	}

	( async () => {
		try {
			const launcherInfo = await loadLauncherJson( `${minecraftVersion}-forge-${forgeVersion}` );

			const { clientToken, accessToken, name } = await login();
			assert.equal( clientToken, CLIENT_TOKEN );

			const nativeDir = path.resolve( MINECRAFT_HOME, 'bin', uuid() );
			await fs.writeFile( path.resolve( __dirname, '.launcherinfo.json' ), JSON.stringify( launcherInfo, null, '\t' ) );
			await fs.emptyDir( path.resolve( MINECRAFT_HOME, 'bin' ) );
			await fs.mkdirp( nativeDir );

			let classpathFiles = [];
			let nativeFiles = [];

			await Promise.all(
				launcherInfo.libraries.map( async ( { natives, downloads: { artifact, classifiers } } ) => {
					let nativeArtifact = null;

					let p = [ async () => {
						const filePath = await downloadFile( artifact );
						classpathFiles = [ ...classpathFiles, filePath ].sort();
					} ];

					if( natives?.[ os.name ] ) {
						nativeArtifact = classifiers[ natives[ os.name ] ];
						p = [ ...p, async () => {
							const filePath = await downloadFile( nativeArtifact );
							nativeFiles = [ ...nativeFiles, filePath ].sort();
							const rs = fs.createReadStream( filePath, { encoding: null } );
							try {
								rs.pipe( Extract( {
									path: nativeDir
								} ) );
								await eventPromise( rs );
							} finally {
								rs.close();
								rs.destroy();
							}
						} ];
					}
					await Promise.all( p.map( p => p() ) );
				} )
			);

			const CLASSPATH_SEP = ( path.sep === '\\' ) ? ';' : ':';
			const CLASSPATH = classpathFiles.join( CLASSPATH_SEP );

			const vars = {
				assets_index_name: launcherInfo.assets,
				assets_root: path.resolve( MINECRAFT_HOME, 'assets' ),
				auth_access_token: accessToken,
				auth_player_name: name,
				auth_uuid: clientToken,
				classpath: CLASSPATH,
				game_directory: path.resolve( MINECRAFT_HOME ),
				launcher_name: 'minecraft-launcher',
				launcher_version: '2.1.15167',
				natives_directory: nativeDir,
				user_type: 'mojang',
				version_name: launcherInfo.id,
				version_type: launcherInfo.type
			};

			const args = [
				...launcherInfo.arguments.jvm.map( e => expandVars( e, vars ) ),
				...EXTRA_ARGS,
				launcherInfo.mainClass,
				...launcherInfo.arguments.game.map( e => expandVars( e, vars ) ),
			];

			const p = spawn( JAVA, args, {
				cwd: MINECRAFT_HOME,
				env: {
					CLASSPATH
				},
				stdio: 'inherit'
			} );
			await eventPromise( p );
		} catch( ex ) {
			console.error( ex );
			process.exit( 1 );
		}
	} )();
} )();
