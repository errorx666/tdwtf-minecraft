#!/usr/bin/env bash
trap popd EXIT
pushd $(realpath "$(dirname "${BASH_SOURCE[0]}")")
cd $(dirname "$0")
node ./error-bot-announce.js $@
