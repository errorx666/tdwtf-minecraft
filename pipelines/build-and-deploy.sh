#!/usr/bin/env bash
trap popd EXIT
pushd $(realpath "$(dirname "${BASH_SOURCE[0]}")")
. ./.local.sh
. ./build.sh
popd
pwd
ls

. ./deploy.sh
