#!/usr/bin/env bash
trap popd EXIT
pushd $(realpath "$(dirname "${BASH_SOURCE[0]}")")
export DISPLAY=:99
node ./restart-server.js $@
